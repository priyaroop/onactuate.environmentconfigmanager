using System.Collections.Generic;

namespace Onactuate.Common.Configuration
{
    public interface IConfigurationProvider
    {
        string EnvironmentName { get; set; }
        IDictionary<string, string> GetProperties(string configToken);
        IDictionary<string, string> GetProperties(string configToken, string environment);
    }
}
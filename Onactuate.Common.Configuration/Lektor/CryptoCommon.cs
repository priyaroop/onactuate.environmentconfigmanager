﻿using System;
using System.Security.Cryptography;

namespace Onactuate.Common.Configuration.Lektor
{
	internal static class CryptoCommon
	{
		public static SymmetricAlgorithm GetSymmetricAlgorithm(SymmetricCryptoAlgorithm cryptoAlg)
		{
			SymmetricAlgorithm symmetricAlg;

			string strSetup = cryptoAlg.ToString();
			string[] parts = strSetup.Split('_');

			switch (parts[0])
			{
				case "AES":
					symmetricAlg = new AesCryptoServiceProvider();// AesManaged();
					break;
				case "Rijndael":
					symmetricAlg = new RijndaelManaged();
					symmetricAlg.BlockSize = int.Parse(parts[3]); // Rijndael is the only one that can set a different block size
					break;
				case "RC2":
					symmetricAlg = new RC2CryptoServiceProvider();
					break;
				case "DES":
					symmetricAlg = new DESCryptoServiceProvider();
					break;
				case "TripleDES":
					symmetricAlg = new TripleDESCryptoServiceProvider();
					break;
				default:
					throw new Exception("Invalid SymmetricAlgorithm");
			}

			symmetricAlg.KeySize = int.Parse(parts[1]);

			switch (parts[2])
			{
				case "CBC":
					symmetricAlg.Mode = CipherMode.CBC;
					break;
				case "ECB":
					symmetricAlg.Mode = CipherMode.ECB;
					break;
				case "CFB":
					symmetricAlg.Mode = CipherMode.CFB;
					break;
				default:
					throw new Exception("Invalid CipherMode");
			}
			
			return symmetricAlg;
		}

		public static System.Security.Cryptography.PaddingMode GetPaddingMode(PaddingMode paddingModes)
		{
			switch (paddingModes)
			{
				case PaddingMode.PKCS7:
					return System.Security.Cryptography.PaddingMode.PKCS7;

				case PaddingMode.None:
					return System.Security.Cryptography.PaddingMode.None;

				case PaddingMode.Zeros:
					return System.Security.Cryptography.PaddingMode.Zeros;

				case PaddingMode.ISO10126:
					return System.Security.Cryptography.PaddingMode.ISO10126;

				case PaddingMode.ANSIX923:
					return System.Security.Cryptography.PaddingMode.ANSIX923;

				default:
					throw new Exception("Invalid PaddingMode");
			}
		}

		// Get string in a byte format
		public static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

			return bytes;
		}

		// Get a string from a byte array
		public static string GetString(byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}
	}
}

﻿using System;
using System.Linq;
using System.Text;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.ConfigSettings.Encryption;

namespace Onactuate.Common.Configuration.Lektor
{
    public static class EncryptionHelper
    {
        //private const string PrivateKeyBackup = "87E65B3DAFE970AF49528DF38D082AFE592D7F6C804D1690E4ADA1531B6DEAA1CB2C7D3B55E2D3CCA58A8B3C638D96C6BB5D0B4E501B48B7C3F9865207FCA5A8";

        /// <summary>
        /// Method to encrypt string data.
        /// </summary>
        /// <param name="dataToEncrypt"></param>
        /// <returns></returns>
        public static byte[] EncryptText(string dataToEncrypt, string environmentName)
        {
            var privateKey = GetPrivateKey(environmentName);

            byte[] dataBytes = Encoding.ASCII.GetBytes(dataToEncrypt);
            byte[] encryptedPassword = CryptoHelper.EncryptData(privateKey, AutoSaltSizes.Salt64, dataBytes, SymmetricCryptoAlgorithm.AES_256_CBC);
            return encryptedPassword;
        }

        private static string GetPrivateKey(string environmentName)
        {
            string privateKey = string.Empty;

            try
            {
                //string environmentName = EnvironmentSection.GetEnvironmentName;
                bool pkFound = false;
                EncryptionSection encryptionSection = EncryptionSection.Instance;
                CryptoElementCollection encryptionSettings = encryptionSection.EncryptionSettings;
                CryptoElement cryptoElement;
                foreach (CryptoElement setting in encryptionSettings)
                {
                    if (setting.Environment.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
                    {
                        pkFound = true;
                        cryptoElement = setting;
                        privateKey = cryptoElement.PrivateKey;
                        break;
                    }
                }

                if (!pkFound)
                {
                    throw new Exception(string.Format("Private Key for environment {0} not found in the Machine.Config.", environmentName));
                }

                return privateKey;
            }
            catch (Exception ex)
            {
                throw new Exception("Private Key not found in the Machine.Config.");
            }
        }

        /// <summary>
        /// Method to decrypt the byte array data and return the decrypted text string
        /// </summary>
        /// <param name="dataToDecrypt"></param>
        /// <returns></returns>
        public static string DecryptData(byte[] dataToDecrypt, string environmentName)
        {
            string privateKey = GetPrivateKey(environmentName);

            byte[] decryptedPasswordAutoSalt = CryptoHelper.DecryptData(privateKey, AutoSaltSizes.Salt64, dataToDecrypt, SymmetricCryptoAlgorithm.AES_256_CBC);
            return Encoding.ASCII.GetString(decryptedPasswordAutoSalt);
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
﻿using System;
using System.Configuration;

namespace Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment
{
    public class EnvironmentSection : ConfigurationSection
    {
        public static string GetEnvironmentName
        {
            get
            {
                try
                {
                    EnvironmentSection environmentSection = (EnvironmentSection)ConfigurationManager.GetSection("EnvironmentSection");
                    return environmentSection.EnvironmentName;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Environment name not found in the Config file. \n{0}", ex.Message));
                }
            }
        }

        [ConfigurationProperty("environmentName", DefaultValue = "", IsKey = true, IsRequired = true)]
        private string EnvironmentName
        {
            get => (string) base["environmentName"];
            set => base["environmentName"] = value;
        }
    }
}
﻿using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess
{
    public class PagingParameters<TEntity>
    {
        public Expression<System.Func<TEntity, bool>> DefaultFilterExpression { get; set; }

        public Expression<System.Func<TEntity, bool>> DefaultSortOrder { get; set; }

        public int PageNumber { get; set; }

        public string Query { get; set; }

        public SqlQueryOperator QueryOperator { get; set; }

        public string QueryType { get; set; }

        public int RecordsPerPage { get; set; }

        public int SkipRecords
        {
            get
            {
                return ((this.PageNumber - 1) * this.RecordsPerPage);
            }
        }

        public string SortBy { get; set; }

        public SortOrder SortOrder { get; set; }

        public int TakeRecords
        {
            get
            {
                return this.RecordsPerPage;
            }
        }
    }
}


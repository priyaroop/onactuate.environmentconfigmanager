﻿using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess.LinqKit
{
    public static class Linq
    {
        public static Expression<System.Func<T, TResult>> Expr<T, TResult>(Expression<System.Func<T, TResult>> expr)
        {
            return expr;
        }

        public static System.Func<T, TResult> Func<T, TResult>(System.Func<T, TResult> expr)
        {
            return expr;
        }
    }
}


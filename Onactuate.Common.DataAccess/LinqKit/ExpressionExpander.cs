﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Onactuate.Common.DataAccess.LinqKit
{
    public class ExpressionExpander : ExpressionVisitor
    {
        private Dictionary<ParameterExpression, Expression> _replaceVars;

        internal ExpressionExpander()
        {
            this._replaceVars = null;
        }

        private ExpressionExpander(Dictionary<ParameterExpression, Expression> replaceVars)
        {
            this._replaceVars = null;
            this._replaceVars = replaceVars;
        }

        private Expression TransformExpr(MemberExpression input)
        {
            if (((((input != null) && (input.Member is FieldInfo)) && input.Member.ReflectedType.IsNestedPrivate) && input.Member.ReflectedType.Name.StartsWith("<>")) && (input.Expression is ConstantExpression))
            {
                object obj2 = ((ConstantExpression) input.Expression).Value;
                if (obj2 == null)
                {
                    return input;
                }
                Type type = obj2.GetType();
                if (!(type.IsNestedPrivate && type.Name.StartsWith("<>")))
                {
                    return input;
                }
                object obj3 = ((FieldInfo) input.Member).GetValue(obj2);
                if (obj3 is Expression)
                {
                    return this.Visit((Expression) obj3);
                }
            }
            return input;
        }

        protected override Expression VisitInvocation(InvocationExpression iv)
        {
            Dictionary<ParameterExpression, Expression> dictionary;
            Expression expression = iv.Expression;
            if (expression is MemberExpression)
            {
                expression = this.TransformExpr((MemberExpression) expression);
            }
            if (expression is ConstantExpression)
            {
                expression = ((ConstantExpression) expression).Value as Expression;
            }
            LambdaExpression expression2 = (LambdaExpression) expression;
            if (this._replaceVars == null)
            {
                dictionary = new Dictionary<ParameterExpression, Expression>();
            }
            else
            {
                dictionary = new Dictionary<ParameterExpression, Expression>(this._replaceVars);
            }
            try
            {
                for (int i = 0; i < expression2.Parameters.Count; i++)
                {
                    dictionary.Add(expression2.Parameters[i], iv.Arguments[i]);
                }
            }
            catch (ArgumentException exception)
            {
                throw new InvalidOperationException("Invoke cannot be called recursively - try using a temporary variable.", exception);
            }
            return new ExpressionExpander(dictionary).Visit(expression2.Body);
        }

        protected override Expression VisitMemberAccess(MemberExpression m)
        {
            if (m.Member.DeclaringType.Name.StartsWith("<>"))
            {
                return this.TransformExpr(m);
            }
            return base.VisitMemberAccess(m);
        }

        protected override Expression VisitMethodCall(MethodCallExpression m)
        {
            if ((m.Method.Name == "Invoke") && (m.Method.DeclaringType == typeof(Extensions)))
            {
                Dictionary<ParameterExpression, Expression> dictionary;
                Expression expression = m.Arguments[0];
                if (expression is MemberExpression)
                {
                    expression = this.TransformExpr((MemberExpression) expression);
                }
                if (expression is ConstantExpression)
                {
                    expression = ((ConstantExpression) expression).Value as Expression;
                }
                LambdaExpression expression2 = (LambdaExpression) expression;
                if (this._replaceVars == null)
                {
                    dictionary = new Dictionary<ParameterExpression, Expression>();
                }
                else
                {
                    dictionary = new Dictionary<ParameterExpression, Expression>(this._replaceVars);
                }
                try
                {
                    for (int i = 0; i < expression2.Parameters.Count; i++)
                    {
                        dictionary.Add(expression2.Parameters[i], m.Arguments[i + 1]);
                    }
                }
                catch (ArgumentException exception)
                {
                    throw new InvalidOperationException("Invoke cannot be called recursively - try using a temporary variable.", exception);
                }
                return new ExpressionExpander(dictionary).Visit(expression2.Body);
            }
            if ((m.Method.Name == "Compile") && (m.Object is MemberExpression))
            {
                MemberExpression input = (MemberExpression) m.Object;
                Expression expression4 = this.TransformExpr(input);
                if (expression4 != input)
                {
                    return expression4;
                }
            }
            if ((m.Method.Name == "AsExpandable") && (m.Method.DeclaringType == typeof(Extensions)))
            {
                return m.Arguments[0];
            }
            return base.VisitMethodCall(m);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            if ((this._replaceVars != null) && this._replaceVars.ContainsKey(p))
            {
                return this._replaceVars[p];
            }
            return base.VisitParameter(p);
        }
    }
}


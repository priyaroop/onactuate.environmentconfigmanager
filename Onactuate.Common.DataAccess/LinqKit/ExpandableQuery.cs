﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess.LinqKit
{
    public class ExpandableQuery<T> : IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IOrderedQueryable, IQueryable, IEnumerable
    {
        private IQueryable<T> _inner;
        private ExpandableQueryProvider<T> _provider;

        internal ExpandableQuery(IQueryable<T> inner)
        {
            this._inner = inner;
            this._provider = new ExpandableQueryProvider<T>((ExpandableQuery<T>) this);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this._inner.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._inner.GetEnumerator();
        }

        public override string ToString()
        {
            return this._inner.ToString();
        }

        internal IQueryable<T> InnerQuery
        {
            get
            {
                return this._inner;
            }
        }

        Type IQueryable.ElementType
        {
            get
            {
                return typeof(T);
            }
        }

        Expression IQueryable.Expression
        {
            get
            {
                return this._inner.Expression;
            }
        }

        IQueryProvider IQueryable.Provider
        {
            get
            {
                return this._provider;
            }
        }
    }
}


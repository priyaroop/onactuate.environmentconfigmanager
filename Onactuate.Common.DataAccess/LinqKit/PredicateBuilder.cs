﻿using System.Linq;
using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess.LinqKit
{
    public static class PredicateBuilder
    {
        public static Expression<System.Func<T, bool>> And<T>(this Expression<System.Func<T, bool>> expr1, LambdaExpression expr2)
        {
            InvocationExpression right = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<System.Func<T, bool>>(Expression.And(expr1.Body, right), expr1.Parameters);
        }

        public static Expression<System.Func<T, bool>> And<T>(this Expression<System.Func<T, bool>> expr1, Expression<System.Func<T, bool>> expr2)
        {
            InvocationExpression right = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<System.Func<T, bool>>(Expression.AndAlso(expr1.Body, right), expr1.Parameters);
        }

        public static Expression<System.Func<T, bool>> False<T>()
        {
            return f => false;
        }

        public static Expression<System.Func<T, bool>> Or<T>(this Expression<System.Func<T, bool>> expr1, LambdaExpression expr2)
        {
            InvocationExpression right = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<System.Func<T, bool>>(Expression.Or(expr1.Body, right), expr1.Parameters);
        }

        public static Expression<System.Func<T, bool>> Or<T>(this Expression<System.Func<T, bool>> expr1, Expression<System.Func<T, bool>> expr2)
        {
            InvocationExpression right = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<System.Func<T, bool>>(Expression.OrElse(expr1.Body, right), expr1.Parameters);
        }

        public static Expression<System.Func<T, bool>> True<T>()
        {
            return f => true;
        }
    }
}


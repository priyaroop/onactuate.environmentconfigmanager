﻿using System.Linq;
using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess.LinqKit
{
    internal class ExpandableQueryProvider<T> : IQueryProvider
    {
        private ExpandableQuery<T> _query;

        internal ExpandableQueryProvider(ExpandableQuery<T> query)
        {
            this._query = query;
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            return this._query.InnerQuery.Provider.CreateQuery(expression.Expand());
        }

        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
        {
            return new ExpandableQuery<TElement>(this._query.InnerQuery.Provider.CreateQuery<TElement>(expression.Expand()));
        }

        object IQueryProvider.Execute(Expression expression)
        {
            return this._query.InnerQuery.Provider.Execute(expression.Expand());
        }

        TResult IQueryProvider.Execute<TResult>(Expression expression)
        {
            return this._query.InnerQuery.Provider.Execute<TResult>(expression.Expand());
        }
    }
}


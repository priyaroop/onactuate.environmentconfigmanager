﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
//using System.Data.Objects;
using System.ServiceModel;
using System.Threading;
using System.Web;
using Onactuate.Common.Logging;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.LoggingContextProviders;

//using System.Data.Entity.Core.Objects;

namespace Onactuate.Common.DataAccess
{
    public class ObjectContextManager
    {
        private static ConcurrentDictionary<int, ObjectContext> _contextStorage = new ConcurrentDictionary<int, ObjectContext>();
        private static ILoggingService _loggingService = new LoggingService(new StaticLoggingContextProvider("ObjectContextManager"));
        private static ConcurrentDictionary<string, Lazy<ObjectContextBuilder<ObjectContext>>> _objectContextBuilders = new ConcurrentDictionary<string, Lazy<ObjectContextBuilder<ObjectContext>>>();
        private const string _serviceName = "ObjectContextManager";
        private static object _syncLock = new object();
        private const string CLOUDCONTEXTKEY = "Onactuate.webportal.domain-cloudContext";
        public static readonly string DefaultConnectionStringName = "DbConnectionString";

        private static void AddConfiguration(string connectionStringName, string[] mappingAssemblies, bool recreateDatabaseIfExists = false, bool lazyLoadingEnabled = true)
        {
            _loggingService.Debug("ObjectContextManager", "AddConfiguration: " + connectionStringName);
            if (string.IsNullOrEmpty(connectionStringName))
            {
                throw new ArgumentNullException("connectionStringName");
            }
            if (mappingAssemblies == null)
            {
                throw new ArgumentNullException("mappingAssemblies");
            }
            _objectContextBuilders.TryAdd(connectionStringName, new Lazy<ObjectContextBuilder<ObjectContext>>(delegate {
                _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "Created builder - key: ", connectionStringName, " objectContextBuilders.Count: ", _objectContextBuilders.Count }));
                return new ObjectContextBuilder<ObjectContext>(connectionStringName, mappingAssemblies, recreateDatabaseIfExists, lazyLoadingEnabled);
            }));
        }

        public static void CloseAllObjectContexts()
        {
            if (Storage != null)
            {
                foreach (ObjectContext context in Storage.GetAllObjectContexts())
                {
                    if (context.Connection.State == ConnectionState.Open)
                    {
                        context.Connection.Close();
                    }
                }
            }
        }

        public static ObjectContext CurrentFor(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }
            bool recreateDatabaseIfExists = Convert.ToBoolean(ConfigurationManager.AppSettings["RecreateDBIfExists"]);
            if (HttpContext.Current != null)
            {
                return GetObjectContextFromHttpContext(key, recreateDatabaseIfExists);
            }
            if ((OperationContext.Current != null) && (OperationContext.Current.Extensions != null))
            {
                return GetObjectContextFromOperationContext(key, recreateDatabaseIfExists);
            }
            return GetObjectContextFromThread(key, recreateDatabaseIfExists);
        }

        private static ObjectContext GetObjectContextFromDictionary(string key, bool recreateDatabaseIfExists, IDictionary dictionary)
        {
            Func<ObjectContextBuilder<ObjectContext>> valueFactory = null;
            ObjectContext context = null;
            if (dictionary["Onactuate.webportal.domain-cloudContext"] == null)
            {
                if (valueFactory == null)
                {
                    valueFactory = delegate {
                        _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "Created builder - key: ", key, " objectContextBuilders.Count: ", _objectContextBuilders.Count }));
                        return new ObjectContextBuilder<ObjectContext>(key, new string[] { "Onactuate.webportal.domain" }, recreateDatabaseIfExists, true);
                    };
                }
                context = _objectContextBuilders.GetOrAdd(key, new Lazy<ObjectContextBuilder<ObjectContext>>(valueFactory)).Value.BuildObjectContext();
                dictionary["Onactuate.webportal.domain-cloudContext"] = context;
                return context;
            }
            return (ObjectContext) dictionary["Onactuate.webportal.domain-cloudContext"];
        }

        private static ObjectContext GetObjectContextFromHttpContext(string key, bool recreateDatabaseIfExists)
        {
            return GetObjectContextFromDictionary(key, recreateDatabaseIfExists, HttpContext.Current.Items);
        }

        private static ObjectContext GetObjectContextFromOperationContext(string key, bool recreateDatabaseIfExists)
        {
            OperationContextStateStore item = OperationContext.Current.Extensions.Find<OperationContextStateStore>();
            if (item == null)
            {
                item = new OperationContextStateStore();
                OperationContext.Current.Extensions.Add(item);
            }
            return GetObjectContextFromDictionary(key, recreateDatabaseIfExists, item.State);
        }

        private static ObjectContext GetObjectContextFromThread(string key, bool recreateDatabaseIfExists)
        {
            int hash = Thread.CurrentThread.GetHashCode();
            return _contextStorage.GetOrAdd(hash, delegate {
                ObjectContextBuilder<ObjectContext> builder = _objectContextBuilders.GetOrAdd(key, new Lazy<ObjectContextBuilder<ObjectContext>>(delegate {
                    _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "Created builder - key: ", key, " objectContextBuilders.Count: ", _objectContextBuilders.Count }));
                    return new ObjectContextBuilder<ObjectContext>(key, new string[] { "Onactuate.webportal.domain" }, recreateDatabaseIfExists, true);
                })).Value;
                _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "Creating context - hash: ", hash, " _contextStorage.Count: ", _contextStorage.Count }));
                return builder.BuildObjectContext();
            });
        }

        public static void Init(string[] mappingAssemblies, bool recreateDatabaseIfExist = false, bool lazyLoadingEnabled = true)
        {
            Init(DefaultConnectionStringName, mappingAssemblies, recreateDatabaseIfExist, lazyLoadingEnabled);
        }

        public static void Init(string connectionStringName, string[] mappingAssemblies, bool recreateDatabaseIfExist = false, bool lazyLoadingEnabled = true)
        {
            AddConfiguration(connectionStringName, mappingAssemblies, recreateDatabaseIfExist, lazyLoadingEnabled);
        }

        public static void InitStorage(IObjectContextStorage storage)
        {
            if (storage == null)
            {
                throw new ArgumentNullException("storage");
            }
            if ((Storage != null) && (Storage != storage))
            {
                throw new ApplicationException("A storage mechanism has already been configured for this application");
            }
            Storage = storage;
        }

        public static void RemoveContext()
        {
            ObjectContext context;
            if ((HttpContext.Current != null) && (HttpContext.Current.Items["Onactuate.webportal.domain-cloudContext"] != null))
            {
                context = (ObjectContext) HttpContext.Current.Items["Onactuate.webportal.domain-cloudContext"];
                if (context.Connection.State == ConnectionState.Open)
                {
                    context.Connection.Close();
                }
                context.Dispose();
                HttpContext.Current.Items["Onactuate.webportal.domain-cloudContext"] = null;
            }
            else
            {
                int hashCode = Thread.CurrentThread.GetHashCode();
                _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "Removing context - hash: ", hashCode, " _contextStorage.Count: ", _contextStorage.Count }));
                context = null;
                if (_contextStorage.TryRemove(hashCode, out context))
                {
                    if (context.Connection.State == ConnectionState.Open)
                    {
                        context.Connection.Close();
                    }
                    context.Dispose();
                    context = null;
                    _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "Removed context - hash: ", hashCode, " _contextStorage.Count: ", _contextStorage.Count }));
                }
                else
                {
                    _loggingService.Debug("ObjectContextManager", string.Concat(new object[] { "WARNING CONTEXT NOT REMOVED - hash: ", hashCode, " _contextStorage.Count: ", _contextStorage.Count }));
                }
            }
        }

        public static ObjectContext Current
        {
            get
            {
                ObjectContext context;
                try
                {
                    context = CurrentFor(DefaultConnectionStringName);
                }
                catch (Exception)
                {
                    throw;
                }
                return context;
            }
        }

        private static IObjectContextStorage Storage
        {
            get
            {
                return Storage;
            }
            set
            {
                Storage = value;
            }
        }
    }
}


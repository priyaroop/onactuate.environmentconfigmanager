﻿namespace Onactuate.Common.DataAccess
{
    public enum SqlQueryOperator
    {
        eq,
        ne,
        lt,
        le,
        gt,
        ge,
        bw,
        bn,
        ew,
        en,
        cn,
        nc
    }
}


﻿using System.Collections.Generic;

namespace Onactuate.Common.DataAccess
{
    public class PagingResultset<TEntity>
    {
        public PagingResultset(IEnumerable<TEntity> pagedResultset, int totalRecords)
        {
            this.PagedResultset = pagedResultset;
            this.TotalRecords = totalRecords;
        }

        public IEnumerable<TEntity> PagedResultset { get; private set; }

        public int TotalRecords { get; private set; }
    }
}


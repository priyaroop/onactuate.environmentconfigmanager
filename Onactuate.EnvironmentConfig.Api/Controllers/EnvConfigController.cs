﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Onactuate.Common.Configuration;
using Onactuate.Common.Configuration.Models;

namespace Onactuate.EnvironmentConfig.Api.Controllers
{
    public class EnvConfigController : ApiController
    {
        private ConfigurationProviderFactory ConfigurationProviderFactoryObj { get; }
        private IConfigurationProvider EnvironmentConfig { get; }

        public EnvConfigController()
        {
            ConfigurationProviderFactoryObj = new ConfigurationProviderFactory();
            EnvironmentConfig = ConfigurationProviderFactoryObj.GetConfigurationProvider();
        }

        [HttpGet]
        [Authorize]
        public EnvironmentKeys GetEnvironmentKeys(string environmentName, string tokenName)
        {
            Dictionary<string, string> keys = EnvironmentConfig.GetProperties(tokenName, environmentName).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            EnvironmentKeys envKeys = new EnvironmentKeys(keys);
            return envKeys;
        }
        
        [HttpGet]
        [Authorize]
        public string GetEnvironmentKeyValue(string environmentName, string tokenName, string keyName)
        {
            Dictionary<string, string> keys = EnvironmentConfig.GetProperties(tokenName, environmentName).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return keys[keyName];
        }
    }
}

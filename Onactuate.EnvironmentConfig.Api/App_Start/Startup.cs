﻿
using System;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Onactuate.EnvironmentConfig.Api.Provider;
using Owin;

[assembly: OwinStartup(typeof(Onactuate.EnvironmentConfig.Api.Startup))] 
namespace Onactuate.EnvironmentConfig.Api
{
    public class Startup
    {
        public void ConfigureAuth(IAppBuilder app) 
        { 
            
            var OAuthOptions = new OAuthAuthorizationServerOptions 
            { 
                AllowInsecureHttp = true, 
                TokenEndpointPath = new PathString("/token"), 
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(20), 
                Provider = new SimpleAuthorizationServerProvider() 
            }; 
 
            app.UseOAuthBearerTokens(OAuthOptions); 
            app.UseOAuthAuthorizationServer(OAuthOptions); 
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()); 
 
            HttpConfiguration config = new HttpConfiguration(); 
            WebApiConfig.Register(config); 
        } 
 
        public void Configuration(IAppBuilder app) 
        { 
            ConfigureAuth(app); 
            GlobalConfiguration.Configure(WebApiConfig.Register); 
        } 
    }
}
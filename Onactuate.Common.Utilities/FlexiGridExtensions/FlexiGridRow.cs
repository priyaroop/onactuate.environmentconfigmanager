﻿using System.Collections.Generic;

namespace Onactuate.Common.Utilities.FlexiGridExtensions
{
    public class FlexiGridRow
    {
        public List<string> cell = new List<string>();
        public string cssClass = string.Empty;
        public int id;
        public bool isChild = true;
        public bool isExpanded = true;
        public bool isVisible = true;
        public int? parentId = null;
    }
}


﻿using System.Collections.Generic;

namespace Onactuate.Common.Utilities.FlexiGridExtensions
{
    public class FlexiGridObject
    {
        public List<FlexiGridRow> detailRows = new List<FlexiGridRow>();
        public int page;
        public List<FlexiGridRow> rows = new List<FlexiGridRow>();
        public int total;
    }
}


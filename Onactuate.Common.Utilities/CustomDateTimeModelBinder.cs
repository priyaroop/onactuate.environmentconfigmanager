﻿using System;
using System.Globalization;
using System.Web.Mvc;
using Onactuate.Common.Logging;
using Onactuate.Common.Logging.LoggingContextProviders;

namespace Onactuate.Common.Utilities
{
    public class CustomDateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            string displayFormatString = bindingContext.ModelMetadata.DisplayFormatString;
            ValueProviderResult result = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            new LoggingService(new StaticLoggingContextProvider(this.ToString())).Info(this.ToString(), "Passed in date value: " + result.AttemptedValue);
            if (!string.IsNullOrEmpty(displayFormatString) && (result != null))
            {
                DateTime time;
                displayFormatString = displayFormatString.Replace("{0:", string.Empty).Replace("}", string.Empty);
                if (DateTime.TryParseExact(result.AttemptedValue, displayFormatString, CultureInfo.CurrentCulture, DateTimeStyles.None, out time))
                {
                    return time;
                }
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("{0} is an invalid date format", result.AttemptedValue));
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}


﻿namespace Onactuate.Common.Utilities
{
    public interface IFormatActivationEmailTemplate
    {
        string CraftEmailTemplate(string appSettingConstants, string activationUrlId, string customerNumber);
    }
}


﻿using System;
using System.ServiceModel;

namespace Onactuate.Common.Utilities
{
    public class DisposableChannelFactory<T> : IDisposable where T: class
    {
        private ChannelFactory<T> _channelFactory;

        public DisposableChannelFactory(string endpointConfigurationName, string endPointUri)
        {
            this.CreateChannelFactory(endpointConfigurationName, endPointUri);
            this.Channel = this._channelFactory.CreateChannel();
        }

        public DisposableChannelFactory(string endpointConfigurationName, string endPointUri, string userName, string password)
        {
            this.CreateChannelFactory(endpointConfigurationName, endPointUri);
            this._channelFactory.Credentials.UserName.UserName = userName;
            this._channelFactory.Credentials.UserName.Password = password;
            this.Channel = this._channelFactory.CreateChannel();
        }

        private void CreateChannelFactory(string endpointConfigurationName, string endPointUri)
        {
            EndpointAddress remoteAddress = new EndpointAddress(endPointUri);
            this._channelFactory = new ChannelFactory<T>(endpointConfigurationName, remoteAddress);
        }

        public void Dispose()
        {
            if (this._channelFactory != null)
            {
                if (this._channelFactory != null)
                {
                    this._channelFactory.Close();
                }
                string fullName = this._channelFactory.GetType().FullName;
                bool flag = false;
                try
                {
                    if (this._channelFactory.State != CommunicationState.Faulted)
                    {
                        this._channelFactory.Close();
                        flag = true;
                    }
                }
                catch (FaultException)
                {
                    throw;
                }
                catch (CommunicationException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (!flag)
                    {
                        this._channelFactory.Abort();
                    }
                }
            }
        }

        public T Channel { get; set; }
    }
}


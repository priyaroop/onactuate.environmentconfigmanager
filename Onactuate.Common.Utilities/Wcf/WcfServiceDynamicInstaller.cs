﻿using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Globalization;
using System.ServiceProcess;

namespace Onactuate.Common.Utilities.Wcf
{
    /// <summary>
    /// This is a custom project installer.
    /// Applies a unique name to the service using the /name switch
    /// Sets user name and password using the /user and /password switches
    /// Allows the use of a local account using the /account switch
    /// </summary>
    [RunInstaller(true)]
    public class WcfServiceDynamicInstaller : Installer
    {
        #region Public Properties

        public string ServiceName
        {
            get { return _serviceInstaller.ServiceName; }
            set { _serviceInstaller.ServiceName = value; }
        }

        public string DisplayName
        {
            get { return _serviceInstaller.DisplayName; }
            set { _serviceInstaller.DisplayName = value; }
        }

        public string Description
        {
            get { return _serviceInstaller.Description; }
            set { _serviceInstaller.Description = value; }
        }

        public ServiceStartMode StartType
        {
            get { return _serviceInstaller.StartType; }
            set { _serviceInstaller.StartType = value; }
        }

        public ServiceAccount Account
        {
            get { return _processInstaller.Account; }
            set { _processInstaller.Account = value; }
        }

        public string ServiceUsername
        {
            get { return _processInstaller.Username; }
            set { _processInstaller.Username = value; }
        }

        public string ServicePassword
        {
            get { return _processInstaller.Password; }
            set { _processInstaller.Password = value; }
        }

        #endregion

        #region Private Member Variables

        private readonly ServiceProcessInstaller _processInstaller;
        private readonly ServiceInstaller _serviceInstaller;

        #endregion

        #region Class Constructors

        /// <summary>
        /// Default Class Constructor for the 
        /// </summary>
        public WcfServiceDynamicInstaller()
        {
            _processInstaller = new ServiceProcessInstaller
                                    {Account = ServiceAccount.User, Username = null, Password = null};
            _serviceInstaller = new ServiceInstaller
                                    {
                                        StartType = ServiceStartMode.Automatic,
                                        ServiceName = "WcfDefaultService",
                                        DisplayName = "",
                                        Description = ""
                                    };

            Installers.AddRange(new Installer[]
                                    {
                                        _processInstaller,
                                        _serviceInstaller
                                    });
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Return the value of the parameter in dictated by key
        /// </summary>
        /// <PARAM name="key">Context parameter key</PARAM>
        /// <returns>Context parameter specified by key</returns>
        public string GetContextParameter(string key)
        {
            string sValue;
            try
            {
                sValue = Context.Parameters[key].ToString(CultureInfo.InvariantCulture);
            }
            catch
            {
                sValue = string.Empty;
            }
            return sValue;
        }

        #endregion

        #region Protected Override Methods

        /// <summary>
        /// This method is run before the install process.
        /// This method is overridden to set the following parameters:
        /// service name (/name switch)
        /// account type (/account switch)
        /// for a user account user name (/user switch)
        /// for a user account password (/password switch)
        /// Note that when using a user account,
        /// if the user name or password is not set,
        /// the installing user is prompted for the credentials to use.
        /// </summary>
        /// <PARAM name="savedState"></PARAM>
        protected override void OnBeforeInstall(IDictionary savedState)
        {
            base.OnBeforeInstall(savedState);
            bool isUserAccount = false;

            // Decode the command line switches
            string name = GetContextParameter("name").Trim();
            if (name != "")
            {
                _serviceInstaller.ServiceName = name;
            }
            string desc = GetContextParameter("desc").Trim();
            if (desc != "")
            {
                _serviceInstaller.Description = desc;
            }
            // What type of credentials to use to run the service
            string acct = GetContextParameter("account");
            switch (acct.ToLower())
            {
                case "user":
                    _processInstaller.Account = ServiceAccount.User;
                    isUserAccount = true;
                    break;
                case "localservice":
                    _processInstaller.Account = ServiceAccount.LocalService;
                    break;
                case "localsystem":
                    _processInstaller.Account = ServiceAccount.LocalSystem;
                    break;
                case "networkservice":
                    _processInstaller.Account = ServiceAccount.NetworkService;
                    break;
            }

            // User name and password
            string username = GetContextParameter("user").Trim();
            string password = GetContextParameter("password").Trim();
            // Should I use a user account?
            if (isUserAccount)
            {
                // If we need to use a user account,
                // set the user name and password
                if (username != "")
                {
                    _processInstaller.Username = username;
                }
                if (password != "")
                {
                    _processInstaller.Password = password;
                }
            }
        }

        /// <summary>
        /// Uninstall based on the service name
        /// </summary>
        /// <PARAM name="savedState"></PARAM>
        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            base.OnBeforeUninstall(savedState);
            // Set the service name based on the command line
            string name = GetContextParameter("name").Trim();
            if (name != "")
            {
                _serviceInstaller.ServiceName = name;
            }
        }

        #endregion
    }
}

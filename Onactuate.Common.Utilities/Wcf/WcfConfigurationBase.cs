using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web;
using Onactuate.Common.Configuration;
using Onactuate.Common.Utilities.Extensions;

namespace Onactuate.Common.Utilities.Wcf
{
    public class WcfConfigurationBase : IWcfConfiguration
    {
        public const int MaxConnections = 1000;
        protected const int ListenBacklog = 1000;
        private const string ClientCertKey = "ClientCertificateName";
        private const string ServerCertKey = "ServerCertificateName";
        private const string SecurityBindingKey = "ServerSecurityBinding";

        //hardcoding these timeouts to 1 day (should be enough for anyone) - they default to only 10 minutes
        private readonly TimeSpan _closeTimeout = new TimeSpan(1, 0, 0, 0);
        private readonly TimeSpan _openTimeout = new TimeSpan(1, 0, 0, 0);
        private readonly TimeSpan _receiveTimeout = new TimeSpan(1, 0, 0, 0);

        private readonly string _configToken;

        protected readonly Dictionary<Type, ContractDescription> _contractDescriptions;
        private readonly string _environment;
        protected readonly IDictionary<string, string> Configuration;

        public WcfConfigurationBase(ICollection<ContractDescription> contracts, string configToken)
            : this(contracts, configToken, GetDefaultEnvironment())
        { }

        public WcfConfigurationBase(ICollection<ContractDescription> contracts, IDictionary<string, string> configuration)
            : this(contracts, configuration, false)
        { }

        internal WcfConfigurationBase(Type contractType, string configToken, string environment)
            :this(new ContractDescription[]{CreateContractDescription(contractType)}, configToken, environment)
        {}

        /// <summary>
        /// Initializes a new instance of the WcfConfigurationBase class
        /// </summary>
        /// <param name="contracts"></param>
        /// <param name="configToken"></param>
        /// <param name="environment"></param>
        internal  WcfConfigurationBase(ICollection<ContractDescription> contracts, string configToken, string environment)
        {
            //validate arguments
            if (contracts.Count == 0)
            {
                throw new ArgumentException("contracts");
            }

            _contractDescriptions = contracts.ToDictionary(contractDescription => contractDescription.ContractType);
            _configToken = configToken;
            _environment = environment;

            SetMaxItemsInObjectGraph();

            ConfigurationProviderFactory factory = new ConfigurationProviderFactory();
            IConfigurationProvider envConfig = factory.GetConfigurationProvider();

            Configuration = envConfig.GetProperties(_configToken, _environment);

            if (Configuration.ContainsKey(SecurityBindingKey))
            {
                string configSecurityValue = Configuration[SecurityBindingKey];
                Debug.Assert(Enum.IsDefined(typeof (WcfSecurityBindingType), configSecurityValue),
                             "EnvConfig key ServerSecurityBinding must be set to a valid memner of WcfSecurityBindingType");

                SecurityBinding =
                    (WcfSecurityBindingType) Enum.Parse(typeof (WcfSecurityBindingType), configSecurityValue, true);
            }
            else
            {
                SecurityBinding = WcfSecurityBindingType.None;
            }
        }

        /// <summary>
        /// Initializes a new instance of the WcfConfigurationBase class
        /// </summary>
        /// <param name="contracts"></param>
        /// <param name="configuration"></param>
        internal WcfConfigurationBase(ICollection<ContractDescription> contracts, IDictionary<string, string> configuration, bool someVal=false)
        {
            //validate arguments
            if (contracts.Count == 0)
            {
                throw new ArgumentException("contracts");
            }

            _contractDescriptions = contracts.ToDictionary(contractDescription => contractDescription.ContractType);
            Configuration = configuration;

            SetMaxItemsInObjectGraph();

            if (Configuration.ContainsKey(SecurityBindingKey))
            {
                string configSecurityValue = Configuration[SecurityBindingKey];
                Debug.Assert(Enum.IsDefined(typeof(WcfSecurityBindingType), configSecurityValue),
                             "EnvConfig key ServerSecurityBinding must be set to a valid memner of WcfSecurityBindingType");

                SecurityBinding =
                    (WcfSecurityBindingType)Enum.Parse(typeof(WcfSecurityBindingType), configSecurityValue, true);
            }
            else
            {
                SecurityBinding = WcfSecurityBindingType.None;
            }
        }


        public WcfSecurityBindingType SecurityBinding { get; protected set; }

        public virtual ServiceProtocol ServiceProtocol
        {
            get
            {
                Uri baseAddress = new Uri(Configuration[WcfConstants.ServiceBaseAddressConfig]);
                switch(baseAddress.Scheme)
                {
                    case "http":
                        return Wcf.ServiceProtocol.Http;
                    case "https":
                        return Wcf.ServiceProtocol.Https;
                    case "net.tcp":
                        return Wcf.ServiceProtocol.Tcp;
                    default:
                        throw new ServiceActivationException(
                            "The service address (ServiceBaseAddress in Config) could not be detected as a web service (http(s)://) or a .NET TCP service (net.tcp://).");
                }
            }
        }

        public Dictionary<Type, ContractDescription> ContractDescriptions
        {
            get { return _contractDescriptions; }
        }

        #region Endpoints

        public virtual IEnumerable<ServiceEndpoint> GetEndpoints()
        {
            ServiceProtocol protocol = ServiceProtocol;

            List<ServiceEndpoint> endpoints = new List<ServiceEndpoint>();
            foreach (ContractDescription contract in _contractDescriptions.Values)
            {
                switch(protocol)
                {
                    case Wcf.ServiceProtocol.Tcp:
                        endpoints.Add(CreateTcpEndpoint(contract));
                        break;
                    case Wcf.ServiceProtocol.Http:
                    case Wcf.ServiceProtocol.Https:
                        endpoints.Add(CreateHttpEndpoint(contract, protocol));
                        if (IsAsmxCompatible(contract))
                        {
                            endpoints.Add(CreateAsmxEndpoint(contract, protocol));
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return endpoints;
        }

        private static bool ContractHasAttribute<TAttribute>(ContractDescription contract)
            where TAttribute : Attribute
        {
            return GetAttributeByType<TAttribute>(contract) != null;
        }

        protected virtual ServiceEndpoint CreateTcpEndpoint(ContractDescription contract)
        {
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.Transport, false)
                                        {
                                            Namespace = contract.Namespace,
                                            MaxBufferSize = int.MaxValue,
                                            MaxReceivedMessageSize = int.MaxValue,
                                            CloseTimeout = _closeTimeout,
                                            OpenTimeout = _openTimeout,
                                            ReceiveTimeout = _receiveTimeout,
                                            TransactionFlow = true,
                                            TransactionProtocol = TransactionProtocol.OleTransactions,
                                            MaxConnections = MaxConnections,
                                            ListenBacklog = ListenBacklog,
                                            ReaderQuotas =
                                                {
                                                    MaxDepth = int.MaxValue,
                                                    MaxStringContentLength = int.MaxValue,
                                                    MaxArrayLength = int.MaxValue,
                                                    MaxBytesPerRead = int.MaxValue,
                                                    MaxNameTableCharCount = int.MaxValue
                                                }
                                        };

            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;

            return new ServiceEndpoint(contract, binding, new EndpointAddress(GetServiceUri(contract.ContractType.Name)));
        }

        protected virtual ServiceEndpoint CreateHttpEndpoint(ContractDescription contract, ServiceProtocol protocol)
        {
            FastInfoSetEncodingAttribute fisAttribute = GetAttributeByType<FastInfoSetEncodingAttribute>(contract);

            WS2007HttpBinding binding =
                new WS2007WithIisAuthHttpBinding(protocol == ServiceProtocol.Https
                                                     ? SecurityMode.Transport
                                                     : SecurityMode.None)
                {
                    Namespace = contract.Namespace,
                    MaxReceivedMessageSize = int.MaxValue,
                    MaxBufferPoolSize = int.MaxValue,
                    TransactionFlow = true,
                    ReaderQuotas =
                    {
                        MaxDepth = int.MaxValue,
                        MaxStringContentLength = int.MaxValue,
                        MaxArrayLength = int.MaxValue,
                        MaxBytesPerRead = int.MaxValue,
                        MaxNameTableCharCount = int.MaxValue
                    },
                    UseCompression = ShouldUseCompression(contract),
                    //FastInfoSetEncoding = fisAttribute != null ? fisAttribute.WriterProfile : null,
                    SecurityBindingType = SecurityBinding
                };
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;

            return new ServiceEndpoint(contract, binding, new EndpointAddress(GetServiceUri(contract.ContractType.Name)));
        }

        protected virtual ServiceEndpoint CreateAsmxEndpoint(ContractDescription contract, ServiceProtocol protocol)
        {
            BasicHttpBinding binding =
                new BasicHttpBinding(protocol == ServiceProtocol.Https
                                         ? BasicHttpSecurityMode.Transport
                                         : BasicHttpSecurityMode.TransportCredentialOnly)
                    {
                        Namespace = contract.Namespace,
                        MaxReceivedMessageSize = int.MaxValue,
                        MaxBufferPoolSize = int.MaxValue,
                        ReaderQuotas =
                            {
                                MaxDepth = int.MaxValue,
                                MaxStringContentLength = int.MaxValue,
                                MaxArrayLength = int.MaxValue,
                                MaxBytesPerRead = int.MaxValue,
                                MaxNameTableCharCount = int.MaxValue
                            },
                    };

            //"windows" makes it <http:NegotiateAuthentication/> for this endpoint, while "Ntlm" makes it use <http:NtlmAuthentication/>
            //.Net 2.0 clients only work with "Windows" when it is running in IIS, and only work with "Ntlm" when self-hosted
            //don't know the reason for this, hence the hack.
            binding.Security.Transport.ClientCredentialType = CheckIfRunningInAspNet()
                                                                  ? HttpClientCredentialType.Windows
                                                                  : HttpClientCredentialType.Ntlm;

            ServiceEndpoint endpoint = new ServiceEndpoint(contract, binding,
                                                           new EndpointAddress(
                                                               new Uri(GetServiceUri(contract.ContractType.Name) + "/asmx")));

            endpoint.Behaviors.Add(new FlatWsdl());
            return endpoint;
        }

        private static bool IsAsmxCompatible(ContractDescription contract)
        {
            return ContractHasAttribute<AsmxCompatibleAttribute>(contract);
        }

        private static bool ShouldUseCompression(ContractDescription contract)
        {
            return ContractHasAttribute<MessageCompressionAttribute>(contract);
        }

        private static bool CheckIfRunningInAspNet()
        {
            return HttpRuntime.AppDomainAppId != null;
        }

        private static TAttribute GetAttributeByType<TAttribute>(ContractDescription contract)
            where TAttribute : Attribute
        {
            // TODO: extension method for this
            return contract.ContractType.GetAttribute<TAttribute>();
        }

        #endregion

        #region Certificates

        public virtual X509Certificate2 GetClientCert()
        {
            return GetCertFromConfig(ClientCertKey);
        }

        public virtual X509Certificate2 GetServerCert()
        {
            return GetCertFromConfig(ServerCertKey);
        }

        public virtual string GetDNSAlias()
        {
            return Configuration.ContainsKey(ServerCertKey) ? Configuration[ServerCertKey] : string.Empty;
        }

        /// <summary>
        /// Gets the X509Certificate2 from the keystore
        /// </summary>
        /// <param name="storeLocation"></param>
        /// <param name="storeName"></param>
        /// <param name="findType"></param>
        /// <param name="findValue"></param>
        /// <returns></returns>
        public static X509Certificate2 GetCert(StoreLocation storeLocation, StoreName storeName, X509FindType findType, object findValue)
        {
            X509Store store = new X509Store(storeName, storeLocation);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection certCollection = store.Certificates.Find(findType, findValue, false);
            if (certCollection.Count == 0)
            {
                throw new SecurityException("Certificate not found");
            }
            if (certCollection.Count > 1)
            {
                throw new SecurityException("Certificate not uniquely defined by the search criteria");
            }
            return certCollection[certCollection.Count - 1];
        }

        public static X509Certificate2 GetCert(string certLocation)
        {
            StoreLocation storeLocation = StoreLocation.LocalMachine;
            StoreName storeName = StoreName.My;
            X509FindType findType = X509FindType.FindBySubjectName;

            string[] searchInfo = certLocation.Split(new char[] {',', ';'});
            Array.Reverse(searchInfo);
            if (searchInfo.Length == 0)
            {
                throw new SecurityException("Certificate [" + certLocation + "] not found");
            }
            object findValue = searchInfo[0];
            if (searchInfo.Length > 1)
            {
                findType = (X509FindType)Enum.Parse(typeof(X509FindType), searchInfo[1]);
            }
            else if (searchInfo.Length > 2)
            {
                storeName = (StoreName)Enum.Parse(typeof(StoreName), searchInfo[2]);
            }
            if (searchInfo.Length > 3)
            {
                storeLocation = (StoreLocation)Enum.Parse(typeof(StoreLocation), searchInfo[3]);
            }

            return GetCert(storeLocation, storeName, findType, findValue);
        }

        protected X509Certificate2 GetCertFromConfig(string keyName)
        {
            return Configuration.ContainsKey(keyName) ? GetCert(Configuration[keyName]) : null;
        }

        #endregion

        protected internal static string GetConfigToken(Type serviceType)
        {
            TypeDescriptor.AddAttributes(serviceType, new XmlSerializerFormatAttribute());
            ServiceTokenAttribute tokenAttribute =
                (ServiceTokenAttribute) TypeDescriptor.GetAttributes(serviceType)[typeof (ServiceTokenAttribute)];

            if (tokenAttribute == null)
            {
                throw new ServiceActivationException(
                    string.Format("Could not find a token from the ServiceToken attribute on the type {0}.", serviceType));
            }

            return tokenAttribute.Token;
        }

        /// <summary>
        /// Retrieves the name of the current environment as defined in Environment configuration's
        /// default configuration provider (namely, via the application domain's app.config)
        /// </summary>
        /// <returns></returns>
        protected internal static string GetDefaultEnvironment()
        {
            ConfigurationProviderFactory factory = new ConfigurationProviderFactory();
            IConfigurationProvider envConfig = factory.GetConfigurationProvider();
            return envConfig.EnvironmentName;
        }

        private static ContractDescription CreateContractDescription(Type contract)
        {
            if(!contract.IsInterface)
                throw new ArgumentException("Type parameter must be an interface.");

            return ContractDescription.GetContract(contract);
        }

        /// <summary>
        /// Sets the maximum number of DataContract instances to serialize/ deserialize to int.MaxValue
        /// </summary>
        private void SetMaxItemsInObjectGraph()
        {
            foreach (ContractDescription contractDescription in _contractDescriptions.Values)
            {
                foreach (OperationDescription operationDescription in contractDescription.Operations)
                {
                    DataContractSerializerOperationBehavior existingBehavior =
                        operationDescription.Behaviors.Find<DataContractSerializerOperationBehavior>();

                    if (existingBehavior != null)
                    {
                        existingBehavior.MaxItemsInObjectGraph = Int32.MaxValue;
                    }
                }
            }
        }

        public virtual Uri GetServiceUri(string typeName)
        {
            if (typeName.StartsWith("I") && Char.IsUpper(typeName[1]))
            {
                typeName = typeName.Substring(1);
            }

            return new Uri(string.Format("{0}/{1}", Configuration[WcfConstants.ServiceBaseAddressConfig], typeName), UriKind.Absolute);
        }

        public virtual Uri GetTcpMexUri(string typeName)
        {
            if (Configuration.ContainsKey(WcfConstants.MexBaseAddressConfig))
            {
                return null;
            }

            return new Uri(string.Format("{0}/{1}", Configuration[WcfConstants.MexBaseAddressConfig], typeName), UriKind.Absolute);
        }

        public static string GetTokenHash(string configToken, string environment)
        {
            return string.Format("{0}.{1}", environment, configToken);
        }
    }
}
using System;

namespace Onactuate.Common.Utilities.Wcf
{
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public class MessageCompressionAttribute : Attribute
    {
    }
}
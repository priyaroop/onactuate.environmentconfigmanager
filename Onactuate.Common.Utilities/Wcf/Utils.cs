using System;

namespace Onactuate.Common.Utilities.Wcf
{
    public static class Utils
    {
        public static TAttribute GetAttribute<TAttribute>(Type type) where TAttribute : Attribute
        {
            return (TAttribute) Attribute.GetCustomAttribute(type, typeof (TAttribute));
        }

        public static TAttribute GetAttribute<TAttribute, TType>(Type type) where TAttribute : Attribute
        {
            return (TAttribute)Attribute.GetCustomAttribute(typeof(TType), typeof(TAttribute));
        }
    }
}
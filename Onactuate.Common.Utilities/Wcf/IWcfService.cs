using System;
using System.ServiceModel;

namespace Onactuate.Common.Utilities.Wcf
{
    public interface IWcfService : IDisposable
    {
        ServiceHost Service { get; }
        void Open();
    }
}
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Description;

namespace Onactuate.Common.Utilities.Wcf
{
    public interface IWcfConfiguration
    {
        Dictionary<Type, ContractDescription> ContractDescriptions { get; }
        ServiceProtocol ServiceProtocol { get; }
        WcfSecurityBindingType SecurityBinding { get; }
        Uri GetServiceUri(string name);
        Uri GetTcpMexUri(string name);
        IEnumerable<ServiceEndpoint> GetEndpoints();
        string GetDNSAlias();
        X509Certificate2 GetClientCert();
        X509Certificate2 GetServerCert();
    }
}
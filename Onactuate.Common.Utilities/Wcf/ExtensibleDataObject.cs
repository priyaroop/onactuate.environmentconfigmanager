using System.Runtime.Serialization;

namespace Onactuate.Common.Utilities.Wcf
{
    [DataContract(Name = "ExtensibleDataObject", Namespace = "urn:Common:Utilities:DataConractV1")]
    public class ExtensibleDataObject : IExtensibleDataObject
    {
        #region IExtensibleDataObject Members

        ExtensionDataObject IExtensibleDataObject.ExtensionData { get; set; }

        #endregion
    }
}
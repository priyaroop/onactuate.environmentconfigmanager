using System;

namespace Onactuate.Common.Utilities.Wcf
{
    /// <summary>
    /// Instructs the WCF classes to expose an ASMX-compatible endpoint for this interface.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public class AsmxCompatibleAttribute : Attribute
    {
    }
}
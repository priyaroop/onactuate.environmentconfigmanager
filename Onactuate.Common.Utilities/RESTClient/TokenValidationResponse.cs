﻿namespace Onactuate.Common.Utilities.RESTClient
{
    
    public class TokenValidationResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }   
        public string expires_in { get; set; }   
    }
}
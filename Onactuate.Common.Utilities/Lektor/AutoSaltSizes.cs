namespace Onactuate.Common.Utilities.Lektor
{
    public enum AutoSaltSizes:int
    {
        Salt32 = 4,
        Salt64 = 8,
        Salt128 = 16,
        Salt192 = 24,
        Salt256 = 32,
        Salt384 = 48,
        Salt512 = 64
    }
}
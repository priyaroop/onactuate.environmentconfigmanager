﻿using System.Collections.Specialized;

namespace Onactuate.Common.Utilities
{
    public interface ICookie
    {
        void Init(NameValueCollection nameValCol);
        void Init(string val);

        string Domain { get; }

        string this[string key] { get; set; }

        string Name { get; }

        string Path { get; }

        bool Secure { get; }
    }
}


using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.LoggingContextProviders;
using Onactuate.EnvironmentConfig.Web.Utility;
using Unity.Mvc5;

namespace Onactuate.EnvironmentConfig.Web
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            IUnityContainer container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            IUnityContainer container = new UnityContainer();

            // register all your components with the container here
            // e.g. container.RegisterType<ITestService, TestService>();   

            //For cloud admin, we want the logging provider that is used to be dynamic, we therefore
            //set up a factory method to resolve any request for ILoggingContextProvider.

            //NOTE::  If you specify a registration in the StandardCloud.config file, this factory method will not be called

            container.RegisterType<ILoggingContextProvider>(new InjectionFactory(uc => new StaticLoggingContextProvider("Cloud Administration" )));

            //container.ConfigureCloudRegistrations("MvcApplication");
            container.ConfigureCloudRegistrations("Onactuate.EnvironmentConfig.Web");

            //container.ResolveAll();
            //container.RegisterControllers();

            return container;
        }

        public static void Start()
        {
            Initialise();
        }
    }
}
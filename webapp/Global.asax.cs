﻿using System;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;
using Onactuate.Common.Logging;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.Extensions;
using Onactuate.Common.Logging.LoggingContextProviders;
using Onactuate.EnvironmentConfig.Web.Utility;

namespace Onactuate.EnvironmentConfig.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static bool _hasInited = false;
        private const string _loggingSource = "MvcApplication";
        private static CacheItemRemovedCallback OnCacheRemove = null;

        protected void Application_BeginRequest()
        {
            if (!System.Configuration.ConfigurationManager.AppSettings["EnableProfile"].AsBool(false)) return;
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // we are passing a factory to loggingActionFilter so that we
            // may create a loggingService at the time of the action
            // invocation. it is passed in at that time because we need
            // to include the httpcontext
            filters.Add(new LoggingActionFilter(
                            (httpcontext) =>
                            {
                                return new LoggingService(
                                    new HttpLoggingContextProvider(
                                        httpcontext,
                                        ApplicationLoggingConstants.ApplicationName));
                            }));
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "Default", // Route name`
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Account", action = "Login", id = "" },// Parameter defaults
                new[] { "Onactuate.EnvironmentConfig.Web.Controllers" }
                );
        }

        protected void ConfigureLogging()
        {
            string logFile = Server.MapPath("~/log4Net.config");
            if (System.IO.File.Exists(logFile))
            {
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(logFile));
            }
        }

        /// <summary>
        /// Invokes the HandleApplicationError to determine if the error is successfully
        /// handled within the application - otherwise will fall back to the
        /// web.config definitions for handling server errors / 404 errors
        /// </summary>
        protected void Application_Error()
        {
            ILoggingContextProvider loggingContextProvider = null;
            loggingContextProvider =
                    new StaticLoggingContextProvider(ApplicationLoggingConstants.ApplicationName);
            Exception lastException = Server.GetLastError();
            LoggingService loggingService = new LoggingService(loggingContextProvider);

            // HandleApplicatinError may generate an exception which will then
            // trigger the web.config's defaultredirect error page to be shown
            if (HandleApplicationError(
                lastException,
                loggingService,
                new HttpContextWrapper(HttpContext.Current),
                ControllerBuilder.Current.GetControllerFactory()))
            {
                Server.ClearError();
            }
        }

        /// <summary>
        /// Handles the application error - returns true if the application was succesfully
        /// handled by internal error handling. The internal error handler will ONLY
        /// handle the case of controller constructor/action exceptions by creating an error
        /// page with retry action. The retry action is the controller's default action. All other errors
        /// will not be handled here and will default to the web.config defaultredirect
        /// page for handling server errors. The retry action for the defaultredirect page is
        /// the homepage of the application
        /// </summary>
        /// <param name="lastException">The last exception.</param>
        /// <param name="loggingService">The logging service.</param>
        /// <param name="httpContextBase">The HTTP context base.</param>
        /// <returns></returns>
        public static bool HandleApplicationError(
            Exception lastException,
            ILoggingService loggingService,
            HttpContextBase httpContextBase,
            IControllerFactory controllerFactory)
        {
            // log base exception
            loggingService.Error(_loggingSource, LoggingMessageHelper.SerializeException(lastException));
            var innerException = lastException.InnerException;
            while (innerException != null)
            {
                // log inner exception
                loggingService.Error(
                    _loggingSource,
                    LoggingMessageHelper.SerializeException(innerException, "Inner Exception"));
                innerException = innerException.InnerException;
            }

            // in order to show the custom error page with dynamic retry action
            // the request must match to a MVC route and the request must not be a 404
            // status exception and the request must not be local health monitor
            // traffic(we want to show the real for that case error)
            MvcHandler mvcHandler = httpContextBase.CurrentHandler as MvcHandler;
            RequestContext requestContext = mvcHandler != null ? mvcHandler.RequestContext : null;
            HttpException httpException = lastException as HttpException;
            bool is404StatusException = httpException != null && httpException.GetHttpCode() == 404;

            if (requestContext != null && !is404StatusException)
            {
                string originalRouteInfo =
                    string.Format(
                        "Uncaught exception occured at Controller:{0} for Action:{1}",
                        requestContext.RouteData.Values["controller"],
                        requestContext.RouteData.Values["action"]);

                // lets log the original requestContext
                loggingService.Error(_loggingSource, originalRouteInfo);

                RequestContext errorRequestContext = new RequestContext();
                errorRequestContext.RouteData = new RouteData();
                errorRequestContext.HttpContext = httpContextBase;
                errorRequestContext.RouteData.Values["originalRouteData"] = requestContext.RouteData;
                errorRequestContext.RouteData.Values["originException"] = lastException;
                errorRequestContext.RouteData.Values["action"] = "Error";
                errorRequestContext.RouteData.Values["controller"] = "Error";

                // errors here will cause the application_error block to fail which will then
                // trigger the defaultredirect page behavior
                IController controller = controllerFactory.CreateController(errorRequestContext, "Error");

                controller.Execute(errorRequestContext);

                return true;
            }

            return false;
        }

        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();
            //RegisterGlobalFilters(GlobalFilters.Filters);
            //RegisterRoutes(RouteTable.Routes);
            AreaRegistration.RegisterAllAreas();
            IdentityConfig.RegisterIdentities();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            if (!_hasInited)
            {
                ConfigureLogging();
                _hasInited = true;
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            HttpContext.Current.User = null;
        }

        /// <summary>
        /// Occurs when a security module has verified user authorization.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void Application_AuthorizeRequest(Object sender, EventArgs args)
        {

        }
    }
}

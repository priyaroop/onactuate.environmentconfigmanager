﻿#region Using

using System.Web.Mvc;

#endregion

namespace Onactuate.EnvironmentConfig.Web
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
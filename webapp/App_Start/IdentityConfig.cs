﻿using Onactuate.EnvironmentConfig.Web.App_Helpers;

namespace Onactuate.EnvironmentConfig.Web
{
    public static class IdentityConfig
    {
        public static void RegisterIdentities()
        {
            // Ensures the default demo user is available to login with
            UserManager.Seed();
        }
    }
}
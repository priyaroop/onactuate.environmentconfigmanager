﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.Lektor;
using Onactuate.Common.Logging.Contracts;
using Onactuate.EnvironmentConfig.Web.Models;
using Onactuate.EnvironmentConfig.Web.Utility;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Platform.Services.HelperServices.AuthManager;

namespace Onactuate.EnvironmentConfig.Web.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        #region Private Member Variables
        
        private IAuthManagerService AuthManagerServiceObj { get; set; }
        private ILoggingService LoggingServiceObj { get; set; }
        private string _environmentName = EnvironmentSection.GetEnvironmentName;

        #endregion

        #region Class Constructor

        public AccountController(IAuthManagerService authManagerServiceObj, ILoggingService loggingServiceObj)
        {
            AuthManagerServiceObj = authManagerServiceObj;
            LoggingServiceObj = loggingServiceObj;
        }
        
        public AccountController(ILoggingService loggingService, IAuthManagerService authManagerServiceObj, ILoggingService loggingServiceObj) : base(loggingService)
        {
            AuthManagerServiceObj = authManagerServiceObj;
            LoggingServiceObj = loggingServiceObj;
        }

        #endregion

        #region Action Methods

        [HttpGet]
        [AllowAnonymous]
        public override ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        // GET: /account/login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            // Store the originating URL so we can attach it to a form field
            var viewModel = new AccountLoginModel { ReturnUrl = returnUrl };

            return View(viewModel);
        }

        // POST: /account/login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AccountLoginModel viewModel)
        {
            // Ensure we have a valid viewModel to work with
            if (!ModelState.IsValid)
                return View(viewModel);

            string password = viewModel.Password;
            byte[] encryptedData = EncryptionHelper.EncryptText(password, _environmentName);
            string encryptedPassword = EncryptionHelper.GetString(encryptedData);

            try
            {
                ApplicationPermissions applicationPermissions = AuthManagerServiceObj.IsAuthorizedUser(viewModel.Username, encryptedPassword);
                // If a user was found
                if (applicationPermissions != null)
                {
                    System.Web.HttpContext.Current.Session.Add("AppPermissionObj", applicationPermissions);
                    System.Web.HttpContext.Current.Session.Add("PermissionType", applicationPermissions.PermissionTypeId);
                    System.Web.HttpContext.Current.Session.Add("UserIdentity", viewModel.Username);

                    IdentityUser user = new IdentityUser(viewModel.Username);
                    // Then create an identity for it and sign it in
                    await SignInAsync(user, viewModel.RememberMe);

                    // If the user came from a specific page, redirect back to it
                    return RedirectToLocal(viewModel.ReturnUrl);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("LoginFailuer", e.Message);
            }

            // If we got this far, something failed, redisplay form
            return View(viewModel);
        }

        // GET: /account/error
        [AllowAnonymous]
        public ActionResult Error()
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            return View();
        }

        // POST: /account/Logout
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            // First we clean the authentication ticket like always
            FormsAuthentication.SignOut();

            // Second we clear the principal to ensure the user does not retain any authentication
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            // Last we redirect to a controller/action that requires authentication to ensure a redirect takes place
            // this clears the Request.IsAuthenticated flag since this triggers a new request
            return RedirectToLocal();
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            // If the return url starts with a slash "/" we assume it belongs to our site
            // so we will redirect to this "action"
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            if (Session["PermissionType"] != null)
            {
                switch (Session["PermissionType"].ToString())
                {
                    case "0":
                    case "1":
                    case "2":
                        return RedirectToAction("AdminPage", "Admin", new { Area="EnvConfig"});
                    case "3":
                    default:
                        ModelState.AddModelError("", "Access is Denied");
                        return RedirectToAction("Login", "Account", new { Area=""});
                }
            }

            // If we cannot verify if the url is local to our host we redirect to a default location
            return RedirectToAction("Login", "Account", new { Area=""});
        }

        private void AddErrors(DbEntityValidationException exc)
        {
            foreach (var error in exc.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors.Select(validationError => validationError.ErrorMessage)))
            {
                ModelState.AddModelError("", error);
            }
        }

        private void AddErrors(IdentityResult result)
        {
            // Add all errors that were returned to the page error collection
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private void EnsureLoggedOut()
        {
            // If the request is (still) marked as authenticated we send the user to the logout action
            if (Request.IsAuthenticated)
                Logout();
        }

        private async Task SignInAsync(IdentityUser user, bool isPersistent)
        {
            // Clear any lingering authencation data
            FormsAuthentication.SignOut();

            // Create a claims based identity for the current user
            //var identity = await _manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

            // Write the authentication cookie
            FormsAuthentication.SetAuthCookie(user.UserName, isPersistent);
        }


        /// <summary>
        /// Page to display the error message
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowError()
        {
            return View("ShowError", Request["ErrorMessage"]);
        }
        
        #endregion
    }
}
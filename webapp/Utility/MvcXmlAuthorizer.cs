﻿using System;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Xml.Serialization;

namespace Onactuate.EnvironmentConfig.Web.Utility
{
    public class MvcXmlAuthorizer : IMvcXmlAuthorizer
    {
        #region Private Class Variables

        private static readonly object _locker = new object();
        private static bool _isInitialized;
        public static ControllerAuthorizationInfoCollection _controllers;

        #endregion

        #region Class Constructors

        public MvcXmlAuthorizer()
        {
            Initilize();
        }

        #endregion

        #region IMvcXmlAuthorizer interface method implementation

        public bool IsAuthorized(String controllerName, IPrincipal user)
        {
            return _controllers.CanAccessController(controllerName, user);
        }

        public bool IsAuthorized(String controllerName, String actionName, IPrincipal user)
        {
            return _controllers.CanAccessAction(controllerName, actionName, user);
        }

        public ActionAuthorizationInfo GetAction(String controllerName, String actionName)
        {
            return _controllers.GetAction(controllerName, actionName);
        }
        
        public string GetParamsForControllerAction(string controllerName, string actionName, IPrincipal user)
        {
            ActionAuthorizationInfo actions = _controllers.First(info => info.Name.Equals(controllerName, StringComparison.OrdinalIgnoreCase)).FindAction(actionName);
            return actions.AdditionalParameters.Aggregate(string.Empty, (current, param) => current + string.Format("{0}={1},", param.Name, param.Value));
        }

        public string[] GetParamsArrayForControllerAction(string controllerName, string actionName, IPrincipal user)
        {
            ActionAuthorizationInfo actions = _controllers.First(info => info.Name.Equals(controllerName, StringComparison.OrdinalIgnoreCase)).FindAction(actionName);
            return actions.AdditionalParameters.Select(current => string.Format("{0}={1}", current.Name, current.Value)).ToArray();
        }

        #endregion

        #region Private Helper Methods

        private void Initilize()
        {
            if (_isInitialized) return;
            lock (_locker)
            {
                if (_isInitialized) return;

                AuthorizationMappingSection settings = AuthorizationMappingSection.GetSettings();
                String path = HttpContext.Current.Server.MapPath(settings.Path);
                _controllers = GetControllerAuthorizationInfoCollection(path);
                _isInitialized = true;
            }
        }

        private ControllerAuthorizationInfoCollection GetControllerAuthorizationInfoCollection(String path)
        {
            ControllerAuthorizationInfoCollection rVal = null;
            XmlSerializer ser = new XmlSerializer(typeof(ControllerAuthorizationInfoCollection));

            using (FileStream fs = File.OpenRead(path))
            {
                rVal = (ControllerAuthorizationInfoCollection)ser.Deserialize(fs);
            }

            return rVal;
        }

        #endregion
    }
}
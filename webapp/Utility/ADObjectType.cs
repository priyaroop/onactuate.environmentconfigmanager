﻿namespace Onactuate.EnvironmentConfig.Web.Utility
{
    public enum ADObjectType
    {
        UsersAndGroups,
        UsersOnly,
        OneUserOnly
    }
}
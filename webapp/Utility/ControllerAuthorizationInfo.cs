﻿using System;
using System.Xml.Serialization;

namespace Onactuate.EnvironmentConfig.Web.Utility
{
    [XmlType(TypeName = "controller")]
    public class ControllerAuthorizationInfo : AuthorizationInfo
    {
        /// <summary>
        /// Gets or sets the actions.
        /// </summary>
        /// <value>The actions.</value>
        [XmlArray(ElementName = "actions"), XmlArrayItem(ElementName = "action")]
        public ActionAuthorizationInfo[] Actions { get; set; }

        internal ActionAuthorizationInfo FindAction(String actionName)
        {
            if (Actions != null)
            {
                foreach (ActionAuthorizationInfo info in Actions)
                {
                    if (String.Compare(info.Name, actionName, true) == 0)
                    {
                        return info;
                    }
                }
            }

            return null;
        }
    }
}
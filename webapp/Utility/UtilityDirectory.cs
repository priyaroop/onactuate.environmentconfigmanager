﻿using System.Configuration;
using System.DirectoryServices;

namespace Onactuate.EnvironmentConfig.Web.Utility
{
    public class UtilityDirectory
    {
        internal static DirectoryEntry NewDirectoryEntry(string path)
        {
            return new DirectoryEntry(
                    path,
                    ConfigurationManager.AppSettings["Active Directory LookUp Username"],
                    ConfigurationManager.AppSettings["Active Directory LookUp Password"]);
        }
    }
}
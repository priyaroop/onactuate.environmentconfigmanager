﻿using System.Xml.Serialization;

namespace Onactuate.EnvironmentConfig.Web.Utility
{
    [XmlType(TypeName = "param")]
    public class AdditionalParameter
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        [XmlAttribute("value")]
        public string Value { get; set; }

    }
}

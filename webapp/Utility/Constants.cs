﻿using System.Collections.Generic;

namespace Onactuate.EnvironmentConfig.Web.Utility
{
    public static class Constants
    {
        public static class Controllers
        {
            public const string HomeController = "Home";
            public const string AdminController = "Admin";
            public const string EnvConfigController = "EnvConfig";
        }

        public static class ControllerActions
        {
            public const string Dashboard = "Dashboard";
            public const string AdminPage = "AdminPage";
            public const string DefaultPage = "Default";
            public const string EditEnvironmentPage = "EditEnvironment";
            public const string EnvironmentPermissionsPage = "EnvironmentPermissions";
        }

        public const string ExpandIconImageName = "expand.png";
        public const string ExpandIconNoActionImageName = "cannotToggle.png";
        public const string CollapseIconImageName = "collapse.png";

        public const string ActionForExpand = "expand";
        public const string ActionForCollapse = "collapse";

        public static readonly List<string> DisplayImageList = new List<string>() { "product.png", "productLineItem.png", "user.png" };
        public static readonly List<string> CssStyleClasses = new List<string>() { "css_grid_rowLevelOne", "css_grid_rowLevelTwo", "css_grid_rowLevelThree" };

        public const string FlexigridRowIdListPost = "rowidlist";
        public const string FlexigridActionTypeListPost = "actiontypelist";

        public const int MaxQuestionsInQuiz = 10;
    }
}
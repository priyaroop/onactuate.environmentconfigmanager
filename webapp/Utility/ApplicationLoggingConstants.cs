﻿namespace Onactuate.EnvironmentConfig.Web.Utility
{
    /// <summary>
    /// Application Constants
    /// </summary>
    public static class ApplicationLoggingConstants
    {
        public const string UserCookie = "CloudAdminPortal-LoggedUser";

        #region Logging constants
        /// <summary>
        /// Name of the application - all logs written from this app will share this application name
        /// </summary>
        public const string ApplicationName = "CloudAdminPortal";

        /// <summary>
        /// Source Name - Used for logging cloudintegration service calls
        /// </summary>
        public const string CloudIntegrationService = "CloudIntegrationService";

        /// <summary>
        /// Source Name - Used for logging email service calls
        /// </summary>
        public const string EmailService = "EmailService";

        /// <summary>
        /// Source Name - Used for logging account service calls
        /// </summary>
        public const string AccountService = "AccountService";

        /// <summary>
        /// Source Name - Used for activation service calls
        /// </summary>
        public const string ActivationService = "ActivationService";

        /// <summary>
        /// Source Name - Used for registration service calls
        /// </summary>
        public const string RegistrationService = "RegistrationService";

        /// <summary>
        /// Source Name - User Account service calls
        /// </summary>
        public const string UserAccountService = "UserAccountService";

        /// <summary>
        /// Source Name - Address validation service calls
        /// </summary>
        public const string AddressValidationService = "AddressValidationService";
        #endregion
    }
}
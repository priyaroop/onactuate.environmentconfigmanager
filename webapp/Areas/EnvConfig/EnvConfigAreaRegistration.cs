﻿using System.Web.Mvc;

namespace Onactuate.EnvironmentConfig.Web.Areas.EnvConfig
{
    public class EnvConfigAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EnvConfig";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EnvConfig_default",
                "EnvConfig/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
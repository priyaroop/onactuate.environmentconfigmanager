﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.Lektor;
using Onactuate.Common.DataAccess;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Utilities.FlexiGridExtensions;
using Onactuate.EnvironmentConfig.Web.Controllers;
using Onactuate.EnvironmentConfig.Web.UserStateManagement;
using Onactuate.EnvironmentConfig.Web.Utility;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Platform.Services.HelperServices.AuthManager;
using Onactuate.Webportal.Platform.Services.SqlTableServices;

namespace Onactuate.EnvironmentConfig.Web.Areas.EnvConfig.Controllers
{
    public class AdminController : BaseController
    {
        #region Private Member Variables

        private IAdminUserState AdminUserStateObj { get; set; }
        private IAuthManagerService AuthManagerServiceObj { get; set; }
        private IEnvironmentMasterService EnvironmentMasterServiceObj { get; set; }
        private IEnvironmentPermissionMasterService EnvironmentPermissionMasterServiceObj { get; set; }

        private string _environmentName = EnvironmentSection.GetEnvironmentName;
        #endregion

        #region Class Constructor

        public AdminController(ILoggingService loggingService, IAdminUserState adminUserStateObj, IAuthManagerService authManagerServiceObj, IEnvironmentMasterService environmentMasterServiceObj, IEnvironmentPermissionMasterService environmentPermissionMasterServiceObj) : base(loggingService)
        {
            AdminUserStateObj = adminUserStateObj;
            AuthManagerServiceObj = authManagerServiceObj;
            EnvironmentMasterServiceObj = environmentMasterServiceObj;
            EnvironmentPermissionMasterServiceObj = environmentPermissionMasterServiceObj;
        }

        #endregion

        #region Default Index Page

        [EnsureAdminAccess(Controller = Constants.Controllers.AdminController, Action = Constants.ControllerActions.AdminPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpGet]
        public override ActionResult Index()
        {
            return RedirectToAction("AdminPage");
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.AdminController, Action = Constants.ControllerActions.AdminPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpGet]
        public ActionResult AdminPage()
        {
            IList<ApplicationPermissions> viewModel = AuthManagerServiceObj.GetApplicationPermissionsList();
            return View("AdminPage", viewModel);
        }

        #endregion

        #region Application Permissions

        [EnsureAdminAccess(Controller = Constants.Controllers.AdminController, Action = Constants.ControllerActions.AdminPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult SaveNewPermission(string username, string password, int permissionType)
        {
            try
            {
                ApplicationPermissions permission = new ApplicationPermissions
                {
                    UserName = username,
                    Password = EncryptionHelper.GetString(EncryptionHelper.EncryptText(password, _environmentName)),
                    PermissionTypeId = permissionType,
                    IsDeletable = true
                };

                AuthManagerServiceObj.CreateApplicationPermissions(permission);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error :: " + ex.InnerException.Message);
            }

            return RedirectToAction("AdminPage", "Admin", new { @mode = 1 });
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.AdminController, Action = Constants.ControllerActions.AdminPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult DeleteUserPermission(FormCollection collection)
        {
            try
            {
                int permId = Convert.ToInt32(collection["permissionId"]);

                AuthManagerServiceObj.DeleteApplicationPermissionByPermId(permId);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error :: " + ex.InnerException.Message);
            }

            return RedirectToAction("AdminPage", "Admin", new { @mode = 1 });
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.AdminController, Action = Constants.ControllerActions.AdminPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public void ChangeUserPermissions(int userId)
        {
            try
            {
                ApplicationPermissions permission = AuthManagerServiceObj.GetApplicationPermissionsById(userId);
                permission.PermissionTypeId = (permission.PermissionTypeId + 1) % 3;

                AuthManagerServiceObj.SaveApplicationPermissions(permission);

            }
            catch (Exception ex)
            { }
        }

        #endregion

        #region Environment Permissions

        [EnsureAccess(Controller = Constants.Controllers.HomeController, Action = Constants.ControllerActions.EnvironmentPermissionsPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpGet]
        public ActionResult EnvironmentPermissions()
        {
            int permissionId = ((ApplicationPermissions)System.Web.HttpContext.Current.Session["AppPermissionObj"]).PermissionId;
            IEnumerable<EnvironmentPermissionMaster> envListWhereUserIsAdmin = EnvironmentPermissionMasterServiceObj.GetEnvironmentPermissionMasterListByPermissionId(permissionId).Where(p=> p.IsAdmin);
            IEnumerable<EnvironmentPermissionMaster> envListWhereUserIsNotAdmin = EnvironmentPermissionMasterServiceObj.GetEnvironmentPermissionMasterListByPermissionId(permissionId).Where(p=> !p.IsAdmin);
            IEnumerable<EnvironmentPermissionMaster> masterEnvList = EnvironmentPermissionMasterServiceObj.GeEnvironmentPermissionMasterList();

            // Show all permissions of all environments where the current user is admin
            List<EnvironmentPermissionMaster> envList = envListWhereUserIsAdmin.SelectMany(obj => masterEnvList.Where(p => p.EnvironmentId.Equals(obj.EnvironmentId))).ToList();
            
            // Show permission of all environments where the current user is not admin
            envList.AddRange(envListWhereUserIsNotAdmin);

            return View("EnvironmentPermissions", envList);
        }

        [EnsureAccess(Controller = Constants.Controllers.HomeController, Action = Constants.ControllerActions.EnvironmentPermissionsPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult GetUserList(int envId)
        {
            IList<ApplicationPermissions> permList = AuthManagerServiceObj.GetApplicationPermissionsListByEnvironment(envId);
            return Json(permList, JsonRequestBehavior.AllowGet);
        }

        [EnsureAccess(Controller = Constants.Controllers.HomeController, Action = Constants.ControllerActions.EnvironmentPermissionsPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult CreateEnvironmentPermission(int envId, int permId,bool isAdmin)
        {
            try
            {
                EnvironmentPermissionMaster master = new EnvironmentPermissionMaster()
                {
                    EnvironmentId = envId,
                    PermissionId = permId,
                    IsAdmin = isAdmin
                };

                EnvironmentPermissionMasterServiceObj.CreateEnvironmentPermissionMaster(master);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error :: " + ex.InnerException.Message);
            }

            return RedirectToAction("EnvironmentPermissions", "Admin", new {Area = "EnvConfig" });
        }

        [EnsureAccess(Controller = Constants.Controllers.HomeController, Action = Constants.ControllerActions.EnvironmentPermissionsPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult DeleteEnvironmentPermission(int permId)
        {
            try
            {
                EnvironmentPermissionMasterServiceObj.DeleteEnvironmentPermissionMasterById(permId);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error :: " + ex.InnerException.Message);
            }

            return RedirectToAction("EnvironmentPermissions", "Admin", new { Area = "EnvConfig" });
        }

        #endregion
    }
}

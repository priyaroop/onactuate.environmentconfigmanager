﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.Lektor;
using Onactuate.Common.DataAccess;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Utilities.FlexiGridExtensions;
using Onactuate.EnvironmentConfig.Web.Controllers;
using Onactuate.EnvironmentConfig.Web.Models;
using Onactuate.EnvironmentConfig.Web.UserStateManagement;
using Onactuate.EnvironmentConfig.Web.Utility;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Platform.Services.SqlTableServices;

namespace Onactuate.EnvironmentConfig.Web.Areas.EnvConfig.Controllers
{
    public class EnvConfigController : BaseController
    {
        #region Private Member Variables

        private IAdminUserState AdminUserStateObj { get; set; }

        private IEnvironmentMasterService EnvironmentMasterServiceObj { get; set; }
        private ITokenMasterService TokenMasterServiceObj { get; set; }
        private IKeyMasterService KeyMasterServiceObj { get; set; }
        
        private string _environmentName = EnvironmentSection.GetEnvironmentName;

        #endregion

        #region Class Constructor

        public EnvConfigController(ILoggingService loggingService, IAdminUserState adminUserStateObj,
            IEnvironmentMasterService environmentMasterServiceObj, ITokenMasterService tokenMasterServiceObj,
            IKeyMasterService keyMasterServiceObj) : base(loggingService)
        {
            AdminUserStateObj = adminUserStateObj;
            EnvironmentMasterServiceObj = environmentMasterServiceObj;
            TokenMasterServiceObj = tokenMasterServiceObj;
            KeyMasterServiceObj = keyMasterServiceObj;
        }

        #endregion

        #region Environment Master

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.DefaultPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpGet]
        public override ActionResult Index()
        {
            return RedirectToAction("Default");
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.DefaultPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpGet]
        public ActionResult Default()
        {
            EnvironmentViewModel viewModel = new EnvironmentViewModel
            {
                PossibleEnvironmentValues = GetEnvironmentMasterListByPermissionId()
            };

            return View("Default", viewModel);
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.DefaultPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult CreateNewEnvironment(EnvironmentViewModel viewModel)
        {
            try
            {
                EnvironmentMaster master = new EnvironmentMaster()
                {
                    EnvironmentName = viewModel.EnvironmentName,
                    EnvironmentDescription = viewModel.EnvironmentDescription
                };

                int permissionId = ((ApplicationPermissions)System.Web.HttpContext.Current.Session["AppPermissionObj"]).PermissionId;
                EnvironmentPermissionMaster permissions = new EnvironmentPermissionMaster()
                {
                    PermissionId = permissionId,
                    IsAdmin = true
                };

                EnvironmentMasterServiceObj.CreateEnvironmentMaster(master, permissions);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error while saving - " + ex.Message);
                if (ex.InnerException != null)
                {
                    AddErrorMessage(ex.InnerException.Message);
                }
            }

            return RedirectToAction("Default");
        }
        
        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.DefaultPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult CloneEnvironment(FormCollection collection)
        {
            try
            {
                int environmentIdToClone = Convert.ToInt32(collection["EnvironmentIdToClone"]);
                EnvironmentMaster environmentMaster = new EnvironmentMaster()
                {
                    EnvironmentName = collection["EnvironmentNameToClone"],
                    EnvironmentDescription = collection["EnvironmentDescriptionToClone"]
                };

                int permissionId = ((ApplicationPermissions)System.Web.HttpContext.Current.Session["AppPermissionObj"]).PermissionId;
                EnvironmentPermissionMaster permissions = new EnvironmentPermissionMaster()
                {
                    PermissionId = permissionId,
                    IsAdmin = true
                };

                EnvironmentMasterServiceObj.CloneEnvironmentMaster(environmentIdToClone, environmentMaster, permissions);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error while saving - " + ex.Message);
                if (ex.InnerException != null)
                {
                    AddErrorMessage(ex.InnerException.Message);
                }
            }

            return RedirectToAction("Default");
        }

        
        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.DefaultPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult DeleteEnvironment(FormCollection collection)
        {
            try
            {
                int environmentIdToDelete = Convert.ToInt32(collection["EnvironmentIdToDelete"]);
                EnvironmentMaster environmentMaster = EnvironmentMasterServiceObj.GetEnvironmentMasterByEnvironmentId(environmentIdToDelete);

                if (environmentMaster != null)
                {
                    EnvironmentMasterServiceObj.DeleteEnvironmentMaster(environmentMaster);
                }
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error while deleting - " + ex.Message);
                if (ex.InnerException != null)
                {
                    AddErrorMessage(ex.InnerException.Message);
                }
            }

            return RedirectToAction("Default");
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.DefaultPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult CheckUniqueEnvironmentName(FormCollection collection)
        {
            string envName = collection["EnvironmentName"];
            EnvironmentMaster environment = EnvironmentMasterServiceObj.GetEnvironmentMasterByEnvironmentName(envName);

            return Json(environment == null ? new {isUniqueName = true} : new {isUniqueName = false});
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.EditEnvironmentPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        public ActionResult EditEnvironment(int selectedEnvironmentId)
        {
            try
            {
                EnvironmentMaster environment = EnvironmentMasterServiceObj.GetEnvironmentMasterByEnvironmentId(selectedEnvironmentId);
                IList<KeyMaster> keysList = KeyMasterServiceObj.GetKeyMasterListByEnvironmentId(selectedEnvironmentId).ToList();

                if (keysList.Count == 0)
                {
                    KeyMaster dummyKey = new KeyMaster()
                    {
                        KeyId = int.MinValue,
                        KeyName = "DummyData",
                        KeyValue = "DummyData",
                        IsEncrypted = false,
                        TokenId = int.MinValue,
                        TokenMasterObj = new TokenMaster()
                        {
                            TokenId = int.MinValue,
                            TokenName = "DummyData",
                            TokenDescription = "DummyData",
                            EnvironmentMasterObj = environment,
                            EnvironmentId = environment.EnvironmentId
                        }
                    };

                    keysList = new[] {dummyKey};
                }

                return View("EditEnvironment", keysList);
            }
            catch (Exception ex)
            {
                AddErrorMessage("Error while saving - " + ex.Message);
                if (ex.InnerException != null)
                {
                    AddErrorMessage(ex.InnerException.Message);
                }
            }

            return RedirectToAction("Login", "Account", new { Area=""});
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController, Action = Constants.ControllerActions.EditEnvironmentPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult CreateNewKey(FormCollection collection)
        {
            int environmentId = Convert.ToInt32(collection["EnvIdToCreate"]);
            string tokenName = collection["TokenNameToCreate"];
            string keyName = collection["KeyNameToCreate"];
            string keyValue = collection["KeyValueToCreate"];
            bool isEncrypted = Convert.ToBoolean(collection["IsEncryptedToCreate"]);

            if (isEncrypted)
            {
                byte[] encryptedData = EncryptionHelper.EncryptText(keyValue,_environmentName);
                keyValue = EncryptionHelper.GetString(encryptedData);
            }

            TokenMaster token = TokenMasterServiceObj.GetTokenMasterByEnvironmentIdAndTokeName(environmentId, tokenName);

            if (token == null)
            {
                token = new TokenMaster
                {
                    TokenName = tokenName,
                    TokenDescription = tokenName,
                    EnvironmentId = environmentId
                };

                TokenMasterServiceObj.CreateTokenMaster(token);
            }

            KeyMaster keyMaster = new KeyMaster
            {
                TokenId = token.TokenId,
                KeyName = keyName,
                KeyValue = keyValue,
                IsEncrypted = isEncrypted
            };
            KeyMasterServiceObj.CreateKeyMaster(keyMaster);

            return RedirectToAction("EditEnvironment", new { selectedEnvironmentId = environmentId });
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController,
            Action = Constants.ControllerActions.EditEnvironmentPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpGet]
        public ActionResult DeleteEntry(int entryNo)
        {
            return PartialView("DeleteEntry", entryNo);
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController,
            Action = Constants.ControllerActions.EditEnvironmentPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult DeleteEntryPost(int entryNo)
        {
            KeyMaster keyMaster = KeyMasterServiceObj.GetKeyMasterByKeyId(entryNo);
            int environmentId = keyMaster.TokenMasterObj.EnvironmentId;
            KeyMasterServiceObj.DeleteKeyMasterByKeyId(entryNo);

            return RedirectToAction("EditEnvironment", new { selectedEnvironmentId = environmentId });
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController,
            Action = Constants.ControllerActions.EditEnvironmentPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult DeleteMultipleEntries(int[] entryNo)
        {
            KeyMaster keyMaster = KeyMasterServiceObj.GetKeyMasterByKeyId(entryNo[0]);
            int environmentId = keyMaster.TokenMasterObj.EnvironmentId;
            KeyMasterServiceObj.DeleteKeyMasterByKeyIdList(entryNo.ToList());

            return RedirectToAction("EditEnvironment", new { selectedEnvironmentId = environmentId });
        }

        [EnsureAdminAccess(Controller = Constants.Controllers.EnvConfigController,
            Action = Constants.ControllerActions.EditEnvironmentPage, DoLogout = true)]
        [EnsureSessionAlive]
        [Authorize]
        [HttpPost]
        public ActionResult EditEntry(FormCollection collection)
        {
            KeyMaster keyToUpdate = KeyMasterServiceObj.GetKeyMasterByKeyId(Convert.ToInt32(collection["KeyIdToEdit"]));
            keyToUpdate.KeyValue = collection["KeyValueToEdit"];
            keyToUpdate.IsEncrypted = Convert.ToBoolean(collection["IsEncryptedToEdit"]);

            if (keyToUpdate.IsEncrypted)
            {
                byte[] encryptedData = EncryptionHelper.EncryptText(keyToUpdate.KeyValue, _environmentName);
                keyToUpdate.KeyValue = EncryptionHelper.GetString(encryptedData);
            }

            KeyMasterServiceObj.UpdateKeyMaster(keyToUpdate);

            return RedirectToAction("EditEnvironment", new { selectedEnvironmentId = keyToUpdate.TokenMasterObj.EnvironmentId });
        }

        #endregion

        #region Private Helper Methods

        private IEnumerable<EnvironmentMaster> GetEnvironmentMasterListByPermissionId()
        {
            IList<EnvironmentMaster> tempList = new List<EnvironmentMaster>();
            tempList.Add(new EnvironmentMaster()
            {
                EnvironmentId = int.MinValue,
                EnvironmentName = "-- Select an Environment --",
                EnvironmentDescription = string.Empty
            });

            int permissionId = ((ApplicationPermissions)System.Web.HttpContext.Current.Session["AppPermissionObj"]).PermissionId;
            foreach (EnvironmentMaster environmentMaster in EnvironmentMasterServiceObj.GetEnvironmentMasterListByPermissionId(permissionId))
            {
                tempList.Add(environmentMaster);
            }

            return tempList;
        }
        
        #endregion
    }
}

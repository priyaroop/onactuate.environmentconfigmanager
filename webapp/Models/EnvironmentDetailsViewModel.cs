﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class EnvironmentDetailsViewModel
    {
        public EnvironmentMaster EnvironmentMasterObj { get; private set; } 
        public IEnumerable<TokenMaster> TokenMasterListObj { get; private set; }
        public IEnumerable<KeyMaster> KeyMasterListObj { get; private set; }

        public EnvironmentDetailsViewModel(EnvironmentMaster environmentMasterObj, IEnumerable<TokenMaster> tokenMasterListObj, IEnumerable<KeyMaster> keyMasterListObj)
        {
            EnvironmentMasterObj = environmentMasterObj;
            TokenMasterListObj = tokenMasterListObj;
            KeyMasterListObj = keyMasterListObj;
        }
    }
}
﻿using System.ComponentModel;

namespace Onactuate.DocumentServer.Web.Models
{
    public class Discussion
    {
        public string Subject { get; set; }

        public string Body { get; set; }

        [DisplayName("Question")]
        public bool IsQuestion { get; set; }

        public Category Category { get; set; }
    }
}

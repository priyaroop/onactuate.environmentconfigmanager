﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class TaskMasterViewModel
    {
        public IEnumerable<TaskData> TaskDataList { get; set; }

        public int TotalTasksInInbox { get; set; }
        public int SentForApprovalTasks { get; set; }
        public int ApprovedTasks { get; set; }
        public int RejectedTasks { get; set; }
        public int HistoryTasks { get; set; }
        public int TasksToApprove { get; set; }

        public TaskStatus CurrentShowing { get; set; }
    }
}
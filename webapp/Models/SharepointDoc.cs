﻿namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class SharepointDoc
    {
        public string DocumentName { get; set; }
        public string DocumentType { get; set; }
    }
}
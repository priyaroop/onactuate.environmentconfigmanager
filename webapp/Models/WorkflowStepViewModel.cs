﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class WorkflowStepViewModel
    {
        public WorkflowStep WorkflowStep { get; set; }
        public List<WorkflowStepApproverViewModel> WorkflowStepApprovers { get; set; }
    }
}
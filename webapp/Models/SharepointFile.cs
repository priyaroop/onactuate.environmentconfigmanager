﻿using System;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    [Serializable]
    public class SharepointFile
    {
        public string FileName { get; private set; }
        public string ServerRelativeUrl { get; private set; }
        public string ModifiedBy { get; private set; }
        public DateTime TimeCreated { get; private set; }
        public DateTime TimeLastModified { get; private set; }
        public bool IsCheckedOut { get; private set; }

        public SharepointFile(string fileName, string serverRelativeUrl, string modifiedBy, DateTime timeCreated, DateTime timeLastModified, bool isCheckedOut)
        {
            FileName = fileName;
            ServerRelativeUrl = serverRelativeUrl;
            ModifiedBy = modifiedBy;
            TimeCreated = timeCreated;
            TimeLastModified = timeLastModified;
            IsCheckedOut = isCheckedOut;
        }

        public override string ToString()
        {
            return $"{nameof(FileName)}: {FileName}, {nameof(ServerRelativeUrl)}: {ServerRelativeUrl}, {nameof(ModifiedBy)}: {ModifiedBy}, {nameof(TimeCreated)}: {TimeCreated}, {nameof(TimeLastModified)}: {TimeLastModified}, {nameof(IsCheckedOut)}: {IsCheckedOut}";
        }
    }
}
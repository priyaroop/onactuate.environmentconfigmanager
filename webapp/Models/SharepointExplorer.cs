﻿using System;
using System.Collections.Generic;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    [Serializable]
    public class SharepointExplorer
    {
        public List<SharepointFolder> SharepointFolders { get; private set; }
        public List<SharepointFile> SharepointFiles { get; private set; }

        public SharepointExplorer(List<SharepointFolder> sharepointFolders, List<SharepointFile> sharepointFiles)
        {
            SharepointFolders = sharepointFolders;
            SharepointFiles = sharepointFiles;
        }

        public override string ToString()
        {
            return $"{nameof(SharepointFolders)}: {SharepointFolders}, {nameof(SharepointFiles)}: {SharepointFiles}";
        }
    }
}
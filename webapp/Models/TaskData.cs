﻿using System;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class TaskData
    {
        public int Id { get; set; }
        public string TaskName { get; set; }
        public int OwnerPermissionId { get; set; }
        public int AssignedToPermissionId { get; set; }
        public DateTime CompletionDate { get; set; }
        public TaskStatus TaskStatusId { get; set; }
        public bool IsNewTask { get; set; }
        public string TaskFileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TaskOwnerName { get; set; }
    }
}
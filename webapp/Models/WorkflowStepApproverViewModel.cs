﻿namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class WorkflowStepApproverViewModel
    {
        public int StepApproverId { get; set; }
        public string ApproverName { get; set; }
        public int PermissionId { get; set; }
        public int WorkflowStepId { get; set; }
    }
}
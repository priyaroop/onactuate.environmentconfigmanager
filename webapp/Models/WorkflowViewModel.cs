﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class WorkflowViewModel
    {
        public WorkflowMaster Workflow { get; set; }
        public List<WorkflowStepViewModel> WorkflowSteps { get; set; }
    }
}
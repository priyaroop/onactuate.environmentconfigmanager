﻿using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class ApproverViewModel
    {
        public string ApproverName { get; set; }
        public WorkflowStepApprover StepApprover { get; set; }
    }
}
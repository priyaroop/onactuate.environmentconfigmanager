﻿namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class UserViewModel
    {
        public string Username { get; set; }
        public int PermissionId { get; set; }
    }
}
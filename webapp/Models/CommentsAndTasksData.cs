﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class CommentsAndTasksData
    {
        public IList<ApplicationPermissions> PermissionsList { get; private set; }
        public List<TaskComment> CommentsList { get; private set; }

        public List<TaskMaster> TasksList { get; private set; }

        public CommentsAndTasksData(IList<ApplicationPermissions> permissionsList, List<TaskComment> commentsList, List<TaskMaster> tasksList)
        {
            PermissionsList = permissionsList;
            CommentsList = commentsList;
            TasksList = tasksList;
        }
    }
}
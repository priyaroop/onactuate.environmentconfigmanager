﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class EnvironmentViewModel
    {
        [Required(ErrorMessage = "Environment Name is required")]
        [DataType(DataType.Text)]
        public string EnvironmentName { get; set; }

        [DataType(DataType.Text)]
        public string EnvironmentDescription { get; set; }

        /// <summary>
        /// List of Environments from Environment Master
        /// </summary>
        public IEnumerable<EnvironmentMaster> PossibleEnvironmentValues { get; set; }
    }
}
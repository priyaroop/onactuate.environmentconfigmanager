﻿using System;

namespace Onactuate.EnvironmentConfig.Web.Models
{
    [Serializable]
    public class SharepointFolder
    {
        public string FolderName { get; private set; }
        public string ServerRelativeUrl { get; private set; }
        public DateTime TimeCreated { get; private set; }
        public DateTime TimeLastModified { get; private set; }

        public SharepointFolder(string folderName, string serverRelativeUrl, DateTime timeCreated, DateTime timeLastModified)
        {
            FolderName = folderName;
            ServerRelativeUrl = serverRelativeUrl;
            TimeCreated = timeCreated;
            TimeLastModified = timeLastModified;
        }

        public override string ToString()
        {
            return $"{nameof(FolderName)}: {FolderName}, {nameof(ServerRelativeUrl)}: {ServerRelativeUrl}, {nameof(TimeCreated)}: {TimeCreated}, {nameof(TimeLastModified)}: {TimeLastModified}";
        }
    }
}
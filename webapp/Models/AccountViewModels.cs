﻿#region Using

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

#endregion

namespace Onactuate.EnvironmentConfig.Web.Models
{
    public class AccountLoginModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }
}
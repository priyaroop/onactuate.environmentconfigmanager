﻿//Function to add an entry
function AddEntry() {
    var Feature_Id = $('#insertId').val();
    var Feature_Group_Id = $('#insertGrpId').val();
    var Feature_Name = $('#insertName').val();
    var Key_Feature_Dependency = $('#insertKFDependency').val();
    var Feature_Priority = $('#insertPriority').val();

    $.ajax({
        url: '/FeatureIndex/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'Feature_Id': Feature_Id, 'Feature_Group_Id': Feature_Group_Id, 'Feature_Name': Feature_Name, 'Key_Feature_Dependency': Key_Feature_Dependency, 'Feature_Priority': Feature_Priority },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });


    var featureIndex = {
        'EntryNo': EntryNo,
        'Feature_Id': tdArray[1],
        'Feature_Group_Id': tdArray[2],
        'Feature_Name': tdArray[3],
        'Key_Feature_Dependency': tdArray[4],
        'Feature_Priority': tdArray[5]
    }


    row.attr('name', EntryNo);
    EditRowReady(featureIndex);
    //var html = $('#editRow td');

    //row.empty();
    //row.append(html);
    //$.ajax({
    //    url: 'home/EditTimesheet',
    //    type: 'get',
    //    dataType: 'html',
    //    data: {'EntryNo' : EntryNo},
    //    success: function (response) { extractRow(response);}
    //});

    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        //var rowtoEdit = row.children('table').children('tbody').children('tr').children('td');
        //row.empty();
        row.attr('name', EntryNo);
        //row.append(rowtoEdit);
    }

}

//function to get values inside controls created during edit
function EditRowReady(FeatureIndex) {
    $('#editFeature_Id').val(FeatureIndex.Feature_Id)
    $('#editFeature_Group_Id').val(FeatureIndex.Feature_Group_Id)
    $('#editFeature_Name').val(FeatureIndex.Feature_Name);
    $('#editKey_Feature_Dependency').val(FeatureIndex.Key_Feature_Dependency)
    $('#editFeature_Priority').val(FeatureIndex.Feature_Priority);


    var tdFeature_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 149px;"><input type="text" readonly="readonly"  value="' + FeatureIndex.Feature_Id + '" name="editFeature_Id" id="editFeature_Id" style="width:149px"></div></td>';

    var tdFeature_Group_Id = '<td align="right"><div style="text-align: right; width: 149px;"><input type="text" value="' + FeatureIndex.Feature_Group_Id + '" name="editFeature_Group_Id"  id="editFeature_Group_Id" style="width:149px"> </div></td>';

    var tdFeature_Name = '<td align="right"><div style="text-align: right; width: 349px;"><input type="text" value="' + FeatureIndex.Feature_Name + '" name="editFeature_Name"  id="editFeature_Name" style="width:349px"> </div></td>';

    var tdKey_Feature_Dependency = '<td align="center"><div style="text-align: center; width: 149px;"><input type="text"  value="' + FeatureIndex.Key_Feature_Dependency + '" name="editKey_Feature_Dependency" id="editKey_Feature_Dependency" style="width:149px"></div></td>';

    var tdFeature_Priority = '<td align="right"><div style="text-align: right; width: 99px;"><input type="text" value="' +FeatureIndex.Feature_Priority + '" name="editFeature_Priority"  id="editFeature_Priority" style="width:99px"> </div></td>';


    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/Images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + FeatureIndex.EntryNo).closest('tr');
    row.empty();
    row.append(tdFeature_Id);
    row.append(tdFeature_Group_Id);
    row.append(tdFeature_Name);
    row.append(tdKey_Feature_Dependency);
    row.append(tdFeature_Priority);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var Feature_Id = $('#editFeature_Id').val();
    var Feature_Group_Id = $('#editFeature_Group_Id').val();
    var Feature_Name = $('#editFeature_Name').val();
    var Key_Feature_Dependency = $('#editKey_Feature_Dependency').val();
    var Feature_Priority = $('#editFeature_Priority').val();

    //if (Description2 == '' || Description2 == 'undefined') {
    //    Description2 = 'NA';
    //}

    //if (Reference == '' || Reference == 'undefined') {
    //    Reference = 'NA';
    //}

    var featureIndex = {
        //'EntryNo': EntryToEdit,
        'Feature_Id': Feature_Id,
        'Feature_Group_Id': Feature_Group_Id,
        'Feature_Name': Feature_Name,
        'Key_Feature_Dependency': Key_Feature_Dependency,
        'Feature_Priority': Feature_Priority
    }

    $.ajax({
        url: '/FeatureIndex/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: featureIndex,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
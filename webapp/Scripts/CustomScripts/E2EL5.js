﻿//Function to add an entry
function AddEntry() {
    var E2E_Id = $('#insertE2EId').val();
    var L5_Id = $('#insertL5Id').val();
    //var Feature_Id = $('#insertFeatureId').val();

    $.ajax({
        url: '/E2EL5Mapping/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'E2E_Id': E2E_Id, 'L5_Id': L5_Id },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });


    var E2EProcessIndex = {
        'EntryNo': EntryNo,
        'E2E_Id': tdArray[1],
        'E2E_Name': tdArray[2],
    }


    row.attr('name', EntryNo);
    EditRowReady(E2EProcessIndex);
    //var html = $('#editRow td');

    //row.empty();
    //row.append(html);
    //$.ajax({
    //    url: 'home/EditTimesheet',
    //    type: 'get',
    //    dataType: 'html',
    //    data: {'EntryNo' : EntryNo},
    //    success: function (response) { extractRow(response);}
    //});

    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        //var rowtoEdit = row.children('table').children('tbody').children('tr').children('td');
        //row.empty();
        row.attr('name', EntryNo);
        //row.append(rowtoEdit);
    }

}

//function to get values inside controls created during edit
function EditRowReady(E2EProcessIndex) {
    $('#editE2E_Id').val(E2EProcessIndex.E2E_Id)
    $('#editE2E_Name').val(E2EProcessIndex.E2E_Name);



    var tdE2E_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 499px;"><input type="text" readonly="readonly"  value="' + E2EProcessIndex.E2E_Id + '" name="editE2E_Id" id="editE2E_Id" style="width:499px"></div></td>';

    var tdE2E_Name = '<td align="right"><div style="text-align: right; width: 499px;"><input type="text" value="' + E2EProcessIndex.E2E_Name + '" name="editE2E_Name"  id="editE2E_Name" style="width:499px"> </div></td>';



    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + E2EProcessIndex.EntryNo).closest('tr');
    row.empty();
    row.append(tdE2E_Id);
    row.append(tdE2E_Name);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var E2E_Id = $('#editE2E_Id').val();
    var E2E_Name = $('#editE2E_Name').val();

    //if (Description2 == '' || Description2 == 'undefined') {
    //    Description2 = 'NA';
    //}

    //if (Reference == '' || Reference == 'undefined') {
    //    Reference = 'NA';
    //}

    var e2eProcessIndex = {
        //'EntryNo': EntryToEdit,
        'E2E_Id': E2E_Id,
        'E2E_Name': E2E_Name,
    }

    $.ajax({
        url: '/E2EL5Feature/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: e2eProcessIndex,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
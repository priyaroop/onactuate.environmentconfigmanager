﻿//Function to add an entry
function AddEntry() {
    var Unique_Id = $('#insertId').val();
    var Feature_Breakdown_Id = $('#insertBreakdownId').val();
    var App_Stream_WorkUnit_Id = $('#insertWorkUnitId').val();
    var App_Stream_WorkUnit = $('#insertWorkUnit').val();
    var App_Stream_WorkUnit_Est = $('#insertWorkUnitEst').val();
    var Status = $('#insertStatus').val();

    $.ajax({
        url: '/ApplicationStreamWU/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'App_Stream_WorkUnit_Id': App_Stream_WorkUnit_Id, 'Feature_Breakdown_Id': Feature_Breakdown_Id, 'App_Stream_WorkUnit': App_Stream_WorkUnit, 'App_Stream_WorkUnit_Est': App_Stream_WorkUnit_Est, 'Status': Status },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}//data: 

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });

    var ApplicationStreamWU = {
        'EntryNo': EntryNo,
        //'Unique_Id': tdArray[1],
        'Feature_Breakdown_Id': tdArray[2],
        'App_Stream_WorkUnit_Id': tdArray[1],
        'App_Stream_WorkUnit': tdArray[3],
        'App_Stream_WorkUnit_Est': tdArray[4],
        'Status': tdArray[5]
    }

    row.attr('name', EntryNo);
    EditRowReady(ApplicationStreamWU);
    //var html = $('#editRow td');
    
    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        row.attr('name', EntryNo);
    }
}

//function to get values inside controls created during edit
function EditRowReady(ApplicationStreamWU) {
    //$('#editUnique_Id').val(ApplicationStreamWU.Unique_Id)
    $('#editApp_Stream_WorkUnit_Id').val(ApplicationStreamWU.App_Stream_WorkUnit_Id)
    $('#editFeature_Breakdown_Id').val(ApplicationStreamWU.Feature_Breakdown_Id)
    
    $('#editApp_Stream_WorkUnit').val(ApplicationStreamWU.App_Stream_WorkUnit)
    $('#editApp_Stream_WorkUnit_Est').val(ApplicationStreamWU.App_Stream_WorkUnit_Est)
    $('#editStatus').val(ApplicationStreamWU.Status);
    
    //var tdUnique_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 100px;"><input type="text" readonly="readonly"  value="' + ApplicationStreamWU.App_Stream_WorkUnit_Id + '" name="editUnique_Id" id="editUniqueId" style="width:100px"></div></td>';
    var blankRow = '<td></td>';

    var tdApp_Stream_WorkUnit_Id = '<td align="center"><div style="text-align: center; width: 50px;"><input type="text"  value="' + ApplicationStreamWU.App_Stream_WorkUnit_Id + '" name="editWorkUnitId" id="editWorkUnitId" style="width:150px"></div></td>';

    var tdFeature_Breakdown_Id = '<td align="right"><div style="text-align: right; width: 50px;"><input type="text" value="' + ApplicationStreamWU.Feature_Breakdown_Id + '" name="editBreakdownId"  id="editBreakdownId" style="width:150px"> </div></td>';

    

    var tdApp_Stream_WorkUnit = '<td align="center"><div style="text-align: center; width: 50px;"><input type="text"  value="' + ApplicationStreamWU.App_Stream_WorkUnit + '" name="editWorkUnit" id="editWorkUnit" style="width:150px"></div></td>';

    var tdApp_Stream_WorkUnit_Est = '<td align="center"><div style="text-align: center; width: 50px;"><input type="text"  value="' + ApplicationStreamWU.App_Stream_WorkUnit_Est + '" name="editWorkUnitEst" id="editWorkUnitEst" style="width:150px"></div></td>';

    var tdStatus = '<td align="right"><div style="text-align: right; width: 50px;"><input type="text" value="' + ApplicationStreamWU.Status + '" name="editStatus"  id="editStatus" style="width:100px"> </div></td>';


    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/Images/icons/cancel.png"> </div></td>';
   


    var row = $('#editButton-' + ApplicationStreamWU.EntryNo).closest('tr');
    row.empty();
    //row.append(tdUnique_Id);
    row.append(blankRow);
    row.append(tdApp_Stream_WorkUnit_Id);
    row.append(tdFeature_Breakdown_Id);
    row.append(tdApp_Stream_WorkUnit);
    row.append(tdApp_Stream_WorkUnit_Est);
    row.append(tdStatus);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    //var Unique_Id = $('#editUniqueId').val();
    var Feature_Breakdown_Id = $('#editBreakdownId').val();
    var App_Stream_WorkUnit_Id = $('#editWorkUnitId').val();
    var App_Stream_WorkUnit = $('#editWorkUnit').val();
    var App_Stream_WorkUnit_Est = $('#editWorkUnitEst').val();
    var Status = $('#editStatus').val();
   
    var ApplicationStreamWU = {
        'EntryNo': EntryToEdit,
        //'Unique_Id': Unique_Id,
        'Feature_Breakdown_Id': Feature_Breakdown_Id,
        'App_Stream_WorkUnit_Id': App_Stream_WorkUnit_Id,
        'App_Stream_WorkUnit': App_Stream_WorkUnit,
        'App_Stream_WorkUnit_Est': App_Stream_WorkUnit_Est,
        'Status': Status
    }

    $.ajax({
        url: '/ApplicationStreamWU/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: ApplicationStreamWU,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
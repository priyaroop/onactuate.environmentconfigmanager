﻿//Function to add an entry
function AddEntry() {
    var Feature_Breakdown_Id = $('#insertBreakdownId').val();
    var Feature_Id = $('#insertId').val();
    var Feature_Breakdown = $('#insertBreakdown').val();
    var Application = $('#insertApplication').val();

    $.ajax({
        url: '/FeatureBreakdownIndex/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'Feature_Breakdown_Id': Feature_Breakdown_Id, 'Feature_Id': Feature_Id, 'Feature_Breakdown': Feature_Breakdown, 'Application': Application },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });

    var FeatureBreakdownIndex = {
        'EntryNo': EntryNo,
        'Feature_Breakdown_Id': tdArray[1],
        'Feature_Id': tdArray[2],
        'Feature_Breakdown': tdArray[3],
        'Application': tdArray[4]
    }

    row.attr('name', EntryNo);
    EditRowReady(FeatureBreakdownIndex);
    //var html = $('#editRow td');
    
    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        row.attr('name', EntryNo);
    }
}

//function to get values inside controls created during edit
function EditRowReady(FeatureBreakdownIndex) {
    $('#editFeature_Breakdown_Id').val(FeatureBreakdownIndex.Feature_Breakdown_Id)
    $('#editFeature_Id').val(FeatureBreakdownIndex.Feature_Id)
    $('#editFeature_Breakdown').val(FeatureBreakdownIndex.Feature_Breakdown)
    $('#editApplication').val(FeatureBreakdownIndex.Application);
    
    var tdFeature_Breakdown_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 125px;"><input type="text" readonly="readonly"  value="' + FeatureBreakdownIndex.Feature_Breakdown_Id + '" name="editBreakdownId"  id="editBreakdownId" style="width:149px"> </div></td>';

    var tdFeature_Id = '<td align="right"><div style="text-align: right; width: 50px;"><input type="text" value="' + FeatureBreakdownIndex.Feature_Id + '" name="editFeatureId" id="editFeatureId" style="width:149px"></div></td>';

    var tdFeature_Breakdown = '<td align="right"><div style="text-align: right; width: 50px;"><input type="text" value="' + FeatureBreakdownIndex.Feature_Breakdown + '" name="editBreakdown"  id="editBreakdown" style="width:349px"> </div></td>';

    var tdApplication = '<td align="center"><div style="text-align: center; width: 16px;"><input type="text"  value="' + FeatureBreakdownIndex.Application + '" name="editApplication" id="editApplication" style="width:299px"></div></td>';
    
    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/Images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + FeatureBreakdownIndex.EntryNo).closest('tr');
    row.empty();
    row.append(tdFeature_Breakdown_Id);
    row.append(tdFeature_Id);
    row.append(tdFeature_Breakdown);
    row.append(tdApplication);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var Feature_Breakdown_Id = $('#editBreakdownId').val();
    var Feature_Id = $('#editFeatureId').val();
    var Feature_Breakdown = $('#editBreakdown').val();
    var Application = $('#editApplication').val();

    var FeatureBreakdownIndex = {
        //'EntryNo': EntryToEdit,
        'Feature_Breakdown_Id': Feature_Breakdown_Id,
        'Feature_Id': Feature_Id,
        'Feature_Breakdown': Feature_Breakdown,
        'Application': Application
    }

    $.ajax({
        url: '/FeatureBreakdownIndex/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: FeatureBreakdownIndex,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
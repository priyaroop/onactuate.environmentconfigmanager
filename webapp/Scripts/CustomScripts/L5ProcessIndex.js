﻿//Function to add an entry
function AddEntry()
{
    var L5_Id = $('#insertId').val();
    var L5_Name = $('#insertName').val();

    $.ajax({
        url: '/L5ProcessIndex/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: {'L5_Id':L5_Id,'L5_Name':L5_Name},
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });


    var L5ProcessIndex = {
        'EntryNo':EntryNo,
        'L5_Id': tdArray[1],
        'L5_Name': tdArray[2],
    }


    row.attr('name', EntryNo);
    EditRowReady(L5ProcessIndex);
    //var html = $('#editRow td');

    //row.empty();
    //row.append(html);
    //$.ajax({
    //    url: 'home/EditTimesheet',
    //    type: 'get',
    //    dataType: 'html',
    //    data: {'EntryNo' : EntryNo},
    //    success: function (response) { extractRow(response);}
    //});

    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        //var rowtoEdit = row.children('table').children('tbody').children('tr').children('td');
        //row.empty();
        row.attr('name', EntryNo);
        //row.append(rowtoEdit);
    }

}

//function to get values inside controls created during edit
function EditRowReady(L5ProcessIndex) {
    $('#editL5_Id').val(L5ProcessIndex.L5_Id)
    $('#editL5_Name').val(L5ProcessIndex.L5_Name);


    var tdL5_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 149px;"><input type="text" readonly="readonly"  value="' + L5ProcessIndex.L5_Id + '" name="editL5_Id" id="editL5_Id" style="width:149px"></div></td>';

    var tdL5_Name = '<td align="right"><div style="text-align: right; width: 799px;"><input type="text" value="' + L5ProcessIndex.L5_Name + '" name="editL5_Name"  id="editL5_Name" style="width:799px"> </div></td>';
 

    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + L5ProcessIndex.EntryNo).closest('tr');
    row.empty();
    row.append(tdL5_Id);
    row.append(tdL5_Name);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
        //Entry number to edit
        var EntryToEdit = $('tr.editOpen').attr('name');
        
        var L5_Id = $('#editL5_Id').val();
        var L5_Name = $('#editL5_Name').val();

        //if (Description2 == '' || Description2 == 'undefined') {
        //    Description2 = 'NA';
        //}

        //if (Reference == '' || Reference == 'undefined') {
        //    Reference = 'NA';
        //}
       
        var L5ProcessIndex = {
            //'EntryNo': EntryToEdit,
            'L5_Id': L5_Id,
            'L5_Name': L5_Name,
        }

        $.ajax({
            url: '/L5ProcessIndex/EditEntry',
            type: 'post',
            dataType: 'json',
            async: false,
            data: L5ProcessIndex,
            complete: function () { $('#flexigridTimesheetData').flexReload(); }
        });

        //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
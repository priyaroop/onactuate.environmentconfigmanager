﻿//Function to add an entry
function AddEntry() {
    var E2E_Thread_Id = $('#insertId').val();
    var E2E_Id = $('#insertE2EId').val();
    var E2E_Thread = $('#insertE2EThread').val();
    alert(E2E_Id);

    $.ajax({
        url: '/E2EThread/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'E2E_Thread_Id': E2E_Thread_Id, 'E2E_Id': E2E_Id, 'E2E_Thread': E2E_Thread },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });

    var E2EThread = {
        'EntryNo': EntryNo,
        'E2E_Thread_Id': tdArray[1],
        'E2E_Id': tdArray[2],
        'E2E_Thread': tdArray[3]
    }

    row.attr('name', EntryNo);
    EditRowReady(E2EThread);
    //var html = $('#editRow td');
    
    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        row.attr('name', EntryNo);
    }
}

//function to get values inside controls created during edit
function EditRowReady(E2EThread) {
    $('#editE2E_Thread_Id').val(E2EThread.E2E_Thread_Id)
    $('#editE2E_Id').val(E2EThread.E2E_Id)
    $('#editE2E_Thread').val(E2EThread.E2E_Thread)

    var tdE2E_Thread_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 150px;"><input type="text" readonly="readonly"  value="' + E2EThread.E2E_Thread_Id + '" name="editThreadId" id="editThreadId" style="width:149px"></div></td>';

    var tdE2E_Id = '<td align="center"><div style="text-align: right; width: 149px;"><input type="text" value="' + E2EThread.E2E_Id + '" name="editE2EId" id="editE2EId" style="width:149px"></div></td>';

    var tdE2E_Thread = '<td align="center"><div style="text-align: right; width: 50px;"><input type="text" value="' + E2EThread.E2E_Thread + '" name="editE2EThread" id="editE2EThread" style="width:649px"></div></td>';
        
    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/Images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + E2EThread.EntryNo).closest('tr');
    row.empty();
    row.append(tdE2E_Thread_Id);
    row.append(tdE2E_Id);
    row.append(tdE2E_Thread);
    row.append(tdAction);

    alert("dsfsf");

}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var E2E_Thread_Id = $('#editThreadId').val();
    var E2E_Id = $('#editE2EId').val();
    var E2E_Thread = $('#editE2EThread').val();

    var E2EThread = {
        //'EntryNo': EntryToEdit,
        'E2E_Thread_Id': E2E_Thread_Id,
        'E2E_Id': E2E_Id,
        'E2E_Thread': E2E_Thread
    }

    $.ajax({
        url: '/E2EThread/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: E2EThread,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
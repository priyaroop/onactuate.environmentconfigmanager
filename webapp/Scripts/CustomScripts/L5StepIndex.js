﻿//Function to add an entry
function AddEntry() {
    var L5_Step_Id = $('#insertId').val();
    var L5_Step_Name = $('#insertName').val();

    $.ajax({
        url: '/L5StepIndex/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'L5_Step_Id': L5_Step_Id, 'L5_Step_Name': L5_Step_Name },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });


    var L5StepIndex = {
        'EntryNo': EntryNo,
        'L5_Step_Id': tdArray[1],
        'L5_Step_Name': tdArray[2],
    }


    row.attr('name', EntryNo);
    EditRowReady(L5StepIndex);
    //var html = $('#editRow td');

    //row.empty();
    //row.append(html);
    //$.ajax({
    //    url: 'home/EditTimesheet',
    //    type: 'get',
    //    dataType: 'html',
    //    data: {'EntryNo' : EntryNo},
    //    success: function (response) { extractRow(response);}
    //});

    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        //var rowtoEdit = row.children('table').children('tbody').children('tr').children('td');
        //row.empty();
        row.attr('name', EntryNo);
        //row.append(rowtoEdit);
    }

}

//function to get values inside controls created during edit
function EditRowReady(L5StepIndex) {
    $('#editL5_Step_Id').val(L5StepIndex.L5_Step_Id)
    $('#editL5_Step_Name').val(L5StepIndex.L5_Step_Name);

    var tdL5_Step_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 125px;"><input type="text" readonly="readonly"  value="' + L5StepIndex.L5_Step_Id + '" name="editL5_Step_Id" id="editL5_Step_Id" style="width:149px"></div></td>';

    var tdL5_Step_Name = '<td align="right"><div style="text-align: right; width: 50px;"><input type="text" value="' +L5StepIndex.L5_Step_Name + '" name="editL5_Step_Name"  id="editL5_Step_Name" style="width:799px"> </div></td>';

    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + L5StepIndex.EntryNo).closest('tr');
    row.empty();
    row.append(tdL5_Step_Id);
    row.append(tdL5_Step_Name);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var l5StepId = $('#editL5_Step_Id').val();
    var l5StepName = $('#editL5_Step_Name').val();

    //if (Description2 == '' || Description2 == 'undefined') {
    //    Description2 = 'NA';
    //}

    //if (Reference == '' || Reference == 'undefined') {
    //    Reference = 'NA';
    //}

    var l5StepIndex = {
        //'EntryNo': EntryToEdit,
        'L5_Step_Id': l5StepId,
        'L5_Step_Name': l5StepName,
    }

    $.ajax({
        url: '/L5StepIndex/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: l5StepIndex,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
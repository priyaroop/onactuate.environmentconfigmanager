﻿//Function to add an entry
function AddEntry() {
    var E2E_Thread_Id = $('#insertId').val();
    var Feature_Breakdown_Id = $('#insertBreakdownId').val();
    var Estimates = $('#insertEstimates').val();
    var Integration_Estimates = $('#insertIEstimates').val();

    $.ajax({
        url: '/E2EThreadEstimates/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'E2E_Thread_Id': E2E_Thread_Id, 'Feature_Breakdown_Id': Feature_Breakdown_Id, 'Estimates': Estimates, 'Integration_Estimates': Integration_Estimates },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });

    var E2EThreadEstimates = {
        'EntryNo': EntryNo,
        'UniqueId': tdArray[1],
        'E2E_Thread_Id': tdArray[2],
        'Feature_Breakdown_Id': tdArray[3],
        'Estimates': tdArray[4],
        'Integration_Estimates': tdArray[5]
    }

    row.attr('name', EntryNo);
    EditRowReady(E2EThreadEstimates);
    //var html = $('#editRow td');
    
    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        row.attr('name', EntryNo);
    }
}

//function to get values inside controls created during edit
function EditRowReady(E2EThreadEstimates) {
    $('#editUnique_Id').val(E2EThreadEstimates.UniqueId)
    $('#editE2E_Thread_Id').val(E2EThreadEstimates.E2E_Thread_Id)
    $('#editFeature_Breakdown_Id').val(E2EThreadEstimates.Feature_Breakdown_Id)
    $('#editEstimates').val(E2EThreadEstimates.Estimates)
    $('#editIntegration_Estimates').val(E2EThreadEstimates.Integration_Estimates);
    
    var tdUnique_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 125px;"><input type="text" readonly="readonly"  value="' + E2EThreadEstimates.UniqueId + '" name="editUniqueId" id="editUniqueId" style="width:100px"></div></td>';

    var tdE2E_Thread_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 125px;"><input type="text" value="' + E2EThreadEstimates.E2E_Thread_Id + '" name="editE2EThreadId" id="editE2EThreadId" style="width:100px"></div></td>';

    var tdFeature_Breakdown_Id = '<td align="right"><div style="text-align: right; width: 100px;"><input type="text" value="' + E2EThreadEstimates.Feature_Breakdown_Id + '" name="editBreakdownId"  id="editBreakdownId" style="width:150px"> </div></td>';

    var tdEstimates = '<td align="right"><div style="text-align: right; width: 50px;"><input type="text" value="' + E2EThreadEstimates.Estimates + '" name="editEstimates"  id="editEstimates" style="width:200px"> </div></td>';

    var tdIEstimates = '<td align="center"><div style="text-align: center; width: 16px;"><input type="text"  value="' + E2EThreadEstimates.Integration_Estimates + '" name="editIEstimates" id="editIEstimates" style="width:300px"></div></td>';
    
    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/Images/icons/cancel.png"> </div></td>';


    var row = $('#editButton-' + E2EThreadEstimates.EntryNo).closest('tr');
    row.empty();
    row.append(tdUnique_Id);
    row.append(tdE2E_Thread_Id);
    row.append(tdFeature_Breakdown_Id);
    row.append(tdEstimates);
    row.append(tdIEstimates);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var E2E_Thread_Id = $('#editE2EThreadId').val();
    var Feature_Breakdown_Id = $('#editBreakdownId').val();
    var Estimates = $('#editEstimates').val();
    var IEstimates = $('#editIEstimates').val();
   
    var E2EThreadEstimates = {
        //'EntryNo': EntryToEdit,
        'E2E_Thread_Id': E2E_Thread_Id,
        'Feature_Breakdown_Id': Feature_Breakdown_Id,
        'Estimates': Estimates,
        'IEstimates': IEstimates
    }

    $.ajax({
        url: '/E2EThreadEstimates/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: E2EThreadEstimates,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
﻿//Function to add an entry
function AddEntry() {
    var Feature_Group_Id = $('#insertId').val();
    var Feature_Group_Name = $('#insertGrpName').val();

    $.ajax({
        url: '/FeatureGroupIndex/CreateEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'Feature_Group_Id': Feature_Group_Id, 'Feature_Group_Name': Feature_Group_Name },
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });

    var FeatureGroupIndex = {
        'EntryNo': EntryNo,
        'Feature_Group_Id': tdArray[1],
        'Feature_Group_Name': tdArray[2]
    }

    row.attr('name', EntryNo);
    EditRowReady(FeatureGroupIndex);
    //var html = $('#editRow td');
    
    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        row.attr('name', EntryNo);
    }
}

//function to get values inside controls created during edit
function EditRowReady(FeatureGroupIndex) {
    $('#editFeature_Group_Id').val(FeatureGroupIndex.Feature_Group_Id)
    $('#editFeature_Group_Name').val(FeatureGroupIndex.Feature_Group_Name);
    
    var tdFeature_Group_Id = '<td align="center"><div style="text-align: center; width: 16px;">&nbsp;</div></td><td align="left" class="sorted"><div style="text-align: left; width: 149px;"><input type="text" readonly="readonly"  value="' + FeatureGroupIndex.Feature_Group_Id + '" name="editFeatureGroupId" id="editFeatureGroupId" style="width:149px"></div></td>';

    var tdFeature_Group_Name = '<td align="right"><div style="text-align: right; width: 799px;"><input type="text" value="' + FeatureGroupIndex.Feature_Group_Name + '" name="editGrpName"  id="editGrpName" style="width:799px"> </div></td>';
        
    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/Images/icons/cancel.png"> </div></td>';
    
    var row = $('#editButton-' + FeatureGroupIndex.EntryNo).closest('tr');
    row.empty();
    row.append(tdFeature_Group_Id);
    row.append(tdFeature_Group_Name);
    row.append(tdAction);
}

function EditEntry() {
    //if (!ValidateEntriesEditTimesheet()) {
    //    return false;
    //} else {
    //Entry number to edit
    var EntryToEdit = $('tr.editOpen').attr('name');

    var Feature_Group_Id = $('#editFeatureGroupId').val();
    var Feature_Group_Name = $('#editGrpName').val();
   
    var FeatureGroupIndex = {
        //'EntryNo': EntryToEdit,
        'Feature_Group_Id': Feature_Group_Id,
        'Feature_Group_Name': Feature_Group_Name
    }

    $.ajax({
        url: '/FeatureGroupIndex/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: FeatureGroupIndex,
        complete: function () { $('#flexigridTimesheetData').flexReload(); }
    });

    //addSuccessNotification('successNotification', 'Entry added successfully');
    //}
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridTimesheetData').flexReload();
}
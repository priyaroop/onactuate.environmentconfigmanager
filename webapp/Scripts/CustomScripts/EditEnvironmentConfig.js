﻿//Function to add an entry
function AddEntry() {
    var EnvironmentId = $('#insertEnvironmentId').val();
    var TokenName = $('#insertTokenName').val();
    var KeyName = $('#insertKeyName').val();
    var KeyValue = $('#insertKeyValue').val();
    var IsEncrypted = $('#insertIsEncrypted:checked').length > 0;

    $.ajax({
        url: '/EnvConfig/EnvConfig/CreateNewKey',
        type: 'post',
        dataType: 'json',
        async: false,
        data: { 'environmentId': EnvironmentId, 'tokenName': TokenName, 'keyName': KeyName, 'keyValue': KeyValue, 'isEncrypted': IsEncrypted },
        complete: function () { $('#flexigridKeysData').flexReload(); }
    });
}

//function to be executed on row edit button click 
function EntryOnEdit(EntryNo) {
    var row = $('#editButton-' + EntryNo).closest('tr');
    if ($('.editOpen').length > 0) {
        return false;
    }
    row.addClass('editOpen');
    var tdArray = [];
    var hdnInputArray = [];

    $('.editOpen input[type=hidden]').map(function () {
        hdnInputArray.push($(this).val());
    });
    row.children('td').map(function () {
        tdArray.push($(this).children('div').text());

    });

    var EnvironmentKeyData = {
        'KeyId': EntryNo,
        'TokenName': tdArray[1],
        'KeyName': tdArray[2],
        'KeyValue': tdArray[3],
        'IsEncrypted': tdArray[4]
    }

    row.attr('name', EntryNo);
    EditRowReady(EnvironmentKeyData);

    function extractRow(data) {
        var rowDiv = $('#editDiv');
        rowDiv.empty();
        rowDiv.append(data);
        var tableData = $('#editDiv td');
        row.append(tableData);
        //var rowtoEdit = row.children('table').children('tbody').children('tr').children('td');
        //row.empty();
        row.attr('name', EntryNo);
        //row.append(rowtoEdit);
    }

}

//function to get values inside controls created during edit
function EditRowReady(EnvironmentKeyData) {
    $('#editTokenName').val(EnvironmentKeyData.TokenName);
    $('#editKeyName').val(EnvironmentKeyData.KeyName);
    $('#editKeyValue').val(EnvironmentKeyData.KeyValue);
    $('#editIsEncrypted').val(EnvironmentKeyData.IsEncrypted);

    var tdEmpty = '<td align="center"><div><input type="hidden" value="' + EnvironmentKeyData.KeyId + '" name="editKeyId" id="editKeyId"></div></td>';

    var tdTokenName = '<td align="left" class="sorted"><div style="text-align: left; width: 199px;">' + EnvironmentKeyData.TokenName + '</div></td>';

    var tdKeyName = '<td align="right"><div style="text-align: right; width: 199px;"><input type="text" value="' + EnvironmentKeyData.KeyName + '" name="editKeyName"  id="editKeyName" style="width:199px"> </div></td>';

    var tdKeyValue = '<td align="right"><div style="text-align: right; width: 399px;"><input type="text" value="' + EnvironmentKeyData.KeyValue + '" name="editKeyValue"  id="editKeyValue" style="width:399px"> </div></td>';

    var tdIsEncrypted = '<td align="right"><div style="text-align: right; width: 74px;"><input type=\"checkbox\" name="editIsEncrypted"  id="editIsEncrypted" style="width:74px"> </div></td>';

    var tdAction = '<td align="center"><div style="text-align: center; width: 64px;"><img title="Update" id="btnAdd" onclick="javascript:EditEntry()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/check_16x16.png">&nbsp;&nbsp<span ></span><img title="Cancel" id="btnAdd" onclick="reloadFlexiGrid()" style="cursor:pointer; cursor:hand;" src="/content/images/icons/cancel.png"> </div></td>';

    var row = $('#editButton-' + EnvironmentKeyData.KeyId).closest('tr');
    row.empty();
    row.append(tdEmpty);
    row.append(tdTokenName);
    row.append(tdKeyName);
    row.append(tdKeyValue);
    row.append(tdIsEncrypted);
    row.append(tdAction);
}

function EditEntry() {

    var EntryToEdit = $('tr.editOpen').attr('name');

    var KeyId = $('#editKeyId').val();
    var KeyName = $('#editKeyName').val();
    var KeyValue = $('#editKeyValue').val();
    var IsEncrypted = $('#editIsEncrypted:checked').length > 0;

    var environmentKeyData = {
        'KeyId': KeyId,
        'KeyName': KeyName,
        'KeyValue': KeyValue,
        'IsEncrypted': IsEncrypted
    }

    $.ajax({
        url: '/EnvConfig/EnvConfig/EditEntry',
        type: 'post',
        dataType: 'json',
        async: false,
        data: environmentKeyData,
        //data: { 'keyId': KeyId, 'keyName': KeyName, 'keyValue': KeyValue, 'isEncrypted': IsEncrypted },
        complete: function () { $('#flexigridKeysData').flexReload(); }
        //error: function (XMLHttpRequest, textStatus, errorThrown) {
        //    alert(XMLHttpRequest.responseText);
        //}
    });
}

//function to reloadload flexiGrid
function reloadFlexiGrid() {
    $('#flexigridKeysData').flexReload();
}
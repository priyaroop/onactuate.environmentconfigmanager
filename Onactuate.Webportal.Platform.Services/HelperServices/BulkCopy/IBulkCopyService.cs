﻿using System.Collections.Generic;

namespace Onactuate.Webportal.Platform.Services.HelperServices.BulkCopy
{
    public interface IBulkCopyService
    {
        void BulkCopy<T>(IEnumerable<T> entitiesList, string tableName);

    }
}
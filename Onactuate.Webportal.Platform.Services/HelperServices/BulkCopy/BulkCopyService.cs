﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.HelperServices.BulkCopy
{
    public class BulkCopyService :IBulkCopyService
    {
        private ISqlBulkCopyRepository SqlBulkCopyRepositoryObj { get; set; }
        
        public BulkCopyService(ISqlBulkCopyRepository sqlBulkCopyRepositoryObj)
        {
            this.SqlBulkCopyRepositoryObj = sqlBulkCopyRepositoryObj;
        }

        public void BulkCopy<T>(IEnumerable<T> entitiesList, string tableName)
        {
            this.SqlBulkCopyRepositoryObj.BulkCopy(entitiesList, tableName);
        }
    }
}
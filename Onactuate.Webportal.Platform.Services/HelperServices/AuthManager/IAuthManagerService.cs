﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.HelperServices.AuthManager
{
    public interface IAuthManagerService
    {
        ApplicationPermissions IsAuthorizedUser(string userName, string password);
        IList<ApplicationPermissions> GetApplicationPermissionsList();
        ApplicationPermissions GetApplicationPermissionsById(int userId);
        IList<ApplicationPermissions> GetApplicationPermissionsListByEnvironment(int envId);


        void SaveApplicationPermissions(ApplicationPermissions permission);
        void CreateApplicationPermissions(ApplicationPermissions permission);
        
        void DeleteApplicationPermissionByPermId(int permId);
        PagingResultset<ApplicationPermissions> GetPagedApplicationPermissionsList(PagingParameters<ApplicationPermissions> parameters);
    }
}
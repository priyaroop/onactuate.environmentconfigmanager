﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.HelperServices.AuthManager
{
    public class AuthManagerService : IAuthManagerService
    {
        private IAuthManagerRepository AuthManagerRepositoryObj { get; set; }

        public AuthManagerService(IAuthManagerRepository authManagerRepositoryObj)
        {
            AuthManagerRepositoryObj = authManagerRepositoryObj;
        }

        public ApplicationPermissions IsAuthorizedUser(string userName, string password)
        {
            return AuthManagerRepositoryObj.IsAuthorizedUser(userName, password);
        }

        public IList<ApplicationPermissions> GetApplicationPermissionsList()
        {
            return AuthManagerRepositoryObj.GetApplicationPermissionsList();
        }

        public ApplicationPermissions GetApplicationPermissionsById(int userId)
        {
            return AuthManagerRepositoryObj.GetApplicationPermissionsById(userId);
        }

        public IList<ApplicationPermissions> GetApplicationPermissionsListByEnvironment(int envId)
        {
            return AuthManagerRepositoryObj.GetApplicationPermissionsListByEnvironment(envId);
        }

        public void SaveApplicationPermissions(ApplicationPermissions permission)
        {
            AuthManagerRepositoryObj.SaveApplicationPermissions(permission);
        }

        public void CreateApplicationPermissions(ApplicationPermissions permission)
        {
            AuthManagerRepositoryObj.CreateApplicationPermissions(permission);
        }
        
        public void DeleteApplicationPermissionByPermId(int permId)
        {
            AuthManagerRepositoryObj.DeleteApplicationPermissionByPermId(permId);
        }

        public PagingResultset<ApplicationPermissions> GetPagedApplicationPermissionsList(PagingParameters<ApplicationPermissions> parameters)
        {
            return AuthManagerRepositoryObj.GetPagedApplicationPermissionsList(parameters);
        }
    }
}
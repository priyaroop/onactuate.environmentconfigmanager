﻿using System;
using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class WorkflowStepService : IWorkflowStepService
    {
        private IWorkflowStepRepository WorkflowStepRepositoryObj { get; set; }

        public WorkflowStepService(IWorkflowStepRepository workflowStepRepositoryObj)
        {
            WorkflowStepRepositoryObj = workflowStepRepositoryObj;
        }

        public WorkflowStep CreateWorkflowStep(WorkflowStep workflowStep)
        {
            WorkflowStepRepositoryObj.CreateWorkflowStep(workflowStep);
            return workflowStep;
        }

        public WorkflowStep UpdateWorkflowStep(WorkflowStep workflowStep)
        {
            WorkflowStepRepositoryObj.UpdateWorkflowStep(workflowStep);
            return workflowStep;
        }

        public void DeleteWorkflowStep(WorkflowStep workflowStep)
        {
            WorkflowStepRepositoryObj.DeleteWorkflowStep(workflowStep);
        }

        public void DeleteWorkflowStepList(IList<WorkflowStep> workflowStepList)
        {
            WorkflowStepRepositoryObj.DeleteWorkflowStepList(workflowStepList);
        }


        public PagingResultset<WorkflowStep> GetPagedWorkflowStepList(PagingParameters<WorkflowStep> parameters)
        {
            return WorkflowStepRepositoryObj.GetPagedWorkflowStepList(parameters);
        }

        public WorkflowStep GetPagedWorkflowStepById(int taskId)
        {
            return WorkflowStepRepositoryObj.GetPagedWorkflowStepById(taskId);
        }

        public WorkflowStep GetWorkflowStepById(int taskId)
        {
            return WorkflowStepRepositoryObj.GetWorkflowStepById(taskId);
        }

        public IEnumerable<WorkflowStep> GetWorkflowStepsByWorkflowId(int workflowId)
        {
            return WorkflowStepRepositoryObj.GetWorkflowStepsByWorkflowId(workflowId);
        }
    }
}
﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class EnvironmentMasterService : IEnvironmentMasterService
    {
        private IEnvironmentMasterRepository EnvironmentMasterRepositoryObj { get; set; }

        public EnvironmentMasterService(IEnvironmentMasterRepository environmentMasterRepositoryObj)
        {
            EnvironmentMasterRepositoryObj = environmentMasterRepositoryObj;
        }

        public IEnumerable<EnvironmentMaster> GetEnvironmentMasterList()
        {
            return EnvironmentMasterRepositoryObj.GetEnvironmentMasterList();
        }

        public EnvironmentMaster GetEnvironmentMasterByEnvironmentId(int environmentId)
        {
            return EnvironmentMasterRepositoryObj.GetEnvironmentMasterByEnvironmentId(environmentId);

        }

        public EnvironmentMaster GetEnvironmentMasterByEnvironmentName(string environmentName)
        {
            return EnvironmentMasterRepositoryObj.GetEnvironmentMasterByEnvironmentName(environmentName);
        }

        public IEnumerable<EnvironmentMaster> GetEnvironmentMasterListByPermissionId(int userPermissionId)
        {
            return EnvironmentMasterRepositoryObj.GetEnvironmentMasterListByPermissionId(userPermissionId);
        }

        public IEnumerable<EnvironmentMaster> GetAdminEnvMasterListByPermissionId(int permissionId)
        {
            return EnvironmentMasterRepositoryObj.GetAdminEnvMasterListByPermissionId(permissionId);
        }

        public void CreateEnvironmentMaster(EnvironmentMaster environmentMaster, EnvironmentPermissionMaster permissions)
        {
            EnvironmentMasterRepositoryObj.CreateEnvironmentMaster(environmentMaster,permissions);
        }
        public void CloneEnvironmentMaster(int environmentIdToClone, EnvironmentMaster environmentMaster, EnvironmentPermissionMaster permissions)
        {
            EnvironmentMasterRepositoryObj.CloneEnvironmentMaster(environmentIdToClone, environmentMaster, permissions);

        }

        public void CreateEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList)
        {
            EnvironmentMasterRepositoryObj.CreateEnvironmentMasterMultiple(environmentMasterList);
        }

        public void UpdateEnvironmentMaster(EnvironmentMaster environmentMaster)
        {
            EnvironmentMasterRepositoryObj.UpdateEnvironmentMaster(environmentMaster);
        }

        public void UpdateEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList)
        {
            EnvironmentMasterRepositoryObj.UpdateEnvironmentMasterMultiple(environmentMasterList);
        }

        public void DeleteEnvironmentMaster(EnvironmentMaster environmentMaster)
        {
            EnvironmentMasterRepositoryObj.DeleteEnvironmentMaster(environmentMaster);
        }

        public void DeleteEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList)
        {
            EnvironmentMasterRepositoryObj.DeleteEnvironmentMasterMultiple(environmentMasterList);
        }
    }
}
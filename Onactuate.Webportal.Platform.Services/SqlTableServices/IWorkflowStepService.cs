﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IWorkflowStepService
    {
        // Create new Document Master
        WorkflowStep CreateWorkflowStep(WorkflowStep workflowMaster);

        // Update Document Master
        WorkflowStep UpdateWorkflowStep(WorkflowStep workflowMaster);

        // Delete
        void DeleteWorkflowStep(WorkflowStep workflowMaster);
        void DeleteWorkflowStepList(IList<WorkflowStep> workflowMasterList);

        // Get Data
        PagingResultset<WorkflowStep> GetPagedWorkflowStepList(PagingParameters<WorkflowStep> parameters);
        WorkflowStep GetPagedWorkflowStepById(int taskId);
        WorkflowStep GetWorkflowStepById(int taskId);
        IEnumerable<WorkflowStep> GetWorkflowStepsByWorkflowId(int workflowId);
    }
}
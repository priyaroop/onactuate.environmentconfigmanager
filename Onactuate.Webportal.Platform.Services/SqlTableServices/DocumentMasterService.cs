﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Impl;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class DocumentMasterService : IDocumentMasterService
    {
        private IDocumentMasterRepository DocumentMasterRepositoryObj { get; set; }

        public DocumentMasterService(IDocumentMasterRepository documentMasterRepositoryObj)
        {
            DocumentMasterRepositoryObj = documentMasterRepositoryObj;
        }

        public void CreateDocumentMaster(DocumentMaster documentMaster)
        {
            DocumentMasterRepositoryObj.CreateDocumentMaster(documentMaster);
        }

        public void UpdateDocumentMaster(DocumentMaster documentMaster)
        {
            DocumentMasterRepositoryObj.UpdateDocumentMaster(documentMaster);
        }

        public void DeleteDocumentMaster(DocumentMaster documentMaster)
        {
            DocumentMasterRepositoryObj.DeleteDocumentMaster(documentMaster);
        }

        public void DeleteDocumentMasterList(IList<DocumentMaster> documentMasterList)
        {
            DocumentMasterRepositoryObj.DeleteDocumentMasterList(documentMasterList);
        }

        public IList<DocumentMaster> GetDocumentMasterListByPermissionId(int permissionId)
        {
            return DocumentMasterRepositoryObj.GetDocumentMasterListByPermissionId(permissionId);
        }

        public PagingResultset<DocumentMaster> GetPagedDocumentMasterList(PagingParameters<DocumentMaster> parameters)
        {
            return DocumentMasterRepositoryObj.GetPagedDocumentMasterList(parameters);
        }

        public DocumentMaster GetDocumentMasterByDocumentId(int documentMasterId)
        {
            return DocumentMasterRepositoryObj.GetDocumentMasterByDocumentId(documentMasterId);
        }

        public void DeleteDocumentMasterList(IEnumerable<int> documentsToDeleteList)
        {
            DocumentMasterRepositoryObj.DeleteDocumentMasterList(documentsToDeleteList);
        }

        public void CreateSharepointDocumentMaster(DocumentMaster documentMaster, SharepointMaster sharepointMaster)
        {
            DocumentMasterRepositoryObj.CreateSharepointDocumentMaster(documentMaster, sharepointMaster);
        }

        public SharepointMaster GetSharepointMasterByDocumentId(int masterDocId)
        {
            return DocumentMasterRepositoryObj.GetSharepointMasterByDocumentId(masterDocId);
        }
    }
}
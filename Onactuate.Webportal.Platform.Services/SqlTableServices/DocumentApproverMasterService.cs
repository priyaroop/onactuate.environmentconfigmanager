﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class DocumentApproverMasterService : IDocumentApproverMasterService
    {
        private IDocumentApproverMasterRepository DocumentApproverMasterRepositoryObj { get; set; }

        public DocumentApproverMasterService(IDocumentApproverMasterRepository documentApproverMasterRepositoryObj)
        {
            DocumentApproverMasterRepositoryObj = documentApproverMasterRepositoryObj;
        }

        public void CreateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster)
        {
            DocumentApproverMasterRepositoryObj.CreateDocumentApproverMaster(documentApproverMaster);
        }

        public void UpdateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster)
        {
            DocumentApproverMasterRepositoryObj.UpdateDocumentApproverMaster(documentApproverMaster);
        }

        public void DeleteDocumentApproverMaster(DocumentApproverMaster documentApproverMaster)
        {
            DocumentApproverMasterRepositoryObj.DeleteDocumentApproverMaster(documentApproverMaster);
        }

        public void DeleteDocumentApproverMasterList(IList<DocumentApproverMaster> documentApproverMasterList)
        {
            DocumentApproverMasterRepositoryObj.DeleteDocumentApproverMasterList(documentApproverMasterList);
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterListByDocumentId(int documentId)
        {
            return DocumentApproverMasterRepositoryObj.GetDocumentApproverMasterListByDocumentId(documentId);
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterListByPermissionId(int permissionId)
        {
            return DocumentApproverMasterRepositoryObj.GetDocumentApproverMasterListByPermissionId(permissionId);
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterListById(int id)
        {
            return DocumentApproverMasterRepositoryObj.GetDocumentApproverMasterListById(id);
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterList()
        {
            return DocumentApproverMasterRepositoryObj.GetDocumentApproverMasterList();
        }
    }
}
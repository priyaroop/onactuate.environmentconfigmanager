﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IApproverGroupMasterService
    {
        // Create new Approver Group Master
        void CreateApproverGroupMaster(ApproverGroupMaster approverGroupMaster);

        // Update Document Master
        void UpdateApproverGroupMaster(ApproverGroupMaster approverGroupMaster);

        // Delete
        void DeleteApproverGroupMaster(ApproverGroupMaster approverGroupMaster);

        // change  == void DeleteApproverGroupMasterList(IList<ApproverGroupMaster> approverGroupMasterList);
        //void DeleteApproverGroupMasterList(IList<int> approverGroupMasterList);
        void DeleteApproverGroupMasterList(IList<ApproverGroupMaster> approverGroupMasterList);
        void DeleteApproverGroupMasterById(int id);

        // Get Data
        IList<ApproverGroupMaster> GetApproverGroupMasterList();
        IList<ApproverGroupMaster> GetApproverGroupMasterListById(int id);
        IList<ApproverGroupMaster> GetApproverGroupMasterListByGroupName(string groupName);
    }
}
﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IApproverGroupMemberService
    {
        // Create new Approver Group Master
        void CreateApproverGroupMember(ApproverGroupMember approverGroupMember);

        // Update Document Master
        void UpdateApproverGroupMember(ApproverGroupMember approverGroupMember);
        void ChangeApproverGroupMembers(IList<ApproverGroupMember> approverGroupMemberList, int approverGroupId);

        // Delete
        void DeleteApproverGroupMember(ApproverGroupMember approverGroupMember);
        void DeleteApproverGroupMemberList(IList<ApproverGroupMember> approverGroupMemberList);
        void DeleteApproverGroupMemberById(int id);
        void DeleteApproverGroupMemberByApproverGroupId(int approverGroupId);
        void DeleteApproverGroupMemberByPermissionId(int permId);

        // Get Data
        IList<ApproverGroupMember> GetApproverGroupMemberList();
        IList<ApproverGroupMember> GetApproverGroupMemberListByApproverGroupId(int approverGroupId);
        void ChangeApproverGroupMembers(IList<int> approversList, int selectedGroupId);
        void AddApproverGroupMembers(int permissionId, int approverGroupId);
    }
}
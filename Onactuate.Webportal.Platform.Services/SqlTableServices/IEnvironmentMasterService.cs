﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IEnvironmentMasterService
    {
        IEnumerable<EnvironmentMaster> GetEnvironmentMasterList();
        EnvironmentMaster GetEnvironmentMasterByEnvironmentId(int environmentId);
        EnvironmentMaster GetEnvironmentMasterByEnvironmentName(string environmentName);
        IEnumerable<EnvironmentMaster> GetEnvironmentMasterListByPermissionId(int userPermissionId);
        IEnumerable<EnvironmentMaster> GetAdminEnvMasterListByPermissionId(int permissionId);

        void CreateEnvironmentMaster(EnvironmentMaster environmentMaster, EnvironmentPermissionMaster permissions);
        void CreateEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList);
        void UpdateEnvironmentMaster(EnvironmentMaster environmentMaster);
        void UpdateEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList);
        void DeleteEnvironmentMaster(EnvironmentMaster environmentMaster);
        void DeleteEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList);
        void CloneEnvironmentMaster(int environmentIdToClone, EnvironmentMaster environmentMaster, EnvironmentPermissionMaster permissions);
    }
}
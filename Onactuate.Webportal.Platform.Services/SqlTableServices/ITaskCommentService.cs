﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface ITaskCommentService
    {
        // Create new Document Master
        void CreateTaskComment(TaskComment taskComment);

        // Update Document Master
        void UpdateTaskComment(TaskComment taskComment);

        // Delete
        void DeleteTaskComment(TaskComment taskComment);
        void DeleteTaskCommentList(IList<TaskComment> taskCommentList);

        // Get Data
        IList<TaskComment> GetTaskCommentListByPermissionId(int permissionId);
        PagingResultset<TaskComment> GetPagedTaskCommentList(PagingParameters<TaskComment> parameters);
        TaskComment GetPagedTaskCommentById(int taskId);
        List<TaskComment> GetTaskCommentListByDocumentId(int model);
        List<TaskMaster> GetTasksListByDocumentId(int model);
        IEnumerable<TaskCommentModel> GetTasksListByTaskId(int taskId);
    }
}
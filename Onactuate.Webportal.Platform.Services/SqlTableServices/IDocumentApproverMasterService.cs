﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IDocumentApproverMasterService
    {
        // Create new Document Approver Master
        void CreateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster);

        // Update Document Master
        void UpdateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster);

        // Delete
        void DeleteDocumentApproverMaster(DocumentApproverMaster documentApproverMaster);
        void DeleteDocumentApproverMasterList(IList<DocumentApproverMaster> documentApproverMasterList);

        // Get Data
        IList<DocumentApproverMaster> GetDocumentApproverMasterListByDocumentId(int documentId);
        IList<DocumentApproverMaster> GetDocumentApproverMasterListByPermissionId(int permissionId);
        IList<DocumentApproverMaster> GetDocumentApproverMasterListById(int id);
        IList<DocumentApproverMaster> GetDocumentApproverMasterList();
    }
}
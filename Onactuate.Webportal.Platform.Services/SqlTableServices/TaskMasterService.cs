﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class TaskMasterService : ITaskMasterService
    {
        private ITaskMasterRepository TaskMasterRepositoryObj { get; set; }

        public TaskMasterService(ITaskMasterRepository taskMasterRepositoryObj)
        {
            TaskMasterRepositoryObj = taskMasterRepositoryObj;
        }

        public void CreateTaskMaster(TaskMaster taskMaster)
        {
            TaskMasterRepositoryObj.CreateTaskMaster(taskMaster);
        }

        public void UpdateTaskMaster(TaskMaster taskMaster)
        {
            TaskMasterRepositoryObj.UpdateTaskMaster(taskMaster);
        }

        public void DeleteTaskMaster(TaskMaster taskMaster)
        {
            TaskMasterRepositoryObj.DeleteTaskMaster(taskMaster);
        }

        public void DeleteTaskMasterList(IList<TaskMaster> taskMasterList)
        {
            TaskMasterRepositoryObj.DeleteTaskMasterList(taskMasterList);
        }

        public IList<TaskMaster> GetTaskMasterListByPermissionId(int permissionId)
        {
            return TaskMasterRepositoryObj.GetTaskMasterListByPermissionId(permissionId);
        }

        public PagingResultset<TaskMaster> GetPagedTaskMasterList(PagingParameters<TaskMaster> parameters)
        {
            return TaskMasterRepositoryObj.GetPagedTaskMasterList(parameters);
        }

        public TaskMaster GetPagedTaskMasterById(int taskId)
        {
            return TaskMasterRepositoryObj.GetPagedTaskMasterById(taskId);
        }

        public TaskMaster GetTaskMasterById(int taskId)
        {
            return TaskMasterRepositoryObj.GetTaskMasterById(taskId);
        }

        public IEnumerable<TaskMaster> GetTaskToApproveList(int permissionId)
        {
            return TaskMasterRepositoryObj.GetTaskToApproveList(permissionId);
        }

        public void MarkTaskAsOld(int taskId)
        {
            TaskMasterRepositoryObj.MarkTaskAsOld(taskId);
        }
    }
}
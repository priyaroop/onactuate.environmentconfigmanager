﻿using System;
using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class WorkflowMasterService : IWorkflowMasterService
    {
        private IWorkflowMasterRepository WorkflowMasterRepositoryObj { get; set; }

        public WorkflowMasterService(IWorkflowMasterRepository workflowMasterRepositoryObj)
        {
            WorkflowMasterRepositoryObj = workflowMasterRepositoryObj;
        }

        public void CreateWorkflowMaster(WorkflowMaster workflowMaster)
        {
            WorkflowMasterRepositoryObj.CreateWorkflowMaster(workflowMaster);
        }

        public void UpdateWorkflowMaster(WorkflowMaster workflowMaster)
        {
            WorkflowMasterRepositoryObj.UpdateWorkflowMaster(workflowMaster);
        }

        public void DeleteWorkflowMaster(WorkflowMaster workflowMaster)
        {
            WorkflowMasterRepositoryObj.DeleteWorkflowMaster(workflowMaster);
        }

        public void DeleteWorkflowMasterList(IList<WorkflowMaster> workflowMasterList)
        {
            WorkflowMasterRepositoryObj.DeleteWorkflowMasterList(workflowMasterList);
        }


        public PagingResultset<WorkflowMaster> GetPagedWorkflowMasterList(PagingParameters<WorkflowMaster> parameters)
        {
            return WorkflowMasterRepositoryObj.GetPagedWorkflowMasterList(parameters);
        }

        public WorkflowMaster GetWorkflowMasterById(int taskId)
        {
            return WorkflowMasterRepositoryObj.GetWorkflowMasterById(taskId);
        }

        public IList<WorkflowMaster> GetWorkflowList()
        {
            return WorkflowMasterRepositoryObj.GetWorkflowList();
        }

        public void DeleteWorkflowMaster(int id)
        {
            WorkflowMasterRepositoryObj.DeleteWorkflowMaster(id);
        }

        public WorkflowMaster GetWorkflowMasterByName(string workflowName)
        {
            return WorkflowMasterRepositoryObj.GetWorkflowMasterByName(workflowName);
        }

    }
}
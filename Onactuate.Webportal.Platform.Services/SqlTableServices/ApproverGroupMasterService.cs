﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class ApproverGroupMasterService : IApproverGroupMasterService
    {
        private IApproverGroupMasterRepository ApproverGroupMasterRepositoryObj { get; set; }

        public ApproverGroupMasterService(IApproverGroupMasterRepository approverGroupMasterRepositoryObj)
        {
            ApproverGroupMasterRepositoryObj = approverGroupMasterRepositoryObj;
        }

        public void CreateApproverGroupMaster(ApproverGroupMaster approverGroupMaster)
        {
            ApproverGroupMasterRepositoryObj.CreateApproverGroupMaster(approverGroupMaster);
        }

        public void UpdateApproverGroupMaster(ApproverGroupMaster approverGroupMaster)
        {
            ApproverGroupMasterRepositoryObj.UpdateApproverGroupMaster(approverGroupMaster);
        }

        public void DeleteApproverGroupMaster(ApproverGroupMaster approverGroupMaster)
        {
            ApproverGroupMasterRepositoryObj.DeleteApproverGroupMaster(approverGroupMaster);
        }



        //change for delete approver group name
        public void DeleteApproverGroupMasterList(IList<ApproverGroupMaster> approverGroupMasterList)
        {
            ApproverGroupMasterRepositoryObj.DeleteApproverGroupMasterList(approverGroupMasterList);
        }
        //public void DeleteApproverGroupMasterList(IList<int> approverGroupMasterList)
        //{
        //    ApproverGroupMasterRepositoryObj.DeleteApproverGroupMasterList(approverGroupMasterList);
        //}

        public void DeleteApproverGroupMasterById(int id)
        {
            ApproverGroupMasterRepositoryObj.DeleteApproverGroupMasterById(id);
        }

        public IList<ApproverGroupMaster> GetApproverGroupMasterList()
        {
            return ApproverGroupMasterRepositoryObj.GetApproverGroupMasterList();
        }

        public IList<ApproverGroupMaster> GetApproverGroupMasterListById(int id)
        {
            return ApproverGroupMasterRepositoryObj.GetApproverGroupMasterListById(id);
        }

        public IList<ApproverGroupMaster> GetApproverGroupMasterListByGroupName(string groupName)
        {
            return ApproverGroupMasterRepositoryObj.GetApproverGroupMasterListByGroupName(groupName);
        }
    }
}
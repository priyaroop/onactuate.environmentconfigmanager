﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class KeyMasterService : IKeyMasterService
    {
        private IKeyMasterRepository KeyMasterRepositoryObj { get; set; }

        public KeyMasterService(IKeyMasterRepository keyMasterRepositoryObj)
        {
            this.KeyMasterRepositoryObj = keyMasterRepositoryObj;
        }


        public IEnumerable<KeyMaster> GetKeyMasterList()
        {
            return KeyMasterRepositoryObj.GetKeyMasterList();
        }

        public IEnumerable<KeyMaster> GetKeyMasterListByTokenId(int tokenId)
        {
            return KeyMasterRepositoryObj.GetKeyMasterListByTokenId(tokenId);
        }

        public KeyMaster GetKeyMasterByKeyId(int keyId)
        {
            return KeyMasterRepositoryObj.GetKeyMasterByKeyId(keyId);
        }

        public IEnumerable<KeyMaster> GetKeyMasterByKeyName(string keyName)
        {
            return KeyMasterRepositoryObj.GetKeyMasterByKeyName(keyName);
        }

        public IEnumerable<KeyMaster> GetKeyMasterListByEnvironmentId(int environmentId)
        {
            return KeyMasterRepositoryObj.GetKeyMasterListByEnvironmentId(environmentId);
        }

        public PagingResultset<KeyMaster> GetKeyMasterPagedResultSetByEnvironmentId(PagingParameters<KeyMaster> parameters, int environmentId)
        {
            return KeyMasterRepositoryObj.GetKeyMasterPagedResultSetByEnvironmentId(parameters, environmentId);
        }

        public void CreateKeyMaster(KeyMaster keyMaster)
        {
            KeyMasterRepositoryObj.CreateKeyMaster(keyMaster);
        }

        public void CreateKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList)
        {
            KeyMasterRepositoryObj.CreateKeyMasterMultiple(keyMasterList);
        }

        public void UpdateKeyMaster(KeyMaster keyMaster)
        {
            KeyMasterRepositoryObj.UpdateKeyMaster(keyMaster);
        }

        public void UpdateKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList)
        {
            KeyMasterRepositoryObj.UpdateKeyMasterMultiple(keyMasterList);
        }

        public void DeleteKeyMaster(KeyMaster keyMaster)
        {
            KeyMasterRepositoryObj.DeleteKeyMaster(keyMaster);
        }

        public void DeleteKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList)
        {
            KeyMasterRepositoryObj.DeleteKeyMasterMultiple(keyMasterList);
        }

        public void DeleteKeyMasterByKeyId(int keyId)
        {
            KeyMasterRepositoryObj.DeleteKeyMasterByKeyId(keyId);

        }

        public void DeleteKeyMasterByKeyIdList(IEnumerable<int> keyIdList)
        {
            KeyMasterRepositoryObj.DeleteKeyMasterByKeyIdList(keyIdList);
        }
    }
}
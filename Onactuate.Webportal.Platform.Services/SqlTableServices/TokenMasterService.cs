﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class TokenMasterService : ITokenMasterService
    {
        private ITokenMasterRepository TokenMasterRepositoryObj { get; set; }

        public TokenMasterService(ITokenMasterRepository tokenMasterRepositoryObj)
        {
            TokenMasterRepositoryObj = tokenMasterRepositoryObj;
        }


        public IEnumerable<TokenMaster> GetTokenMasterList()
        {
            return TokenMasterRepositoryObj.GetTokenMasterList();
        }

        public IEnumerable<TokenMaster> GetTokenMasterListByEnvironmentId(int environmentId)
        {
            return TokenMasterRepositoryObj.GetTokenMasterListByEnvironmentId(environmentId);
        }

        public TokenMaster GetTokenMasterByEnvironmentIdAndTokeName(int environmentId, string tokenName)
        {
            return TokenMasterRepositoryObj.GetTokenMasterByEnvironmentIdAndTokeName(environmentId, tokenName);
        }

        public void CreateTokenMaster(TokenMaster tokenMaster)
        {
            TokenMasterRepositoryObj.CreateTokenMaster(tokenMaster);
        }

        public void CreateTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList)
        {
            TokenMasterRepositoryObj.CreateTokenMasterMultiple(tokenMasterList);
        }

        public void UpdateTokenMaster(TokenMaster tokenMaster)
        {
            TokenMasterRepositoryObj.UpdateTokenMaster(tokenMaster);
        }

        public void UpdateTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList)
        {
            TokenMasterRepositoryObj.UpdateTokenMasterMultiple(tokenMasterList);
        }

        public void DeleteTokenMaster(TokenMaster tokenMaster)
        {
            TokenMasterRepositoryObj.DeleteTokenMaster(tokenMaster);
        }

        public void DeleteTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList)
        {
            TokenMasterRepositoryObj.DeleteTokenMasterMultiple(tokenMasterList);
        }
    }
}
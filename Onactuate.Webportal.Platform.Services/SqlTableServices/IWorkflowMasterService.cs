﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IWorkflowMasterService
    {
        // Create new Document Master
        void CreateWorkflowMaster(WorkflowMaster workflowMaster);

        // Update Document Master
        void UpdateWorkflowMaster(WorkflowMaster workflowMaster);

        // Delete
        void DeleteWorkflowMaster(WorkflowMaster workflowMaster);
        void DeleteWorkflowMasterList(IList<WorkflowMaster> workflowMasterList);

        // Get Data
        PagingResultset<WorkflowMaster> GetPagedWorkflowMasterList(PagingParameters<WorkflowMaster> parameters);
        WorkflowMaster GetWorkflowMasterById(int taskId);
        WorkflowMaster GetWorkflowMasterByName(string workflowName);

        IList<WorkflowMaster> GetWorkflowList();
        void DeleteWorkflowMaster(int id);
    }
}
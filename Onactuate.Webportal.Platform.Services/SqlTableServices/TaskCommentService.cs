﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class TaskCommentService : ITaskCommentService
    {
        public ITaskCommentRepository TaskCommentRepositoryObj;

        public TaskCommentService(ITaskCommentRepository taskCommentRepositoryObj)
        {
            TaskCommentRepositoryObj = taskCommentRepositoryObj;
        }


        public void CreateTaskComment(TaskComment taskComment)
        {
            TaskCommentRepositoryObj.CreateTaskComment(taskComment);
        }

        public void UpdateTaskComment(TaskComment taskComment)
        {
            TaskCommentRepositoryObj.UpdateTaskComment(taskComment);
        }

        public void DeleteTaskComment(TaskComment taskComment)
        {
            TaskCommentRepositoryObj.DeleteTaskComment(taskComment);
        }

        public void DeleteTaskCommentList(IList<TaskComment> taskCommentList)
        {
            TaskCommentRepositoryObj.DeleteTaskCommentList(taskCommentList);
        }

        public IList<TaskComment> GetTaskCommentListByPermissionId(int permissionId)
        {
            return TaskCommentRepositoryObj.GetTaskCommentListByPermissionId(permissionId);
        }

        public PagingResultset<TaskComment> GetPagedTaskCommentList(PagingParameters<TaskComment> parameters)
        {
            return TaskCommentRepositoryObj.GetPagedTaskCommentList(parameters);
        }

        public TaskComment GetPagedTaskCommentById(int taskId)
        {
            return TaskCommentRepositoryObj.GetPagedTaskCommentById(taskId);
        }

        public List<TaskComment> GetTaskCommentListByDocumentId(int model)
        {
            return TaskCommentRepositoryObj.GetTaskCommentListByDocumentId(model);

        }

        public List<TaskMaster> GetTasksListByDocumentId(int model)
        {
            return TaskCommentRepositoryObj.GetTasksListByDocumentId(model);
        }

        public IEnumerable<TaskCommentModel> GetTasksListByTaskId(int taskId)
        {
            return TaskCommentRepositoryObj.GetTasksListByTaskId(taskId);

        }
    }
}
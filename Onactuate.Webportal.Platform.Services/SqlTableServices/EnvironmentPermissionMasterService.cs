﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class EnvironmentPermissionMasterService : IEnvironmentPermissionMasterService
    {
        private IEnvironmentPermissionMasterRepository EnvironmentPermissionMasterRepositoryObj { get; set; }

        public EnvironmentPermissionMasterService(IEnvironmentPermissionMasterRepository environmentPermissionMasterRepositoryObj)
        {
            EnvironmentPermissionMasterRepositoryObj = environmentPermissionMasterRepositoryObj;
        }


        public IEnumerable<EnvironmentPermissionMaster> GeEnvironmentPermissionMasterList()
        {
            return EnvironmentPermissionMasterRepositoryObj.GeEnvironmentPermissionMasterList();
        }

        public IEnumerable<EnvironmentPermissionMaster> GetEnvironmentPermissionMasterListByPermissionId(int permissionId)
        {
            return
                EnvironmentPermissionMasterRepositoryObj.GetEnvironmentPermissionMasterListByPermissionId(permissionId);
        }

        public IEnumerable<EnvironmentPermissionMaster> GetEnvironmentPermissionMasterListByEnvironmentId(int environmentId)
        {
            return
                EnvironmentPermissionMasterRepositoryObj.GetEnvironmentPermissionMasterListByEnvironmentId(environmentId);
        }

        public PagingResultset<EnvironmentPermissionMaster> GetPagedEnvironmentPermissionsList(PagingParameters<EnvironmentPermissionMaster> parameters, int permissionId)
        {
            return EnvironmentPermissionMasterRepositoryObj.GetPagedEnvironmentPermissionsList(parameters, permissionId);
        }

        public void CreateEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster)
        {
            EnvironmentPermissionMasterRepositoryObj.CreateEnvironmentPermissionMaster(environmentPermissionMaster);
        }

        public void CreateEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList)
        {
            EnvironmentPermissionMasterRepositoryObj.CreateEnvironmentPermissionMasterMultiple(environmentPermissionMasterList);
        }

        public void UpdateEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster)
        {
            EnvironmentPermissionMasterRepositoryObj.UpdateEnvironmentPermissionMaster(environmentPermissionMaster);
        }

        public void UpdateEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList)
        {
            EnvironmentPermissionMasterRepositoryObj.UpdateEnvironmentPermissionMasterMultiple(environmentPermissionMasterList);
        }

        public void DeleteEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster)
        {
            EnvironmentPermissionMasterRepositoryObj.DeleteEnvironmentPermissionMaster(environmentPermissionMaster);
        }

        public void DeleteEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList)
        {
            EnvironmentPermissionMasterRepositoryObj.DeleteEnvironmentPermissionMasterMultiple(environmentPermissionMasterList);
        }

        public void DeleteEnvironmentPermissionMasterById(int environmentPermissionId)
        {
            EnvironmentPermissionMasterRepositoryObj.DeleteEnvironmentPermissionMasterById(environmentPermissionId);
        }
    }
}
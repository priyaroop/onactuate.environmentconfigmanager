﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface IEnvironmentPermissionMasterService
    {
        IEnumerable<EnvironmentPermissionMaster> GeEnvironmentPermissionMasterList();
        IEnumerable<EnvironmentPermissionMaster> GetEnvironmentPermissionMasterListByPermissionId(int permissionId);
        IEnumerable<EnvironmentPermissionMaster> GetEnvironmentPermissionMasterListByEnvironmentId(int environmentId);
        PagingResultset<EnvironmentPermissionMaster> GetPagedEnvironmentPermissionsList(PagingParameters<EnvironmentPermissionMaster> parameters, int permissionId);

        void CreateEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster);
        void CreateEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList);
        void UpdateEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster);
        void UpdateEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList);
        void DeleteEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster);
        void DeleteEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList);
        void DeleteEnvironmentPermissionMasterById(int environmentPermissionId);
    }
}
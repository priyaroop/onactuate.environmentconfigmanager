﻿using System;
using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class WorkflowStepApproverService : IWorkflowStepApproverService
    {
        private IWorkflowStepApproverRepository WorkflowStepApproverRepositoryObj { get; set; }

        public WorkflowStepApproverService(IWorkflowStepApproverRepository workflowStepApproverRepositoryObj)
        {
            WorkflowStepApproverRepositoryObj = workflowStepApproverRepositoryObj;
        }

        public WorkflowStepApprover CreateWorkflowStepApprover(WorkflowStepApprover workflowStepApprover)
        {
            WorkflowStepApproverRepositoryObj.CreateWorkflowStepApprover(workflowStepApprover);
            return workflowStepApprover;
        }

        public WorkflowStepApprover UpdateWorkflowStepApprover(WorkflowStepApprover workflowStepApprover)
        {
            WorkflowStepApproverRepositoryObj.UpdateWorkflowStepApprover(workflowStepApprover);
            return workflowStepApprover;
        }

        public void DeleteWorkflowStepApprover(WorkflowStepApprover workflowStepApprover)
        {
            WorkflowStepApproverRepositoryObj.DeleteWorkflowStepApprover(workflowStepApprover);
        }

        public void DeleteWorkflowStepApproverList(IList<WorkflowStepApprover> workflowStepApproverList)
        {
            WorkflowStepApproverRepositoryObj.DeleteWorkflowStepApproverList(workflowStepApproverList);
        }


        public PagingResultset<WorkflowStepApprover> GetPagedWorkflowStepApproverList(PagingParameters<WorkflowStepApprover> parameters)
        {
            return WorkflowStepApproverRepositoryObj.GetPagedWorkflowStepApproverList(parameters);
        }

        public WorkflowStepApprover GetPagedWorkflowStepApproverById(int taskId)
        {
            return WorkflowStepApproverRepositoryObj.GetPagedWorkflowStepApproverById(taskId);
        }

        public WorkflowStepApprover GetWorkflowStepApproverById(int taskId)
        {
            return WorkflowStepApproverRepositoryObj.GetWorkflowStepApproverById(taskId);
        }

        public IEnumerable<WorkflowStepApprover> GetWorkflowStepApproversByStepId(int stepId)
        {
            return WorkflowStepApproverRepositoryObj.GetWorkflowStepApproversByStepId(stepId);
        }
    }
}
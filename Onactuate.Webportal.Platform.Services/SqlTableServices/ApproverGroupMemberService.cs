﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class ApproverGroupMemberService : IApproverGroupMemberService
    {
        private IApproverGroupMemberRepository ApproverGroupMemberRepositoryObj;

        public ApproverGroupMemberService(IApproverGroupMemberRepository approverGroupMemberRepositoryObj)
        {
            ApproverGroupMemberRepositoryObj = approverGroupMemberRepositoryObj;
        }

        public void CreateApproverGroupMember(ApproverGroupMember approverGroupMember)
        {
            ApproverGroupMemberRepositoryObj.CreateApproverGroupMember(approverGroupMember);
        }

        public void UpdateApproverGroupMember(ApproverGroupMember approverGroupMember)
        {
            ApproverGroupMemberRepositoryObj.UpdateApproverGroupMember(approverGroupMember);
        }

        public void ChangeApproverGroupMembers(IList<ApproverGroupMember> approverGroupMemberList, int approverGroupId)
        {
            ApproverGroupMemberRepositoryObj.ChangeApproverGroupMembers(approverGroupMemberList, approverGroupId);
        }

        public void DeleteApproverGroupMember(ApproverGroupMember approverGroupMember)
        {
            ApproverGroupMemberRepositoryObj.DeleteApproverGroupMember(approverGroupMember);
        }

        public void DeleteApproverGroupMemberList(IList<ApproverGroupMember> approverGroupMemberList)
        {
            ApproverGroupMemberRepositoryObj.DeleteApproverGroupMemberList(approverGroupMemberList);
        }

        public void DeleteApproverGroupMemberById(int id)
        {
            ApproverGroupMemberRepositoryObj.DeleteApproverGroupMemberById(id);
        }

        public void DeleteApproverGroupMemberByApproverGroupId(int approverGroupId)
        {
            ApproverGroupMemberRepositoryObj.DeleteApproverGroupMemberByApproverGroupId(approverGroupId);
        }

        public void DeleteApproverGroupMemberByPermissionId(int permId)
        {
            ApproverGroupMemberRepositoryObj.DeleteApproverGroupMemberByPermissionId(permId);
        }

        public IList<ApproverGroupMember> GetApproverGroupMemberList()
        {
            return ApproverGroupMemberRepositoryObj.GetApproverGroupMemberList();
        }

        public IList<ApproverGroupMember> GetApproverGroupMemberListByApproverGroupId(int approverGroupId)
        {
            return ApproverGroupMemberRepositoryObj.GetApproverGroupMemberListByApproverGroupId(approverGroupId);
        }

        public void ChangeApproverGroupMembers(IList<int> approversList, int selectedGroupId)
        {
            ApproverGroupMemberRepositoryObj.ChangeApproverGroupMembers(approversList, selectedGroupId);
        }

        public void AddApproverGroupMembers(int permissionId, int approverGroupId)
        {
            ApproverGroupMemberRepositoryObj.AddApproverGroupMembers(permissionId, approverGroupId);
        }
    }
}
﻿namespace Onactuate.Notification.Core.Constants
{
    /// <summary>
    /// This is the type of the notification
    /// Define your new notification type here
    /// </summary>
    public enum NotificaionType
    {
        /// <summary>
        /// No notification
        /// </summary>
        Empty = 0,

        /// <summary>
        /// Notification to be sent when registration is successful
        /// </summary>
        RegistrationSuccessNotification,

        /// <summary>
        /// Notification to be sent when there is an error in registration
        /// </summary>
        RegistrationErrorNotification
    }
}

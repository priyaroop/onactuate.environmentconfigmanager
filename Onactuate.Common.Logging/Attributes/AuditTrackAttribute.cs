﻿using System;

namespace Onactuate.Common.Logging.Attributes
{
    // This attribute is used to create audit logs for specific properties. We are assuming that the 
    // object can be a list or a complex type the complex type can have a list of items or properties 
    // with primitives under it if a list needs to be tracked there must be atleast one property of the 
    // listitem marked as an identifier to be able to diff the lists
    // Also we only diff identifier for the list not check list item properties 
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class AuditTrackAttribute : Attribute
    {
        public AuditTrackAttribute()
        {
            IsIdentifier = false;
        }

        public bool IsIdentifier { get; set; }

        public string Name { get; set; }
    }

}

﻿using System;
using System.Runtime.Serialization;
using System.Web;

namespace Onactuate.Common.Logging.LoggingContextProviders
{
    /// <summary>
    /// UserLoggingContextProvider
    /// </summary>
    public class HttpLoggingContextProvider : StaticLoggingContextProvider
    {
        private HttpContextBase _httpContext;

        /// <summary>
        /// Gets the server ip
        /// </summary>
        public override string ServerIP
        {
            get
            {
                string ipAddress = string.Empty;
                if (_httpContext != null &&
                    _httpContext.Request != null &&
                    _httpContext.Request.ServerVariables != null)
                {
                    ipAddress = _httpContext.Request.ServerVariables["LOCAL_ADDR"];
                }

                return ipAddress;
            }
        }

        /// <summary>
        /// Gets the client ip
        /// </summary>
        public override string ClientIP
        {
            get
            {
                string clientIP = string.Empty;
                if (_httpContext != null &&
                    _httpContext.Request != null)
                {
                    if (_httpContext.Request.Headers != null &&
                        !string.IsNullOrEmpty(_httpContext.Request.Headers["HTTP_X_FORWARDED_FOR"]))
                    {
                        clientIP = _httpContext.Request.Headers["HTTP_X_FORWARDED_FOR"];
                    }
                    else
                    {
                        try
                        {
                            clientIP = _httpContext.Request.UserHostAddress;
                        }
                        catch(SerializationException e)
                        {
                            Console.WriteLine(e);
                        }  
                        catch (NullReferenceException e)
                        {
                            Console.WriteLine(e);
                        }
                        catch (ArgumentNullException e)
                        {
                            Console.WriteLine(e);
                        }
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }

                return clientIP;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLoggingContextProvider"/> class.
        /// </summary>
        /// <param name="userState">State of the user.</param>
        /// <param name="httpContext">The HTTP context.</param>
        /// <param name="serviceName">Name of the service.</param>
        public HttpLoggingContextProvider(
            HttpContextProvider httpContextProvider,
            string serviceName)
            : base(serviceName)
        {
            if (httpContextProvider != null)
            {
                _httpContext = httpContextProvider.CurrentHttpContext();
            }
            else
            {
                _httpContext = null;
            }
        }
        public HttpLoggingContextProvider(
            HttpContextBase httpContextBase,
            string serviceName)
            : base(serviceName)
        {
            _httpContext = httpContextBase;
        }
    }
}
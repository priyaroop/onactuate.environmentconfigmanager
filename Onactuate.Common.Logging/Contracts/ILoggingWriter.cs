﻿using System;

namespace Onactuate.Common.Logging.Contracts
{
    public interface ILoggingWriter
    {
        /// <summary>
        /// Performs writing of raw log data - interface is setup mostly to fascilitate unittesting
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="correlationId">The correlation id.</param>
        /// <param name="source">The source.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="userId">The user id.</param>
        /// <param name="serverIP">The server IP.</param>
        /// <param name="clientIP">The client IP.</param>
        /// <param name="statusDescription">The status description.</param>
        /// <param name="duration">The duration.</param>
        void WriteToLog(
            Priority priority,
            string serviceName,
            Guid correlationId,
            string source,
            bool success,
            string userId,
            string serverIP,
            string clientIP,
            string statusDescription,
            double duration);
    }
}

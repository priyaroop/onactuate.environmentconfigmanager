﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Onactuate.Common.Logging.Constants;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// This WCF service behavior module performs the inspection of the incoming message to
    /// determine if the CorrelationManager.ActivityId is being sent. If not it, will then
    /// attempt to fall back to using the X-C3-CId is being set in the HTTP request headers,
    /// if present
    /// </summary>
    public class CorrelationIdInspectorBehavior : IDispatchMessageInspector
    {
        private const string ServiceModelDiagnosticNameSpace = "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics";
        private const string ActivityIdKeyName = "ActivityId";

        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// 
        /// We use this entry point to determine if the message has Activity being propagated
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate state. This object is passed back in the 
        /// <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)"/> method.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification="WCF pipeline will ensure these params are not null")]
        public object AfterReceiveRequest(
            ref Message request,
            IClientChannel channel,
            InstanceContext instanceContext)
        {
            Guid correlationIdGuid;
            string correlationIdString = string.Empty;

            int messageHeaderIndex = request.Headers.FindHeader(ActivityIdKeyName, ServiceModelDiagnosticNameSpace);

            // try to grab the correlationid string from the SOAP headers first
            if (messageHeaderIndex > -1)
            {
                correlationIdString = request.Headers.GetHeader<string>(messageHeaderIndex);
            }

            bool correlationIdStringIsEmptyStringOrEmptyGuid =
                string.IsNullOrEmpty(correlationIdString) ||
                string.Equals(correlationIdString, Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase);

            // fall back to using http header if the correlationId is still
            // empty or we've parsed an empty one from SOAP header
            if (correlationIdStringIsEmptyStringOrEmptyGuid &&
                request.Properties != null &&
                request.Properties.ContainsKey(HttpRequestMessageProperty.Name))
            {
                HttpRequestMessageProperty property = request.Properties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;

                if (property != null && property.Headers != null)
                {
                    correlationIdString = property.Headers[LoggingConstants.HttpCorrelationIdHeader];
                }
            }

            if (!Guid.TryParse(correlationIdString, out correlationIdGuid) || correlationIdGuid == Guid.Empty)
            {
                // failed to parse a valid correlationID, this suggests that this service is the first
                // entry point to the transaction
                correlationIdGuid = Guid.NewGuid();
            }

            Trace.CorrelationManager.ActivityId = correlationIdGuid;

            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// 
        /// Emit the CorrelationId back into the HTTP headers
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)"/> method.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design","CA1062:Validate arguments of public methods",MessageId = "0",Justification = "WCF pipeline will ensure these params are not null")]
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            if (Trace.CorrelationManager.ActivityId != Guid.Empty && reply != null)
            {
                HttpResponseMessageProperty httpResponse;
                if (!reply.Properties.ContainsKey(HttpResponseMessageProperty.Name))
                {
                    httpResponse = new HttpResponseMessageProperty();
                    reply.Properties.Add(HttpResponseMessageProperty.Name, httpResponse);
                }
                else
                {
                    httpResponse = (HttpResponseMessageProperty)reply.Properties[HttpResponseMessageProperty.Name];
                }

                if (httpResponse != null && httpResponse.Headers != null)
                {
                    httpResponse.Headers[LoggingConstants.HttpCorrelationIdHeader] = Trace.CorrelationManager.ActivityId.ToString("N");
                }
            }
        }
    }
}

﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// Filter for capturing logging for entry and exiting from action methods
    /// </summary>
    public class LoggingActionFilter : IActionFilter
    {
        private readonly Func<HttpContextBase, ILoggingService> _loggingServiceFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingActionFilter"/> class.
        /// We are using a factory because LoggingActionFilter is constructed at the
        /// start of the application - but we need to construct the loggingservice
        /// at the time of the action execution
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="loggingServiceFactory">The logging service factory.</param>
        public LoggingActionFilter(
            Func<HttpContextBase, ILoggingService> loggingServiceFactory)
        {
            _loggingServiceFactory = loggingServiceFactory;
        }

        /// <summary>
        /// Called before an action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ILoggingService service = _loggingServiceFactory.Invoke(filterContext.HttpContext);
            string originalRouteInfo =
                string.Format(
                    CultureInfo.InvariantCulture,
                    "Begin execution for Controller:{0} Action:{1}",
                    filterContext.RequestContext.RouteData.Values["controller"],
                    filterContext.RequestContext.RouteData.Values["action"]);

            service.Info("ActionFilter", originalRouteInfo);
        }

        /// <summary>
        /// Called after the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            ILoggingService service = _loggingServiceFactory.Invoke(filterContext.HttpContext);
            string originalRouteInfo =
                string.Format(
                    CultureInfo.InvariantCulture,
                    "End execution for Controller:{0} Action:{1}",
                    filterContext.RequestContext.RouteData.Values["controller"],
                    filterContext.RequestContext.RouteData.Values["action"]);

            service.Info("ActionFilter", originalRouteInfo);
        }
    }
}

﻿using System;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.LoggingContextProviders;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// WCF behavior for logging incoming requests and outgoing response
    /// </summary>
    public class LoggingInspectorBehavior : IDispatchMessageInspector
    {
        private readonly string _serviceName;
        private const string LoggingSource = "LoggingInspectorBehavior";

        public LoggingInspectorBehavior(string serviceName)
        {
            _serviceName = serviceName;
        }

        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)"/> method.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "Trafiglobal.Common.Logging.LoggingService.Info(System.String,System.String)", Justification="Logging values not needed for globalization")]
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            if (request != null)
            {
                LoggingService loggingService = new LoggingService(new MessageLoggingContextProvider(request.Properties, _serviceName));

                LogMessage(request, true, loggingService);
            }

            return true;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)"/> method.</param>
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            if (reply != null)
            {
                LoggingService loggingService = new LoggingService(new MessageLoggingContextProvider(reply.Properties, _serviceName));

                LogMessage(reply, false, loggingService);
            }
        }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="isEntry">if set to <c>true</c> [is entry].</param>
        /// <param name="loggingService">The logging service.</param>
        private void LogMessage(Message message, bool isEntry, ILoggingService loggingService)
        {
            // default name for operation logging
            string operationName = "Operation";

            if (message != null &&
                message.Headers != null &&
                !string.IsNullOrEmpty(message.Headers.Action))
            {
                // use action if possible - this is for the mainline WCF 
                // operation case
                string action = message.Headers.Action;
                operationName = action.Substring(action.LastIndexOf("/", StringComparison.OrdinalIgnoreCase) + 1);
            }
            else if(
                message !=null &&
                message.Properties != null &&
                message.Properties.ContainsKey("HttpOperationName"))
            {
                // for REST enabled WCF operations - the Action field is not populated
                // we will fall back to use the HttpOperationName
                operationName = message.Properties["HttpOperationName"] as string ?? operationName;
            }

            loggingService.Info(
                LoggingSource,
                string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} execution for {1}",
                    isEntry ? "Begin" : "End",
                    operationName));
        }
    }
}

﻿using NUnit.Framework;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;

namespace Onactuate.Common.Configuration.IntegrationTests
{
    [TestFixture]
    public class EnvironmentConfigurationTests
    {
        [Test]
        public void EnvironmentNameTest()
        {
            Assert.AreEqual(EnvironmentSection.GetEnvironmentName, "RegistrationWeb.Prod");
        }
    }
}
﻿using NUnit.Framework;
using Onactuate.Common.Configuration.ConfigSettings;
using Onactuate.Common.Configuration.ConfigSettings.Encryption;

namespace Onactuate.Common.Configuration.IntegrationTests
{
    [TestFixture]
    public class EncryptionConfigurationTests
    {
        [Test]
        public void GetEncryptionConfigurationInstance()
        {
            EncryptionSection instance = EncryptionSection.Instance;

            // Check that the section has been found in the machine.config
            Assert.IsNotNull(instance);

            // Check that the value of Private Key is not null or empty
            CryptoElementCollection collection = instance.EncryptionSettings;
            Assert.AreEqual(collection.Count, 2);
        }
    }
}
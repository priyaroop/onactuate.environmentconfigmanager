﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Onactuate.Common.Configuration.Models;
using Onactuate.Common.Utilities.RESTClient;

namespace Onactuate.Common.Configuration.IntegrationTests
{
    [TestFixture]
    public class EnvConfigApiTests
    {
        private Uri RestServiceBaseUrl { get; set; }
        private string AuthenticationUsername { get; set; }
        private string AuthenticationPassword { get; set; }
        private string GrantType { get; set; }

        [SetUp]
        public void Init()
        {
            RestServiceBaseUrl = new Uri("http://api.oalabsconfig.com/");
            AuthenticationUsername = "priyaroop.singh";
            AuthenticationPassword = "onGurga0n@001";
            GrantType = "password";
        }

        [TearDown]
        public void Dispose()
        {

        }

        [TestCase("Onactuate.DocPlusPlus.RegistrationWeb.DEV", "EmailSettings", "SmtpUserName")]
        [TestCase("RegistrationWeb.Prod", "EmailSettings", "SmtpUserName")]
        [TestCase("RegistrationWeb.Prod", "CreateWebSettings", "WindowsUserName")]
        public void GetEnvironmentKeyValueTest(string environmentName, string tokenName, string keyName)
        {
            SimpleRESTClient restClient = new SimpleRESTClient(RestServiceBaseUrl, AuthenticationUsername, AuthenticationPassword);
            string methodName = "EnvConfig/GetEnvironmentKeyValue";
            IDictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"environmentName", environmentName}, {"tokenName", tokenName}, {"keyName", keyName}
            };

            string response = restClient.CallHttpGetMethod<string>(methodName, new RequestParameters(parameters));
            Console.WriteLine(string.Format("{0},{1},{2} \t\t:\t{3}", environmentName, tokenName, keyName, response));
            Assert.IsNotNull(response);
        }


        [TestCase("Onactuate.DocPlusPlus.RegistrationWeb.DEV", "EmailSettings")]
        [TestCase("RegistrationWeb.Prod", "EmailSettings")]
        [TestCase("RegistrationWeb.Prod", "CreateWebSettings")]
        public void GetEnvironmentKeysTest(string environmentName, string tokenName)
        {
            SimpleRESTClient restClient = new SimpleRESTClient(RestServiceBaseUrl, AuthenticationUsername, AuthenticationPassword);
            string methodName = "EnvConfig/GetEnvironmentKeys";
            IDictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"environmentName", environmentName}, {"tokenName", tokenName}
            };

            EnvironmentKeys response = restClient.CallHttpGetMethod<EnvironmentKeys>(methodName, new RequestParameters(parameters));
            foreach (KeyValuePair<string, string> keyValuePair in response.Keys)
            {
                Console.WriteLine(string.Format("{0}\t\t\t\t:\t{1}", keyValuePair.Key, keyValuePair.Value));
            }

            Assert.IsNotNull(response);
        }
    }
}
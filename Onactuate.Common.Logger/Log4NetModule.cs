using System;
using System.Web;

namespace Onactuate.Common.Logger
{
    public class Log4NetModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            Log.EnsureInitialized();
            context.Error += context_Error;
        }

        void context_Error(object sender, EventArgs e)
        {
            Exception exception = HttpContext.Current.Server.GetLastError();
            Log.Fatal(this, exception.Message, exception);
        }

        public void Dispose()
        {
        }
    }
}
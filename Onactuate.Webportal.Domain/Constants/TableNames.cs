﻿namespace Onactuate.Webportal.Domain.Constants
{
    public static class TableNames
    {
        public const string E2EProcessIndexTable = "E2E_PROCESS_INDEX";
        public const string FeatureIndexTable = "FEATURE_INDEX";
        public const string L5ProcessIndexTable = "L5_PROCESS_INDEX";
        public const string E2E_L5_FeatureMapping = "E2E_L5_FEATURE_MAPPING";
        public const string Application_StreamWU = "APPLICATION_STREAM_WORKUNIT";
        public const string E2E_Thread = "E2E_THREAD";
        public const string FeatureBreakdownIndex = "FEATURE_BREAKDOWN_INDEX";
        public const string E2EL5FeatureMappingView = "E2E_L5_FEATURE_MAPPING_VW";
        public const string E2EL5 = "E2E_L5_MAPPING";
        public const string L5Feature = "L5_FEATURE_MAPPING";
        public const string FeatureGroupIndex = "FEATURE_GROUP_INDEX";
        public const string L5StepIndex = "L5_STEP_INDEX";
        public const string L5StepFeature = "L5_STEP_FEATURE_MAPPING";
        public const string E2EThreadEstimates = "E2E_THREAD_ESTIMATES";
    }
}
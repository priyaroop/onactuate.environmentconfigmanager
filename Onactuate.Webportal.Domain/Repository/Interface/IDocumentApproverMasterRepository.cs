﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IDocumentApproverMasterRepository
    {
        // Create new Document Approver Master
        void CreateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster);

        // Update Document Master
        void UpdateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster);

        // Delete
        void DeleteDocumentApproverMaster(DocumentApproverMaster documentApproverMaster);
        void DeleteDocumentApproverMasterList(IList<DocumentApproverMaster> documentApproverMasterList);

        // Get Data
        IList<DocumentApproverMaster> GetDocumentApproverMasterListByDocumentId(int documentId);
        IList<DocumentApproverMaster> GetDocumentApproverMasterListByPermissionId(int permissionId);
        IList<DocumentApproverMaster> GetDocumentApproverMasterListById(int id);
        IList<DocumentApproverMaster> GetDocumentApproverMasterList();
    }
}
﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IWorkflowStepRepository
    {
        // Create new Document Master
        WorkflowStep CreateWorkflowStep(WorkflowStep workflowStep);

        // Update Document Master
        WorkflowStep UpdateWorkflowStep(WorkflowStep workflowStep);

        // Delete
        void DeleteWorkflowStep(WorkflowStep workflowStep);
        void DeleteWorkflowStepList(IList<WorkflowStep> workflowStepList);

        // Get Data
        PagingResultset<WorkflowStep> GetPagedWorkflowStepList(PagingParameters<WorkflowStep> parameters);
        WorkflowStep GetPagedWorkflowStepById(int workflowStepId);
        WorkflowStep GetWorkflowStepById(int workflowStepId);
        IEnumerable<WorkflowStep> GetWorkflowStepsByWorkflowId(int workflowId);
    }
}
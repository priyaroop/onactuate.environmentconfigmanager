﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IDocumentMasterRepository
    {
        // Create new Document Master
        void CreateDocumentMaster(DocumentMaster documentMaster);
        
        // Update Document Master
        void UpdateDocumentMaster(DocumentMaster documentMaster);

        // Delete
        void DeleteDocumentMaster(DocumentMaster documentMaster);
        void DeleteDocumentMasterList(IList<DocumentMaster> documentMasterList);

        // Get Data
        IList<DocumentMaster> GetDocumentMasterListByPermissionId(int permissionId);
        PagingResultset<DocumentMaster> GetPagedDocumentMasterList(PagingParameters<DocumentMaster> parameters);
        DocumentMaster GetDocumentMasterByDocumentId(int documentMasterId);
        void DeleteDocumentMasterList(IEnumerable<int> documentsToDeleteList);
        void CreateSharepointDocumentMaster(DocumentMaster documentMaster, SharepointMaster sharepointMaster);
        SharepointMaster GetSharepointMasterByDocumentId(int masterDocId);
    }
}
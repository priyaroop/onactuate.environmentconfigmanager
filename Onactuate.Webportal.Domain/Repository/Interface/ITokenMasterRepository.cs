﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface ITokenMasterRepository
    {
        IEnumerable<TokenMaster> GetTokenMasterList();
        IEnumerable<TokenMaster> GetTokenMasterListByEnvironmentId(int environmentId);
        TokenMaster GetTokenMasterByEnvironmentIdAndTokeName(int environmentId, string tokenName);

        void CreateTokenMaster(TokenMaster tokenMaster);
        void CreateTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList);
        void UpdateTokenMaster(TokenMaster tokenMaster);
        void UpdateTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList);
        void DeleteTokenMaster(TokenMaster tokenMaster);
        void DeleteTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList);
    }
}
﻿using System.Collections.Generic;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface ISqlBulkCopyRepository
    {
        void BulkCopy<T>(IEnumerable<T> entitiesList, string tableName);

    }
}
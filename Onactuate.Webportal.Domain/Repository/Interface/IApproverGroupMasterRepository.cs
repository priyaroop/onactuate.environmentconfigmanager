﻿using System.Collections.Generic;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IApproverGroupMasterRepository
    {
        // Create new Approver Group Master
        void CreateApproverGroupMaster(ApproverGroupMaster approverGroupMaster);

        // Update Document Master
        void UpdateApproverGroupMaster(ApproverGroupMaster approverGroupMaster);

        // Delete
        void DeleteApproverGroupMaster(ApproverGroupMaster approverGroupMaster);

        //change for delete approver group name ==  void DeleteApproverGroupMasterList(IList<ApproverGroupMaster> approverGroupMasterList);
       // void DeleteApproverGroupMasterList(IList<int> approverGroupMasterList);
        void DeleteApproverGroupMasterList(IList<ApproverGroupMaster> approverGroupMasterList);
        void DeleteApproverGroupMasterById(int id);

        // Get Data
        IList<ApproverGroupMaster> GetApproverGroupMasterList();
        IList<ApproverGroupMaster> GetApproverGroupMasterListById(int id);
        IList<ApproverGroupMaster> GetApproverGroupMasterListByGroupName(string groupName);
    }
}
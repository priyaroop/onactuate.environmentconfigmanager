﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IKeyMasterRepository
    {
        IEnumerable<KeyMaster> GetKeyMasterList();
        IEnumerable<KeyMaster> GetKeyMasterListByTokenId(int tokenId);
        KeyMaster GetKeyMasterByKeyId(int keyId);
        IEnumerable<KeyMaster> GetKeyMasterByKeyName(string keyName);
        IEnumerable<KeyMaster> GetKeyMasterListByEnvironmentId(int environmentId);
        PagingResultset<KeyMaster> GetKeyMasterPagedResultSetByEnvironmentId(PagingParameters<KeyMaster> parameters, int environmentId);

        void CreateKeyMaster(KeyMaster keyMaster);
        void CreateKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList);
        void UpdateKeyMaster(KeyMaster keyMaster);
        void UpdateKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList);
        void DeleteKeyMaster(KeyMaster keyMaster);
        void DeleteKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList);
        void DeleteKeyMasterByKeyId(int keyId);
        void DeleteKeyMasterByKeyIdList(IEnumerable<int> keyIdList);
    }
}
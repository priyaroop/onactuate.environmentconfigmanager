﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IWorkflowStepApproverRepository
    {
        // Create new Document Master
        WorkflowStepApprover CreateWorkflowStepApprover(WorkflowStepApprover workflowStepApprover);

        // Update Document Master
        WorkflowStepApprover UpdateWorkflowStepApprover(WorkflowStepApprover workflowStepApprover);

        // Delete
        void DeleteWorkflowStepApprover(WorkflowStepApprover workflowStepApprover);
        void DeleteWorkflowStepApproverList(IList<WorkflowStepApprover> workflowStepList);

        // Get Data
        PagingResultset<WorkflowStepApprover> GetPagedWorkflowStepApproverList(PagingParameters<WorkflowStepApprover> parameters);
        WorkflowStepApprover GetPagedWorkflowStepApproverById(int workflowStepApproverId);
        WorkflowStepApprover GetWorkflowStepApproverById(int workflowStepApproverId);
        IEnumerable<WorkflowStepApprover> GetWorkflowStepApproversByStepId(int stepId);
        
    }
}
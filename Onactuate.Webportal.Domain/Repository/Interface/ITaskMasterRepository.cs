﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface ITaskMasterRepository
    {
        // Create new Document Master
        void CreateTaskMaster(TaskMaster taskMaster);

        // Update Document Master
        void UpdateTaskMaster(TaskMaster taskMaster);

        // Delete
        void DeleteTaskMaster(TaskMaster taskMaster);
        void DeleteTaskMasterList(IList<TaskMaster> taskMasterList);

        // Get Data
        IList<TaskMaster> GetTaskMasterListByPermissionId(int permissionId);
        PagingResultset<TaskMaster> GetPagedTaskMasterList(PagingParameters<TaskMaster> parameters);
        TaskMaster GetPagedTaskMasterById(int taskId);
        TaskMaster GetTaskMasterById(int taskId);
        IEnumerable<TaskMaster> GetTaskToApproveList(int permissionId);
        void MarkTaskAsOld(int taskId);
    }
}
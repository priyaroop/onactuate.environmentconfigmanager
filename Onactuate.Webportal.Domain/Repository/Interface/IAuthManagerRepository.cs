﻿using System.Collections.Generic;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface IAuthManagerRepository
    {
        void AddSystemUsers(IEnumerable<ApplicationPermissions> permissionsList);
        void CreateApplicationPermission(ApplicationPermissions newPermission);
        void UpdateSystemUsers(IEnumerable<ApplicationPermissions> permissionsList);
        ApplicationPermissions IsAuthorizedUser(string userName, string password);
        IList<ApplicationPermissions> GetApplicationPermissionsList();
        ApplicationPermissions GetApplicationPermissionsById(int userId);
        IList<ApplicationPermissions> GetApplicationPermissionsListByEnvironment(int envId);

        void SaveApplicationPermissions(ApplicationPermissions permission);
        void CreateApplicationPermissions(ApplicationPermissions permission);
        void DeleteApplicationPermissionByPermId(int permId);
        PagingResultset<ApplicationPermissions> GetPagedApplicationPermissionsList(PagingParameters<ApplicationPermissions> parameters);
    }
}


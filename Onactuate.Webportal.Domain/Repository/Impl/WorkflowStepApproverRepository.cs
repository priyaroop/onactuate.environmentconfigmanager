﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class WorkflowStepApproverRepository : RepositoryBase, IWorkflowStepApproverRepository
    {
        public WorkflowStepApproverRepository()
        {
        }

        protected WorkflowStepApproverRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public WorkflowStepApprover CreateWorkflowStepApprover(WorkflowStepApprover workflowStepApprover)
        {
            base.SaveChanges<WorkflowStepApprover>(workflowStepApprover);
            return workflowStepApprover;
        }

        public WorkflowStepApprover UpdateWorkflowStepApprover(WorkflowStepApprover workflowStepApprover)
        {
            base.Update<WorkflowStepApprover>(workflowStepApprover);
            return workflowStepApprover;
        }

        public void DeleteWorkflowStepApprover(WorkflowStepApprover workflowStepApprover)
        {
            Delete<WorkflowStepApprover>(workflowStepApprover);
        }

        public void DeleteWorkflowStepApproverList(IList<WorkflowStepApprover> workflowStepApproverList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (WorkflowStepApprover workflowStepApprover in workflowStepApproverList)
                {
                    DeleteWorkflowStepApprover(workflowStepApprover);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public PagingResultset<WorkflowStepApprover> GetPagedWorkflowStepApproverList(PagingParameters<WorkflowStepApprover> parameters)
        {
            return base.GetPagedResultset(parameters);
        }

        public WorkflowStepApprover GetPagedWorkflowStepApproverById(int taskId)
        {
            return base.GetQuery<WorkflowStepApprover>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public WorkflowStepApprover GetWorkflowStepApproverById(int taskId)
        {
            return base.GetQuery<WorkflowStepApprover>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public IEnumerable<WorkflowStepApprover> GetWorkflowStepApproversByStepId(int stepId)
        {
            return base.GetQuery<WorkflowStepApprover>(t => t.WorkflowStepId.Equals(stepId));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class TaskMasterRepository : RepositoryBase, ITaskMasterRepository
    {
        public TaskMasterRepository()
        {
        }

        protected TaskMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void CreateTaskMaster(TaskMaster taskMaster)
        {
            base.SaveChanges<TaskMaster>(taskMaster);
        }

        public void UpdateTaskMaster(TaskMaster taskMaster)
        {
            base.Update<TaskMaster>(taskMaster);
        }

        public void DeleteTaskMaster(TaskMaster taskMaster)
        {
            Delete<TaskMaster>(taskMaster);
        }

        public void DeleteTaskMasterList(IList<TaskMaster> taskMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (TaskMaster taskMaster in taskMasterList)
                {
                    DeleteTaskMaster(taskMaster);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public IList<TaskMaster> GetTaskMasterListByPermissionId(int permissionId)
        {
            return base.GetQuery<TaskMaster>(p => p.OwnerPermissionId.Equals(permissionId) || p.AssignedToPermissionId.Equals(permissionId)).ToList();
        }

        public PagingResultset<TaskMaster> GetPagedTaskMasterList(PagingParameters<TaskMaster> parameters)
        {
            return base.GetPagedResultset(parameters);
        }

        public TaskMaster GetPagedTaskMasterById(int taskId)
        {
            return base.GetQuery<TaskMaster>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public TaskMaster GetTaskMasterById(int taskId)
        {
            return base.GetQuery<TaskMaster>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public IEnumerable<TaskMaster> GetTaskToApproveList(int permissionId)
        {
            return from task in base.GetQuery<TaskMaster>()
                join appGrp in base.GetQuery<ApproverGroupMaster>()
                    on task.ApproverGroupId equals appGrp.Id
                    join appMember in base.GetQuery<ApproverGroupMember>()
                   on appGrp.Id equals appMember.ApproverGroupId
                   where appMember.PermissionId.Equals(permissionId)
                   && (task.TaskStatusId == TaskStatus.SentForApproval)
                select task;
        }

        public void MarkTaskAsOld(int taskId)
        {
            TaskMaster master = GetQuery<TaskMaster>(p => p.Id.Equals(taskId)).FirstOrDefault();

            if (master != null)
            {
                master.IsNewTask = false;
                base.Update<TaskMaster>(master);
            }
        }
    }
}
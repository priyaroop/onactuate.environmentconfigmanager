﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class ApproverGroupMasterRepository : RepositoryBase, IApproverGroupMasterRepository
    {
        public ApproverGroupMasterRepository()
        {
        }

        public ApproverGroupMasterRepository(ObjectContext context) : base(context)
        {
        }
        
        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }


        public void CreateApproverGroupMaster(ApproverGroupMaster approverGroupMaster)
        {
            base.SaveChanges<ApproverGroupMaster>(approverGroupMaster);
        }

        public void UpdateApproverGroupMaster(ApproverGroupMaster approverGroupMaster)
        {
            base.Update<ApproverGroupMaster>(approverGroupMaster);
        }

        public void DeleteApproverGroupMaster(ApproverGroupMaster approverGroupMaster)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                // Delete all the members first. then delete the group
                // comment to void del member
                //foreach (ApproverGroupMember member in GetQuery<ApproverGroupMember>())
                //{
                //    base.Delete<ApproverGroupMember>(member);
                //}
                base.Delete<ApproverGroupMaster>(approverGroupMaster);

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        //change for delete approver group name
        public void DeleteApproverGroupMasterList(IList<ApproverGroupMaster> approverGroupMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApproverGroupMaster approverGroupMaster in approverGroupMasterList)
                {
                    DeleteApproverGroupMaster(approverGroupMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        //jasneet code strt
        //public void DeleteApproverGroupMasterList(IList<int> approverGroupMasterList)
        //{
        //    TransactionScope scope = this.CreateTransactionScope();
        //    try
        //    {
        //        DeleteApproverGroupMaster(GetQuery<ApproverGroupMaster>(p => p.Id.Equals(approverGroupMasterList)).FirstOrDefault());

        //        scope.Complete();
        //    }
        //    catch (Exception exception)
        //    {
        //        string message = exception.Message;
        //        if (exception.InnerException != null)
        //        {
        //            message = exception.InnerException.Message;
        //        }
        //        throw new Exception(message, exception);
        //    }
        //    finally
        //    {
        //        if (scope != null)
        //        {
        //            scope.Dispose();
        //        }
        //    }
        //}

//jasneet end



        public void DeleteApproverGroupMasterById(int id)
        {
            DeleteApproverGroupMaster(GetQuery<ApproverGroupMaster>(p => p.Id.Equals(id)).FirstOrDefault());
        }

        public IList<ApproverGroupMaster> GetApproverGroupMasterList()
        {
            return base.GetQuery<ApproverGroupMaster>() == null
                ? new List<ApproverGroupMaster>()
                : base.GetQuery<ApproverGroupMaster>().ToList();
        }

        public IList<ApproverGroupMaster> GetApproverGroupMasterListById(int id)
        {
            return base.GetQuery<ApproverGroupMaster>(p => p.Id.Equals(id)) == null
                ? new List<ApproverGroupMaster>()
                : base.GetQuery<ApproverGroupMaster>(p => p.Id.Equals(id)).ToList();
        }

        public IList<ApproverGroupMaster> GetApproverGroupMasterListByGroupName(string groupName)
        {
            return base.GetQuery<ApproverGroupMaster>(p => p.GroupName.Equals(groupName, StringComparison.OrdinalIgnoreCase)) == null
                ? new List<ApproverGroupMaster>()
                : base.GetQuery<ApproverGroupMaster>(p => p.GroupName.Equals(groupName, StringComparison.OrdinalIgnoreCase)).ToList();
        }
    }
}
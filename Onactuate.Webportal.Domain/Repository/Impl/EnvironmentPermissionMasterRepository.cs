﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Extensions;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class EnvironmentPermissionMasterRepository : RepositoryBase, IEnvironmentPermissionMasterRepository
    {
        public EnvironmentPermissionMasterRepository()
        {
        }

        public EnvironmentPermissionMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public IEnumerable<EnvironmentPermissionMaster> GeEnvironmentPermissionMasterList()
        {
            return base.GetQuery<EnvironmentPermissionMaster>();
        }

        public IEnumerable<EnvironmentPermissionMaster> GetEnvironmentPermissionMasterListByPermissionId(int permissionId)
        {
            return base.GetQuery<EnvironmentPermissionMaster>(p => p.PermissionId.Equals(permissionId));
        }

        public IEnumerable<EnvironmentPermissionMaster> GetEnvironmentPermissionMasterListByEnvironmentId(int environmentId)
        {
            return base.GetQuery<EnvironmentPermissionMaster>(p => p.EnvironmentId.Equals(environmentId));
        }

        public PagingResultset<EnvironmentPermissionMaster> GetPagedEnvironmentPermissionsList(PagingParameters<EnvironmentPermissionMaster> parameters, int permissionId)
        {
            IEnumerable<EnvironmentPermissionMaster> tempList = GetEnvironmentPermissionMasterListByPermissionId(permissionId);
            List<EnvironmentPermissionMaster> completeTempList = new List<EnvironmentPermissionMaster>();
            foreach (EnvironmentPermissionMaster value in tempList)
            {
                if (value.IsAdmin)
                {
                    completeTempList.AddRange(GetEnvironmentPermissionMasterListByEnvironmentId(value.EnvironmentId));
                }
                else
                {
                    completeTempList.Add(value);
                }
            }
            
            return PagingResultSetExtensions.GetPagedResultset(completeTempList.AsQueryable(), parameters);
        }

        public void CreateEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster)
        {
            base.SaveChanges<EnvironmentPermissionMaster>(environmentPermissionMaster);
        }

        public void CreateEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (EnvironmentPermissionMaster environmentPermissionMaster in environmentPermissionMasterList)
                {
                    base.SaveChanges<EnvironmentPermissionMaster>(environmentPermissionMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void UpdateEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster)
        {
            base.Update<EnvironmentPermissionMaster>(environmentPermissionMaster);
        }

        public void UpdateEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (EnvironmentPermissionMaster environmentPermissionMaster in environmentPermissionMasterList)
                {
                    base.Update<EnvironmentPermissionMaster>(environmentPermissionMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteEnvironmentPermissionMaster(EnvironmentPermissionMaster environmentPermissionMaster)
        {
            base.Delete<EnvironmentPermissionMaster>(environmentPermissionMaster);
        }

        public void DeleteEnvironmentPermissionMasterMultiple(IEnumerable<EnvironmentPermissionMaster> environmentPermissionMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (EnvironmentPermissionMaster environmentPermissionMaster in environmentPermissionMasterList)
                {
                    base.Delete<EnvironmentPermissionMaster>(environmentPermissionMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteEnvironmentPermissionMasterById(int environmentPermissionId)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                EnvironmentPermissionMaster toDelete = GetQuery<EnvironmentPermissionMaster>(p=>p.EnvironmentPermissionId.Equals(environmentPermissionId)).FirstOrDefault();

                if (toDelete != null)
                {
                    base.Delete<EnvironmentPermissionMaster>(toDelete);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class DocumentMasterRepository : RepositoryBase, IDocumentMasterRepository
    {
        public DocumentMasterRepository()
        {
        }

        protected DocumentMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }
        
        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void CreateDocumentMaster(DocumentMaster documentMaster)
        {
            base.SaveChanges<DocumentMaster>(documentMaster);
        }

        public void UpdateDocumentMaster(DocumentMaster documentMaster)
        {
            base.Update<DocumentMaster>(documentMaster);
        }
        
        public void DeleteDocumentMaster(DocumentMaster documentMaster)
        {
            // TBD:: How to delete the file
            // Delete the file associated with the Document Master
            //File.Delete(documentMaster.DocumentName);

            switch (documentMaster.DocumentType)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    SharepointMaster sharepointDoc = base.GetQuery<SharepointMaster>(p => p.DocumentMasterId.Equals(documentMaster.Id)).FirstOrDefault();
                    if (sharepointDoc != null)
                    {
                        Delete<SharepointMaster>(sharepointDoc);
                    }
                    break;
                case 3:
                    SharepointMaster oneDriveDoc = base.GetQuery<SharepointMaster>(p => p.DocumentMasterId.Equals(documentMaster.Id)).FirstOrDefault();
                    if (oneDriveDoc != null)
                    {
                        Delete<SharepointMaster>(oneDriveDoc);
                    }
                    break;
            }

            // Delete the Document from database
            Delete<DocumentMaster>(documentMaster);
        }

        public void DeleteDocumentMasterList(IList<DocumentMaster> documentMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (DocumentMaster documentMaster in documentMasterList)
                {
                    DeleteDocumentMaster(documentMaster);
                }
                
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }
        
        public IList<DocumentMaster> GetDocumentMasterListByPermissionId(int permissionId)
        {
            return base.GetQuery<DocumentMaster>(p => p.UploadedByPermissionId.Equals(permissionId)).ToList();
        }

        public PagingResultset<DocumentMaster> GetPagedDocumentMasterList(PagingParameters<DocumentMaster> parameters)
        {
            return base.GetPagedResultset(parameters);
        }

        public DocumentMaster GetDocumentMasterByDocumentId(int documentMasterId)
        {
            return base.GetQuery<DocumentMaster>(p => p.Id.Equals(documentMasterId)).FirstOrDefault();

        }

        public void DeleteDocumentMasterList(IEnumerable<int> documentsToDeleteList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (int documentId in documentsToDeleteList)
                {
                    DocumentMaster doc = GetDocumentMasterByDocumentId(documentId);

                    // Get all tasks for the document
                    foreach (TaskMaster task in GetQuery<TaskMaster>(tm => tm.MasterDocumentId.Equals(documentId)))
                    {
                        // Get all comments for the tasks
                        foreach (TaskComment comment in GetQuery<TaskComment>(tc => tc.TaskId.Equals(task.Id)))
                        {
                            // Delete this comment
                            Delete<TaskComment>(comment);
                        }

                        // TBD:: How to delete the file
                        // Delete the file associated with the task
                        //File.Delete(task.TaskDocumentName); task.TaskDocumentName);

                        // Delete this task
                        Delete<TaskMaster>(task);
                    }

                    // Delete the master document
                    DeleteDocumentMaster(doc);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void CreateSharepointDocumentMaster(DocumentMaster documentMaster, SharepointMaster sharepointMaster)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                base.SaveChanges<DocumentMaster>(documentMaster);
                sharepointMaster.DocumentMasterId = documentMaster.Id;

                base.SaveChanges<SharepointMaster>(sharepointMaster);

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public SharepointMaster GetSharepointMasterByDocumentId(int masterDocId)
        {
            return base.GetQuery<SharepointMaster>(p => p.DocumentMasterId.Equals(masterDocId)).FirstOrDefault();
        }
    }
}
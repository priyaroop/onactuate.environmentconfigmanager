﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.Configuration.Lektor;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class AuthManagerRepository : RepositoryBase, IAuthManagerRepository
    {
        public AuthManagerRepository()
        {
        }

        public AuthManagerRepository(ObjectContext context) : base(context)
        {
        }
        
        public void CreateApplicationPermission(ApplicationPermissions newPermission)
        {
            base.SaveChanges<ApplicationPermissions>(newPermission);
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void AddSystemUsers(IEnumerable<ApplicationPermissions> permissionsList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApplicationPermissions permissions in permissionsList)
                {
                    base.SaveChanges<ApplicationPermissions>(permissions);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeletePermissionByPermissionId(int permissionId)
        {
            ApplicationPermissions entity = Queryable.FirstOrDefault<ApplicationPermissions>(base.GetQuery<ApplicationPermissions>(), p => p.PermissionId.Equals(permissionId));
            if (entity != null)
            {
                base.Delete<ApplicationPermissions>(entity);
            }
        }
        
        public void DeleteSystemUsers(IEnumerable<ApplicationPermissions> permissionsList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApplicationPermissions permissions in permissionsList)
                {
                    base.Delete<ApplicationPermissions>(permissions);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }
        
        public void UpdateSystemUserPermission(int permissionId)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                ApplicationPermissions entity = Queryable.FirstOrDefault<ApplicationPermissions>(base.GetQuery<ApplicationPermissions>(), p => p.PermissionId.Equals(permissionId));
                if (entity != null)
                {
                    int num = (entity.PermissionTypeId + 1) % 3;
                    entity.PermissionTypeId = num;
                    base.Update<ApplicationPermissions>(entity);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void UpdateSystemUsers(IEnumerable<ApplicationPermissions> permissionsList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApplicationPermissions permissions in permissionsList)
                {
                    base.Update<ApplicationPermissions>(permissions);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public ApplicationPermissions IsAuthorizedUser(string userName, string password)
        {
            // Check username exists in the database
            ApplicationPermissions perms = base.GetQuery<ApplicationPermissions>(p => p.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault<ApplicationPermissions>();

            if (perms == null)
            {
                throw new Exception("Username does not exist. Please enter correct username and password.");
            }

            // Check if the password entered is correct or not
            if (!password.Equals(password))
            {
                throw new Exception("Password mismatch. Please enter correct username and password.");
            }

            return perms;
        }

        public IList<ApplicationPermissions> GetApplicationPermissionsList()
        {
            return base.GetQuery<ApplicationPermissions>().ToList();
        }

        public ApplicationPermissions GetApplicationPermissionsById(int userId)
        {
            return
                base.GetQuery<ApplicationPermissions>(
                    p =>
                        p.PermissionId.Equals(userId))
                    .FirstOrDefault<ApplicationPermissions>();
        }

        public IList<ApplicationPermissions> GetApplicationPermissionsListByEnvironment(int envId)
        {
            IQueryable<ApplicationPermissions> permList = from perm in base.GetQuery<ApplicationPermissions>()
                join env in base.GetQuery<EnvironmentPermissionMaster>()
                    on perm.PermissionId equals env.PermissionId
                where env.EnvironmentId.Equals(envId)
                select perm;

            return base.GetQuery<ApplicationPermissions>().Except(permList).ToList();
        }

        public void SaveApplicationPermissions(ApplicationPermissions permission)
        {
            base.Update<ApplicationPermissions>(permission);
        }
        
        public void CreateApplicationPermissions(ApplicationPermissions permission)
        {
            SaveChanges<ApplicationPermissions>(permission);
        }

        public void DeleteApplicationPermissionByPermId(int permId)
        {
            ApplicationPermissions permissions =
                base.GetQuery<ApplicationPermissions>(p => p.PermissionId.Equals(permId))
                    .FirstOrDefault<ApplicationPermissions>();

            if (permissions != null)
            {
                Delete<ApplicationPermissions>(permissions);
            }
        }

        public PagingResultset<ApplicationPermissions> GetPagedApplicationPermissionsList(PagingParameters<ApplicationPermissions> parameters)
        {
            return base.GetPagedResultset(parameters);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Extensions;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class KeyMasterRepository : RepositoryBase, IKeyMasterRepository
    {
        public KeyMasterRepository()
        {
        }

        public KeyMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }


        public IEnumerable<KeyMaster> GetKeyMasterList()
        {
            return base.GetQuery<KeyMaster>();
        }

        public IEnumerable<KeyMaster> GetKeyMasterListByTokenId(int tokenId)
        {
            return base.GetQuery<KeyMaster>(p => p.TokenId.Equals(tokenId));
        }
        public KeyMaster GetKeyMasterByKeyId(int keyId)
        {
            return base.GetQuery<KeyMaster>(p => p.KeyId.Equals(keyId)).FirstOrDefault();
        }

        public IEnumerable<KeyMaster> GetKeyMasterByKeyName(string keyName)
        {
            return base.GetQuery<KeyMaster>(p => p.KeyName.Equals(keyName, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<KeyMaster> GetKeyMasterListByEnvironmentId(int environmentId)
        {
            return
                from key in base.GetQuery<KeyMaster>()
                join token in base.GetQuery<TokenMaster>()
                    on key.TokenId equals token.TokenId
                join env in base.GetQuery<EnvironmentMaster>(e => e.EnvironmentId.Equals(environmentId))
                    on token.EnvironmentId equals env.EnvironmentId
                orderby env.EnvironmentId, token.TokenName, key.KeyName
                select key;
        }

        public PagingResultset<KeyMaster> GetKeyMasterPagedResultSetByEnvironmentId(PagingParameters<KeyMaster> parameters, int environmentId)
        {
            IEnumerable<KeyMaster> tempKeyList = GetKeyMasterListByEnvironmentId(environmentId);

            return PagingResultSetExtensions.GetPagedResultset(tempKeyList.AsQueryable(), parameters);
        }

        public void CreateKeyMaster(KeyMaster keyMaster)
        {
            base.SaveChanges<KeyMaster>(keyMaster);
        }

        public void CreateKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (KeyMaster keyMaster in keyMasterList)
                {
                    base.SaveChanges<KeyMaster>(keyMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void UpdateKeyMaster(KeyMaster keyMaster)
        {
            base.Update<KeyMaster>(keyMaster);
        }

        public void UpdateKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (KeyMaster keyMaster in keyMasterList)
                {
                    base.Update<KeyMaster>(keyMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteKeyMaster(KeyMaster keyMaster)
        {
            base.Delete<KeyMaster>(keyMaster);
        }

        public void DeleteKeyMasterMultiple(IEnumerable<KeyMaster> keyMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (KeyMaster keyMaster in keyMasterList)
                {
                    base.Delete<KeyMaster>(keyMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteKeyMasterByKeyId(int keyId)
        {
            KeyMaster keyToDelete = GetKeyMasterByKeyId(keyId);
            if (keyToDelete != null)
            {
                DeleteKeyMaster(keyToDelete);
            }
        }

        public void DeleteKeyMasterByKeyIdList(IEnumerable<int> keyIdList)
        {
            IEnumerable<KeyMaster> keyMasterListToDelete = keyIdList.Select(GetKeyMasterByKeyId).ToList();
            DeleteKeyMasterMultiple(keyMasterListToDelete);
        }
    }
}
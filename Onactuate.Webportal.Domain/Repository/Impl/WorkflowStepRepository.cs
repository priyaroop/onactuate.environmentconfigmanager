﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class WorkflowStepRepository : RepositoryBase, IWorkflowStepRepository
    {
        public WorkflowStepRepository()
        {
        }

        protected WorkflowStepRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public WorkflowStep CreateWorkflowStep(WorkflowStep workflowStep)
        {
            base.SaveChanges<WorkflowStep>(workflowStep);
            return workflowStep;
        }

        public WorkflowStep UpdateWorkflowStep(WorkflowStep workflowStep)
        {
            base.Update<WorkflowStep>(workflowStep);
            return workflowStep;
        }

        public void DeleteWorkflowStep(WorkflowStep workflowStep)
        {
            Delete<WorkflowStep>(workflowStep);
        }

        public void DeleteWorkflowStepList(IList<WorkflowStep> taskMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (WorkflowStep workflowStep in taskMasterList)
                {
                    DeleteWorkflowStep(workflowStep);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public PagingResultset<WorkflowStep> GetPagedWorkflowStepList(PagingParameters<WorkflowStep> parameters)
        {
            return base.GetPagedResultset(parameters);
        }

        public WorkflowStep GetPagedWorkflowStepById(int taskId)
        {
            return base.GetQuery<WorkflowStep>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public WorkflowStep GetWorkflowStepById(int taskId)
        {
            return base.GetQuery<WorkflowStep>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public IEnumerable<WorkflowStep> GetWorkflowStepsByWorkflowId(int workflowId)
        {
            return base.GetQuery<WorkflowStep>(t=>t.WorkflowMasterId==workflowId);
        }
    }
}
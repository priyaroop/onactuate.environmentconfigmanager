﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class WorkflowMasterRepository : RepositoryBase, IWorkflowMasterRepository
    {
        public WorkflowMasterRepository()
        {
        }

        protected WorkflowMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void CreateWorkflowMaster(WorkflowMaster taskMaster)
        {
            base.SaveChanges<WorkflowMaster>(taskMaster);
        }

        public void UpdateWorkflowMaster(WorkflowMaster taskMaster)
        {
            base.Update<WorkflowMaster>(taskMaster);
        }

        public void DeleteWorkflowMaster(WorkflowMaster taskMaster)
        {
            Delete<WorkflowMaster>(taskMaster);
        }

        public void DeleteWorkflowMasterList(IList<WorkflowMaster> taskMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (WorkflowMaster taskMaster in taskMasterList)
                {
                    DeleteWorkflowMaster(taskMaster);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public PagingResultset<WorkflowMaster> GetPagedWorkflowMasterList(PagingParameters<WorkflowMaster> parameters)
        {
            return base.GetPagedResultset(parameters);
        }

        public WorkflowMaster GetWorkflowMasterById(int taskId)
        {
            return base.GetQuery<WorkflowMaster>(t => t.Id.Equals(taskId)).FirstOrDefault();
        }

        public WorkflowMaster GetWorkflowMasterByName(string workflowName)
        {
            return base.GetQuery<WorkflowMaster>(t => t.WorkflowName.Equals(workflowName,StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        public IList<WorkflowMaster> GetWorkflowList()
        {
            return base.GetAll<WorkflowMaster>();
        }

        public void DeleteWorkflowMaster(int id)
        {
            base.Delete<WorkflowMaster>(x=>x.Id==id);
        }
    }
}
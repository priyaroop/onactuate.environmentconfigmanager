﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class ApproverGroupMemberRepository : RepositoryBase,  IApproverGroupMemberRepository
    {
        public ApproverGroupMemberRepository()
        {
        }

        public ApproverGroupMemberRepository(ObjectContext context) : base(context)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void CreateApproverGroupMember(ApproverGroupMember approverGroupMember)
        {
            base.SaveChanges(approverGroupMember);
        }

        public void UpdateApproverGroupMember(ApproverGroupMember approverGroupMember)
        {
            base.Update(approverGroupMember);
        }

        public void ChangeApproverGroupMembers(IList<ApproverGroupMember> approverGroupMemberList, int approverGroupId)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                // Delete existing approvers/ members of the approver group
                IList<ApproverGroupMember> members = GetQuery<ApproverGroupMember>(p => p.ApproverGroupId.Equals(approverGroupId)).ToList();
                DeleteApproverGroupMemberList(members);

                // Add the new list of members
                foreach (ApproverGroupMember member in approverGroupMemberList)
                {
                    base.SaveChanges<ApproverGroupMember>(member);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        
        public void DeleteApproverGroupMember(ApproverGroupMember approverGroupMember)
        {
            base.Delete(approverGroupMember);
        }

        public void DeleteApproverGroupMemberList(IList<ApproverGroupMember> approverGroupMemberList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApproverGroupMember member in approverGroupMemberList)
                {
                    base.Delete<ApproverGroupMember>(member);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteApproverGroupMemberById(int id)
        {
            ApproverGroupMember member = GetQuery<ApproverGroupMember>(p => p.Id.Equals(id)).FirstOrDefault();
            if(member != null)
                base.Delete(member);
        }

        public void DeleteApproverGroupMemberByApproverGroupId(int approverGroupId)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApproverGroupMember member in GetQuery<ApproverGroupMember>(p=>p.ApproverGroupId.Equals(approverGroupId)))
                {
                    base.Delete<ApproverGroupMember>(member);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteApproverGroupMemberByPermissionId(int permId)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (ApproverGroupMember member in GetQuery<ApproverGroupMember>(p => p.PermissionId.Equals(permId)))
                {
                    base.Delete<ApproverGroupMember>(member);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public IList<ApproverGroupMember> GetApproverGroupMemberList()
        {
            return GetQuery<ApproverGroupMember>().ToList();
        }

        public IList<ApproverGroupMember> GetApproverGroupMemberListByApproverGroupId(int approverGroupId)
        {
            return GetQuery<ApproverGroupMember>(p=>p.ApproverGroupId.Equals(approverGroupId)).ToList();
        }

        public void ChangeApproverGroupMembers(IList<int> approversList, int selectedGroupId)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                // Delete existing approvers/ members of the approver group
                IList<ApproverGroupMember> members = GetQuery<ApproverGroupMember>(p => p.ApproverGroupId.Equals(selectedGroupId)).ToList();

                DeleteApproverGroupMemberList(members);

                // Add the new list of members
                foreach (int permId in approversList)
                {
                    ApproverGroupMember member = new ApproverGroupMember()
                    {
                        PermissionId = permId,
                        ApproverGroupId = selectedGroupId
                    };

                    base.SaveChanges<ApproverGroupMember>(member);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void AddApproverGroupMembers(int permissionId, int approverGroupId)
        {
            ApproverGroupMember member = new ApproverGroupMember()
            {
                PermissionId = permissionId,
                ApproverGroupId = approverGroupId
            };

            base.SaveChanges<ApproverGroupMember>(member);
        }
    }
}
﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class SqlBulkCopyRepository : RepositoryBase, ISqlBulkCopyRepository
    {
        public SqlBulkCopyRepository()
        {
        }
        public SqlBulkCopyRepository(ObjectContext context) : base(context)
        {
        }

        public void BulkCopy<T>(IEnumerable<T> entitiesList, string tableName)
        {
            SqlBulkCopy sbCopy = new SqlBulkCopy(connectionString: base.ConnectionString)
            {
                DestinationTableName = tableName
            };

            sbCopy.WriteToServer(entitiesList.AsDataReader());
        }
    }
}
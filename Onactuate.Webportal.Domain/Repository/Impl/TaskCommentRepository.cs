﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class TaskCommentRepository : RepositoryBase, ITaskCommentRepository
    {
        public TaskCommentRepository()
        {
        }

        protected TaskCommentRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void CreateTaskComment(TaskComment taskComment)
        {
            base.SaveChanges<TaskComment>(taskComment);
        }

        public void UpdateTaskComment(TaskComment taskComment)
        {
            base.Update(taskComment);
        }

        public void DeleteTaskComment(TaskComment taskComment)
        {
            base.Delete(taskComment);
        }

        public void DeleteTaskCommentList(IList<TaskComment> taskCommentList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (TaskComment taskComment in taskCommentList)
                {
                    DeleteTaskComment(taskComment);
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public IList<TaskComment> GetTaskCommentListByPermissionId(int permissionId)
        {
            return base.GetQuery<TaskComment>(p => p.Id.Equals(permissionId)).ToList();
        }

        public PagingResultset<TaskComment> GetPagedTaskCommentList(PagingParameters<TaskComment> parameters)
        {
            return base.GetPagedResultset(parameters);
        }

        public TaskComment GetPagedTaskCommentById(int taskId)
        {
            return base.GetQuery<TaskComment>(p => p.Id.Equals(taskId)).FirstOrDefault();
        }

        public List<TaskComment> GetTaskCommentListByDocumentId(int model)
        {
            return
                (from comment in base.GetQuery<TaskComment>()
                    join task in base.GetQuery<TaskMaster>()
                        on comment.TaskId equals task.Id
                    join doc in base.GetQuery<DocumentMaster>()
                        on task.MasterDocumentId equals doc.Id
                    where doc.Id.Equals(model)
                    select comment).ToList();
        }

        public List<TaskMaster> GetTasksListByDocumentId(int model)
        {
            return
                (from task in base.GetQuery<TaskMaster>()
                    join doc in base.GetQuery<DocumentMaster>()
                        on task.MasterDocumentId equals doc.Id
                 where doc.Id.Equals(model)
                    select task).ToList();

        }

        public IEnumerable<TaskCommentModel> GetTasksListByTaskId(int taskId)
        {
            return
                (from comment in base.GetQuery<TaskComment>(p => p.TaskId.Equals(taskId))
                    join perm in base.GetQuery<ApplicationPermissions>()
                        on comment.CreatedBy equals perm.PermissionId
                    select new TaskCommentModel()
                    {
                        Id = comment.Id,
                        TaskId = comment.TaskId,
                        Comments = comment.Comments,
                        CreatedBy = comment.CreatedBy,
                        CreatedOn = comment.CreatedOn,
                        UserName = perm.UserName
                    });
        }
    }
}
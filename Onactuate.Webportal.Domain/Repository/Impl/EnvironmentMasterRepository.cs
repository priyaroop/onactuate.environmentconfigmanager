﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;
using TransactionScope = System.Transactions.TransactionScope;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class EnvironmentMasterRepository : RepositoryBase, IEnvironmentMasterRepository
    {
        public EnvironmentMasterRepository()
        {
        }

        public EnvironmentMasterRepository(ObjectContext context) : base(context)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public IEnumerable<EnvironmentMaster> GetEnvironmentMasterList()
        {
            return base.GetQuery<EnvironmentMaster>().ToList();
        }

        public EnvironmentMaster GetEnvironmentMasterByEnvironmentName(string environmentName)
        {
            return
                base.GetQuery<EnvironmentMaster>(
                    p => p.EnvironmentName.Equals(environmentName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        public EnvironmentMaster GetEnvironmentMasterByEnvironmentId(int environmentId)
        {
            return base.GetQuery<EnvironmentMaster>(p => p.EnvironmentId.Equals(environmentId)).FirstOrDefault();
        }

        public IEnumerable<EnvironmentMaster> GetEnvironmentMasterListByPermissionId(int userPermissionId)
        {
            return from env in base.GetQuery<EnvironmentMaster>()
                join envPerm in base.GetQuery<EnvironmentPermissionMaster>()
                    on env.EnvironmentId equals envPerm.EnvironmentId
                where envPerm.PermissionId.Equals(userPermissionId)
                select env;
        }

        public IEnumerable<EnvironmentMaster> GetAdminEnvMasterListByPermissionId(int permissionId)
        {
            return from env in base.GetQuery<EnvironmentMaster>()
                join envPerm in base.GetQuery<EnvironmentPermissionMaster>()
                    on env.EnvironmentId equals envPerm.EnvironmentId
                where envPerm.PermissionId.Equals(permissionId) && envPerm.IsAdmin
                select env;
        }

        public void CreateEnvironmentMaster(EnvironmentMaster environmentMaster, EnvironmentPermissionMaster permissions)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                base.SaveChanges<EnvironmentMaster>(environmentMaster);
                permissions.EnvironmentId = environmentMaster.EnvironmentId;
                base.SaveChanges<EnvironmentPermissionMaster>(permissions);

                // TODO: Code cleanup. Delete this after testing the Create Environment functionality
                //// Create a Default Token
                //TokenMaster token = new TokenMaster()
                //{
                //    EnvironmentId = environmentMaster.EnvironmentId,
                //    TokenName = "Default",
                //    TokenDescription = "Default Token. Created by default for each region"
                //};
                //base.SaveChanges<TokenMaster>(token);

                //// Create a Default Key
                //KeyMaster key = new KeyMaster()
                //{
                //    TokenId = token.TokenId,
                //    IsEncrypted = false,
                //    KeyName = "DefaultKey",
                //    KeyValue = "DefaultKeyValue"
                //};
                //base.SaveChanges<KeyMaster>(key);

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void CloneEnvironmentMaster(int environmentIdToClone, EnvironmentMaster environmentMaster, EnvironmentPermissionMaster permissions)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                // Create new Environment
                base.SaveChanges<EnvironmentMaster>(environmentMaster);
                permissions.EnvironmentId = environmentMaster.EnvironmentId;

                // Save permission
                base.SaveChanges<EnvironmentPermissionMaster>(permissions);

                // Clone all environment permissions
                IEnumerable<EnvironmentPermissionMaster> permList = GetQuery<EnvironmentPermissionMaster>(p=>p.EnvironmentId.Equals(environmentIdToClone) && p.PermissionId != permissions.PermissionId);
                foreach (EnvironmentPermissionMaster perm in permList)
                {
                    // Clone the permission
                    EnvironmentPermissionMaster clonePermission = new EnvironmentPermissionMaster()
                    {
                        EnvironmentId = environmentMaster.EnvironmentId,
                        PermissionId = perm.PermissionId,
                        IsAdmin = perm.IsAdmin
                    };
                    base.SaveChanges<EnvironmentPermissionMaster>(clonePermission);
                }

                // Get All tokens for environmentIdToClone
                IEnumerable<TokenMaster> tokenList = GetQuery<TokenMaster>(p => p.EnvironmentId.Equals(environmentIdToClone));
                foreach (TokenMaster token in tokenList)
                {
                    // Clone the Token
                    TokenMaster cloneToken = new TokenMaster()
                    {
                        EnvironmentId = environmentMaster.EnvironmentId,
                        TokenName = token.TokenName,
                        TokenDescription = token.TokenDescription
                    };
                    base.SaveChanges<TokenMaster>(cloneToken);

                    // Clone all Keys for the token
                    IEnumerable<KeyMaster> keysList = GetQuery<KeyMaster>(p => p.TokenId.Equals(token.TokenId));
                    foreach (KeyMaster key in keysList)
                    {
                        // Clone the Key
                        KeyMaster cloneKey = new KeyMaster()
                        {
                            TokenId = cloneToken.TokenId,
                            IsEncrypted = key.IsEncrypted,
                            KeyName = key.KeyName,
                            KeyValue = key.KeyValue
                        };
                        base.SaveChanges<KeyMaster>(cloneKey);
                    }
                }

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void CreateEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (EnvironmentMaster environmentMastes in environmentMasterList)
                {
                    base.SaveChanges<EnvironmentMaster>(environmentMastes);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void UpdateEnvironmentMaster(EnvironmentMaster environmentMaster)
        {
            base.Update<EnvironmentMaster>(environmentMaster);
        }

        public void UpdateEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (EnvironmentMaster environmentMastes in environmentMasterList)
                {
                    base.Update<EnvironmentMaster>(environmentMastes);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteEnvironmentMaster(EnvironmentMaster environmentMaster)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                // Delete all the environment permissions
                foreach (EnvironmentPermissionMaster perm in GetQuery<EnvironmentPermissionMaster>(p=>p.EnvironmentId.Equals(environmentMaster.EnvironmentId)))
                {
                    base.Delete<EnvironmentPermissionMaster>(perm);
                }

                // Delete all the keys for the environment
                IEnumerable<KeyMaster> keysList = from env in base.GetQuery<EnvironmentMaster>()
                                                    join token in base.GetQuery<TokenMaster>()
                                                        on env.EnvironmentId equals token.EnvironmentId
                                                    join key in base.GetQuery<KeyMaster>()
                                                        on token.TokenId equals key.TokenId
                                                    where env.EnvironmentId.Equals(environmentMaster.EnvironmentId)
                                                    select key;
                foreach (KeyMaster key in keysList)
                {
                    base.Delete<KeyMaster>(key);
                }

                // Delete all the tokens for the environment
                IEnumerable<TokenMaster> tokenList = from env in base.GetQuery<EnvironmentMaster>()
                                                        join token in base.GetQuery<TokenMaster>()
                                                            on env.EnvironmentId equals token.EnvironmentId
                                                        where env.EnvironmentId.Equals(environmentMaster.EnvironmentId)
                                                        select token;
                foreach (TokenMaster token in tokenList)
                {
                    base.Delete<TokenMaster>(token);
                }

                // Finally, delete the environment
                base.Delete<EnvironmentMaster>(environmentMaster);

                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteEnvironmentMasterMultiple(IEnumerable<EnvironmentMaster> environmentMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (EnvironmentMaster environment in environmentMasterList)
                {
                    DeleteEnvironmentMaster(environment);
                    //base.Delete<EnvironmentMaster>(environmentMastes);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }
    }
}
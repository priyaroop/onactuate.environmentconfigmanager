﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class DocumentApproverMasterRepository : RepositoryBase, IDocumentApproverMasterRepository
    {
        public DocumentApproverMasterRepository()
        {
        }

        protected DocumentApproverMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void CreateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster)
        {
            base.SaveChanges<DocumentApproverMaster>(documentApproverMaster);
        }

        public void UpdateDocumentApproverMaster(DocumentApproverMaster documentApproverMaster)
        {
            base.Update<DocumentApproverMaster>(documentApproverMaster);
        }

        public void DeleteDocumentApproverMaster(DocumentApproverMaster documentApproverMaster)
        {
            base.Delete<DocumentApproverMaster>(documentApproverMaster);
        }

        public void DeleteDocumentApproverMasterList(IList<DocumentApproverMaster> documentApproverMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (DocumentApproverMaster docAppMaster in documentApproverMasterList)
                {
                    base.Delete<DocumentApproverMaster>(docAppMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterListByDocumentId(int documentId)
        {
            return base.GetQuery<DocumentApproverMaster>(p => p.DocumentId.Equals(documentId)).ToList();
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterListByPermissionId(int permissionId)
        {
            return base.GetQuery<DocumentApproverMaster>(p => p.PermissionId.Equals(permissionId)).ToList();
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterListById(int id)
        {
            return base.GetQuery<DocumentApproverMaster>(p => p.Id.Equals(id)).ToList();
        }

        public IList<DocumentApproverMaster> GetDocumentApproverMasterList()
        {
            return base.GetQuery<DocumentApproverMaster>().ToList();
        }
    }
}
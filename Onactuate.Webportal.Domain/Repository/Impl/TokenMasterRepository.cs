﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class TokenMasterRepository : RepositoryBase, ITokenMasterRepository
    {
        public TokenMasterRepository()
        {
        }

        public TokenMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }


        public IEnumerable<TokenMaster> GetTokenMasterList()
        {
            return base.GetQuery<TokenMaster>();
        }

        public IEnumerable<TokenMaster> GetTokenMasterListByEnvironmentId(int environmentId)
        {
            return base.GetQuery<TokenMaster>(p => p.EnvironmentId.Equals(environmentId));
        }

        public TokenMaster GetTokenMasterByEnvironmentIdAndTokeName(int environmentId, string tokenName)
        {
            return base.GetQuery<TokenMaster>(p => p.EnvironmentId.Equals(environmentId) && p.TokenName.Equals(tokenName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        public void CreateTokenMaster(TokenMaster tokenMaster)
        {
            base.SaveChanges(tokenMaster);
        }

        public void CreateTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (TokenMaster tokenMaster in tokenMasterList)
                {
                    base.SaveChanges<TokenMaster>(tokenMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void UpdateTokenMaster(TokenMaster tokenMaster)
        {
            base.Update<TokenMaster>(tokenMaster);
        }

        public void UpdateTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (TokenMaster tokenMaster in tokenMasterList)
                {
                    base.Update<TokenMaster>(tokenMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }

        public void DeleteTokenMaster(TokenMaster tokenMaster)
        {
            base.Delete<TokenMaster>(tokenMaster);
        }

        public void DeleteTokenMasterMultiple(IEnumerable<TokenMaster> tokenMasterList)
        {
            TransactionScope scope = this.CreateTransactionScope();
            try
            {
                foreach (TokenMaster tokenMaster in tokenMasterList)
                {
                    base.Delete<TokenMaster>(tokenMaster);
                }
                scope.Complete();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = exception.InnerException.Message;
                }
                throw new Exception(message, exception);
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Onactuate.Webportal.Domain.Model
{
    public class ApproverGroupMember
    {
        public virtual int Id { get; set; }
        public virtual int ApproverGroupId { get; set; }
        public virtual int PermissionId { get; set; }

        public virtual ApproverGroupMaster ApproverGroupObj { get; set; }
        public virtual ApplicationPermissions ApplicationPermissionsObj { get; set; }

    }
}
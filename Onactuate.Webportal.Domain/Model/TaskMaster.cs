﻿using System;

namespace Onactuate.Webportal.Domain.Model
{
    public class TaskMaster
    {
        public virtual int Id { get; set; }
        public virtual string TaskName { get; set; }
        public virtual int OwnerPermissionId { get; set; }
        public virtual int AssignedToPermissionId { get; set; }
        public virtual DateTime CompletionDate { get; set; }
        public virtual TaskStatus TaskStatusId { get; set; }
        public virtual bool IsNewTask { get; set; }
        public virtual string TaskDocumentName { get; set; }
        public virtual string TaskDocumentCopyName { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual int MasterDocumentId { get; set; }
        public virtual int ApproverGroupId { get; set; }
    }
}
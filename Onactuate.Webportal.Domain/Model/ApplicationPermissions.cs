﻿namespace Onactuate.Webportal.Domain.Model
{
    public class ApplicationPermissions
    {
        public virtual bool IsDeletable { get; set; }

        public virtual int PermissionId { get; set; }

        public virtual int PermissionTypeId { get; set; }

        public virtual string UserName { get; set; }

        public virtual string Password { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onactuate.Webportal.Domain.Model
{
    public class WorkflowStep
    {
        public virtual int Id { get; set; }
        public virtual int WorkflowMasterId { get; set; }
        public virtual String StepName { get; set; }
        public virtual String StepDescription { get; set; }

        public virtual WorkflowMaster WorkflowMaster { get; set; }
        public virtual ICollection<WorkflowStepApprover> WorkflowStepApprovers { get; set; }
    }
}

﻿namespace Onactuate.Webportal.Domain.Model
{
    public class EnvironmentPermissionMaster
    {
        public virtual int EnvironmentPermissionId { get; set; }

        public virtual int PermissionId { get; set; }

        public virtual int EnvironmentId { get; set; }

        public virtual bool IsAdmin { get; set; }

        public virtual ApplicationPermissions ApplicationPermissionsObj { get; set; }
        public virtual EnvironmentMaster EnvironmentMasterObj { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onactuate.Webportal.Domain.Model
{
    public class WorkflowStepApprover
    {
        public virtual int Id { get; set; }
        public virtual int WorkflowStepId { get; set; }
        public virtual int PermissionId { get; set; }
        public virtual WorkflowStep WorkflowStep { get; set; }
    }
}

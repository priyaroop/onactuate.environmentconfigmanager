﻿namespace Onactuate.Webportal.Domain.Model
{
    public class SharepointMaster
    {
        public virtual int Id { get; set; }
        public virtual int DocumentMasterId { get; set; }
        public virtual string SharepointUrl { get; set; }
        public virtual string SharepointUsername { get; set; }
        public virtual byte[] SharepointPassword { get; set; }
        public virtual string SharepointRelativeFolderPath { get; set; }
    }
}
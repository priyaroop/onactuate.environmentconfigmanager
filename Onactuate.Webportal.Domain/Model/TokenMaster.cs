﻿namespace Onactuate.Webportal.Domain.Model
{
    public class TokenMaster
    {
        public virtual int TokenId { get; set; }

        public virtual int EnvironmentId { get; set; }

        public virtual string TokenName { get; set; }

        public virtual string TokenDescription { get; set; }

        public virtual EnvironmentMaster EnvironmentMasterObj { get; set; }
    }
}
﻿using System;

namespace Onactuate.Webportal.Domain.Model
{
    public class DocumentMaster
    {
        public virtual int Id { get; set; }
        public virtual string DocumentName { get; set; }
        public virtual string DocumentDescription { get; set; }
        public virtual string UploadFileName { get; set; }
        public virtual DateTime UploadedOn { get; set; }
        public virtual int UploadedByPermissionId { get; set; }

        public virtual bool HasBeenOpened { get; set; }

        public virtual int DocumentType { get; set; }

    }
}
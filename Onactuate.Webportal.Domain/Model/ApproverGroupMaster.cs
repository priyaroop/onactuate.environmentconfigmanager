﻿namespace Onactuate.Webportal.Domain.Model
{
    public class ApproverGroupMaster
    {
        public virtual int Id { get; set; }
        public virtual string GroupName { get; set; }
    }
}
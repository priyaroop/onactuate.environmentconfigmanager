﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onactuate.Webportal.Domain.Model
{
    public class WorkflowMaster
    {
        public virtual int Id { get; set; }
        public virtual string WorkflowName { get; set; }
        public virtual string WorkflowDescription { get; set; }
        public virtual ICollection<WorkflowStep> WorkflowSteps { get; set; }
    }
}

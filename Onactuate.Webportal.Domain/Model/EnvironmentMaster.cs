﻿namespace Onactuate.Webportal.Domain.Model
{
    public class EnvironmentMaster
    {
        public virtual int EnvironmentId { get; set; }

        public virtual string EnvironmentName { get; set; }

        public virtual string EnvironmentDescription { get; set; }
    }
}
﻿namespace Onactuate.Webportal.Domain.Model
{
    public class KeyMaster
    {
        public virtual int KeyId { get; set; }

        public virtual int TokenId { get; set; }

        public virtual string KeyName { get; set; }

        public virtual string KeyValue { get; set; }

        public virtual bool IsEncrypted { get; set; }

        public virtual TokenMaster TokenMasterObj { get; set; }
    }
}
﻿using System;

namespace Onactuate.Webportal.Domain.Model
{
    public class TaskComment
    {
        public virtual int Id { get; set; }
        public virtual int TaskId { get; set; }
        public virtual string Comments { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual int CreatedBy { get; set; }

    }
}
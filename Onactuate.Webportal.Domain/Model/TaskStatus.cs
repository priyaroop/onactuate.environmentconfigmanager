﻿namespace Onactuate.Webportal.Domain.Model
{
    public enum TaskStatus
    {
        Assigned = 1,
        InProgress,
        SentForApproval,
        Approved,
        Rejected,
        Completed,
        Deleted,
        TaskToApprove
    }
}
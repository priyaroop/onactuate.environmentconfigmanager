﻿using System.Collections.Generic;

namespace Onactuate.Webportal.Domain.Model
{
    public class DocumentApproverMaster
    {
        public virtual int Id { get; set; }
        public virtual int DocumentId { get; set; }
        public virtual int PermissionId { get; set; }

        //public virtual DocumentMaster DocumentMasterObj { get; set; }
        //public virtual IList<ApplicationPermissions> ApplicationPermissionsList { get; set; }
    }
}
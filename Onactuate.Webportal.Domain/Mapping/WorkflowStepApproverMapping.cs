﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class WorkflowStepApproverMapping : EntityTypeConfiguration<WorkflowStepApprover>
    {
        public WorkflowStepApproverMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.WorkflowStepId);
            base.Property(t => t.PermissionId);

            //base.HasRequired<WorkflowStep>(x=>x.WorkflowStep).WithMany(x => x.WorkflowStepApprovers).WillCascadeOnDelete(true);
            base.ToTable("WorkflowStepApprovers");
        }
    }
}
﻿using System.Data.Entity.ModelConfiguration;
using System.Linq.Expressions;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class ApplicationPermissionsMapping : EntityTypeConfiguration<ApplicationPermissions>
    {
        public ApplicationPermissionsMapping()
        {
            base.HasKey<int>(t => t.PermissionId);
            base.Property(t => t.UserName).IsRequired().HasMaxLength(0x3e8);
            base.Property(t => t.Password).IsRequired();
            base.Property<bool>((Expression<System.Func<ApplicationPermissions, bool>>)(t => t.IsDeletable)).IsRequired();
            base.Property<int>((Expression<System.Func<ApplicationPermissions, int>>) (t => t.PermissionTypeId)).IsRequired();

            base.ToTable("ApplicationPermission");
        }
    }
}


﻿using System.Data.Entity.ModelConfiguration;
using System.Linq.Expressions;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class KeyMasterMapping : EntityTypeConfiguration<KeyMaster>
    {
        public KeyMasterMapping()
        {
            base.HasKey<int>(t => t.KeyId);
            base.Property<int>(t => t.TokenId);
            base.Property(t => t.KeyName).IsRequired();
            base.Property(t => t.KeyValue);
            base.Property<bool>((Expression<System.Func<KeyMaster, bool>>)(t => t.IsEncrypted)).IsRequired();

            this.HasRequired(x => x.TokenMasterObj).WithMany().HasForeignKey(x => x.TokenId);

            base.ToTable("KeyMaster");
        }
    }
}
﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class ApproverGroupMemberMapping : EntityTypeConfiguration<ApproverGroupMember>
    {
        public ApproverGroupMemberMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.ApproverGroupId);
            base.Property(t => t.PermissionId);

            this.HasRequired(x => x.ApproverGroupObj).WithMany().HasForeignKey(x => x.ApproverGroupId);
            this.HasRequired(x => x.ApplicationPermissionsObj).WithMany().HasForeignKey(x => x.PermissionId);

            base.ToTable("ApproverGroupMember");


        }
    }
}
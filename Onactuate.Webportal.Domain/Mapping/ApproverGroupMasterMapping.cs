﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class ApproverGroupMasterMapping : EntityTypeConfiguration<ApproverGroupMaster>
    {
        public ApproverGroupMasterMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.GroupName);

            base.ToTable("ApproverGroupMaster");
        }
    }
}
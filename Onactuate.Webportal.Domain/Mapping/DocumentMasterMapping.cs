﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class DocumentMasterMapping : EntityTypeConfiguration<DocumentMaster>
    {
        public DocumentMasterMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.DocumentName).IsRequired();
            base.Property(t => t.DocumentDescription);
            base.Property(t => t.UploadFileName);
            base.Property(t => t.UploadedOn);
            base.Property(t => t.UploadedByPermissionId);
            base.Property(t => t.HasBeenOpened);
            base.Property(t => t.DocumentType);

            base.ToTable("DocumentMaster");
        }
    }
}
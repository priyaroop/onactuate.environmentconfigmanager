﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class WorkflowStepMapping : EntityTypeConfiguration<WorkflowStep>
    {
        public WorkflowStepMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.WorkflowMasterId);
            base.Property(t => t.StepName);
            base.Property(t => t.StepDescription);


            base.ToTable("WorkflowSteps");
        }
    }
}
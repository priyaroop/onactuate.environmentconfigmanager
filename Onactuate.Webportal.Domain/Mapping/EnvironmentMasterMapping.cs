﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class EnvironmentMasterMapping : EntityTypeConfiguration<EnvironmentMaster>
    {
        public EnvironmentMasterMapping()
        {
            base.HasKey<int>(t => t.EnvironmentId);
            base.Property(t => t.EnvironmentName).IsRequired();
            base.Property(t => t.EnvironmentDescription);
            
            base.ToTable("EnvironmentMaster");

        }
    }
}
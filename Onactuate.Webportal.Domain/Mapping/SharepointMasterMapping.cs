﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class SharepointMasterMapping : EntityTypeConfiguration<SharepointMaster>
    {
        public SharepointMasterMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.DocumentMasterId).IsRequired();
            base.Property(t => t.SharepointUrl).IsRequired();
            base.Property(t => t.SharepointUsername).IsRequired();
            base.Property(t => t.SharepointPassword).IsRequired();
            base.Property(t => t.SharepointRelativeFolderPath);

            base.ToTable("SharepointMaster");
        }
    }
}
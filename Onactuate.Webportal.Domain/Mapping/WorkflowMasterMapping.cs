﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class WorkflowMasterMapping : EntityTypeConfiguration<WorkflowMaster>
    {
        public WorkflowMasterMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.WorkflowName);
            base.Property(t => t.WorkflowDescription);

            base.ToTable("WorkflowMaster");
        }
    }
}
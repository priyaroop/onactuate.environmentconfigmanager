﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class DocumentApproverMasterMapping : EntityTypeConfiguration<DocumentApproverMaster>
    {
        public DocumentApproverMasterMapping()
        {
            base.HasKey(t => new { t.Id, t.DocumentId, t.PermissionId});
            //base.Property(t => t.DocumentId);
            //base.Property(t => t.PermissionId);

            //this.HasRequired(x => x.DocumentMasterObj).WithMany().HasForeignKey(x => x.DocumentId);
            //this.HasRequired(x => x.ApplicationPermissionsList).WithMany().HasForeignKey(x => x.PermissionId);

            base.ToTable("DocumentApproverMaster");
        }
    }
}
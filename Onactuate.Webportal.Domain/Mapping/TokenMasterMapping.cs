﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class TokenMasterMapping : EntityTypeConfiguration<TokenMaster>
    {
        public TokenMasterMapping()
        {
            base.HasKey<int>(t => t.TokenId);
            base.Property<int>(t => t.EnvironmentId).IsRequired();
            base.Property(t => t.TokenName).IsRequired();
            base.Property(t => t.TokenDescription);

            this.HasRequired(x => x.EnvironmentMasterObj).WithMany().HasForeignKey(x => x.EnvironmentId);

            base.ToTable("TokenMaster");
        }
    }
}
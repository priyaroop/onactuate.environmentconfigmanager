﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class TaskCommentMapping : EntityTypeConfiguration<TaskComment>
    {
        public TaskCommentMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.TaskId);
            base.Property(t => t.Comments);
            base.Property(t => t.CreatedOn);
            base.Property(t => t.CreatedBy);
            
            base.ToTable("TaskComment");
        }
    }
}
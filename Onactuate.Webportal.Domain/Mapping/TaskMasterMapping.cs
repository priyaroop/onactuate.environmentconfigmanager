﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class TaskMasterMapping : EntityTypeConfiguration<TaskMaster>
    {
        public TaskMasterMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.TaskName);
            base.Property(t => t.OwnerPermissionId);
            base.Property(t => t.AssignedToPermissionId);
            base.Property(t => t.CompletionDate);
            base.Property(t => t.TaskStatusId);
            base.Property(t => t.IsNewTask);
            base.Property(t => t.TaskDocumentName);
            base.Property(t => t.TaskDocumentCopyName);
            base.Property(t => t.CreatedOn);
            base.Property(t => t.MasterDocumentId);
            base.Property(t => t.ApproverGroupId);

            base.ToTable("TaskMaster");
        }
    }
}
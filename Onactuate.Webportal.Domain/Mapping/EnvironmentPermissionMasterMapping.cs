﻿using System.Data.Entity.ModelConfiguration;
using System.Linq.Expressions;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class EnvironmentPermissionMasterMapping : EntityTypeConfiguration<EnvironmentPermissionMaster>
    {
        public EnvironmentPermissionMasterMapping()
        {
            base.HasKey(t => new {t.EnvironmentPermissionId});
            base.Property<int>(t => t.PermissionId);
            base.Property<int>(t => t.EnvironmentId);
            base.Property<bool>((Expression<System.Func<EnvironmentPermissionMaster, bool>>)(t => t.IsAdmin));

            this.HasRequired(x => x.ApplicationPermissionsObj).WithMany().HasForeignKey(x => x.PermissionId);
            this.HasRequired(x => x.EnvironmentMasterObj).WithMany().HasForeignKey(x => x.EnvironmentId);

            base.ToTable("EnvironmentPermissionMaster");
        }
    }
}